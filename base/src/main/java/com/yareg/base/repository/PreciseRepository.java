package com.yareg.base.repository;

import androidx.annotation.NonNull;

import com.yareg.base.livedata.SparseLiveData;
import com.yareg.base.model.PersistentPrecise;

public class PreciseRepository extends SparseLiveData<PersistentPrecise> {

    //----------------------------------------------------------------------------------------------
    //
    // INSTANCE
    // https://en.wikipedia.org/wiki/Initialization_on_demand_holder_idiom
    //
    //----------------------------------------------------------------------------------------------

    private static class Singleton {
        static final PreciseRepository INSTANCE = new PreciseRepository();
    }

    @NonNull
    public static PreciseRepository getInstance() { return PreciseRepository.Singleton.INSTANCE; }

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    private PreciseRepository() {
        super(new PersistentPrecise());
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    public void putFloat(final long key, final float value) {
        setItem(new PersistentPrecise(key, value));
    }
}
