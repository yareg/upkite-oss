package com.yareg.base.repository;

import androidx.annotation.NonNull;

import com.yareg.base.livedata.SparseLiveData;
import com.yareg.base.model.PersistentInteger;

public class IntegerRepository extends SparseLiveData<PersistentInteger> {

    //----------------------------------------------------------------------------------------------
    //
    // INSTANCE
    // https://en.wikipedia.org/wiki/Initialization_on_demand_holder_idiom
    //
    //----------------------------------------------------------------------------------------------

    private static class Singleton {
        static final IntegerRepository INSTANCE = new IntegerRepository();
    }

    @NonNull
    public static IntegerRepository getInstance() { return IntegerRepository.Singleton.INSTANCE; }

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    private IntegerRepository() {
        super(new PersistentInteger());
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    public void putLong(final long key, final long value) {
        setItem(new PersistentInteger(key, value));
    }

    /*
    public void putInt(final long key, final int value) {
        putItem(new PersistentInteger(key, value));
    }
    */

    public void putBoolean(final long key, final boolean value) {
        setItem(new PersistentInteger(key, value ? 1 : 0));
    }
}
