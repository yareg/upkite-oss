package com.yareg.base.activity;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.yareg.base.livedata.Snap;
import com.yareg.base.manager.BaseNotificationManager;
import com.yareg.base.ui.Ui;

public abstract class BaseActivity extends AppCompatActivity {

    private ViewModelProvider viewModelProvider;

    protected Ui   ui;
    protected View baseLayout;
    protected View contentFrame;

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModelProvider = new ViewModelProvider(
                this,
                ViewModelProvider.AndroidViewModelFactory.getInstance(getApplication())
        );

        ui = viewModelProvider.get(Ui.class);

        Snap.getInstance().observe(this, (data)-> {
            if(data == null) { return; }

            final View view = ui.isDrawerOpen ? baseLayout : contentFrame;

            if(data.action == null) {
                BaseNotificationManager.showSnackbar(view, data.stringRes);
            } else {
                BaseNotificationManager.showSnackbar(
                        view, data.stringRes, data.actionRes, data.action
                );
            }
        });
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- GET VIEW MODEL PROVIDER -----------------------------------------------------------------

    public ViewModelProvider getViewModelProvider() { return viewModelProvider; }

    // --- GET UI ----------------------------------------------------------------------------------

    public Ui getUi() { return ui; }
}