package com.yareg.base.activity;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.yareg.base.fragment.FragmentItem;
import com.yareg.base.ui.Ui;

public abstract class FragmentActivity extends BaseActivity {

    private static int containerViewId;

    private boolean isFragmentTransactionPending = false;

    //----------------------------------------------------------------------------------------------
    //
    // SUPER OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) { ui.setStartFragment(); }
    }

    @Override
    public void onBackPressed() {
        if (ui.getFragmentStack().onBackPressed()) { return; }
        super.onBackPressed();
    }

    //----------------------------------------------------------------------------------------------
    //
    // PROTECTED METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- SET FRAGMENT CONTAINER VIEW ID ----------------------------------------------------------

    protected void setFragmentContainerViewId(final int viewId) {
        containerViewId = viewId;
        ui.getFragmentStack().observe(this, this::onFragmentStackUpdated);
    }

    // --- ON FRAGMENT TRANSACTION -----------------------------------------------------------------

    protected void onFragmentTransaction(@NonNull final FragmentItem item) {
        ui.setFragmentData(item);
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- ON FRAGMENT STACK UPDATED ---------------------------------------------------------------

    private void onFragmentStackUpdated(@Nullable final FragmentItem item) {
        if (item == null || isFragmentTransactionPending) { return; }
        isFragmentTransactionPending = true;
        final FragmentTransaction ft = createTransaction(item);
        ft.runOnCommit(()-> {
            onFragmentTransaction(item);
            isFragmentTransactionPending = false;
        });
        ft.commit();
    }

    // --- CREATE TRANSACTION ----------------------------------------------------------------------

    @NonNull
    private FragmentTransaction createTransaction(@NonNull final FragmentItem item) {

        final FragmentManager     fragmentManager = getSupportFragmentManager();
        final FragmentItem        previousItem    = ui.getFragmentStack().getPreviousItem();
        final FragmentTransaction transaction     = fragmentManager.beginTransaction();

        Fragment fragment = fragmentManager.findFragmentByTag(item.tag);
        if (fragment == null) {
            fragment = item.creator.createInstance();
            transaction.add(containerViewId, fragment, item.tag);
        } else {
            transaction.show(fragment);
        }

        if (previousItem == null) { return transaction; }

        final Fragment previousFragment = fragmentManager.findFragmentByTag(previousItem.tag);
        if (previousFragment != null) {
            fragment.setEnterTransition(Ui.FADE_IN);
            previousFragment.setExitTransition(Ui.FADE_OUT);
            if (previousItem.isDisposable) {
                transaction.remove(previousFragment);
            } else {
                transaction.hide(previousFragment);
            }
        }

        return transaction;
    }
}
