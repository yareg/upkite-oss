package com.yareg.base.manager;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.core.app.NotificationCompat;

import android.os.Build;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.yareg.base.model.BaseNotification;

public abstract class BaseNotificationManager {

    //----------------------------------------------------------------------------------------------
    //
    // NOTIFICATION
    // https://developer.android.com/guide/topics/ui/notifiers/notifications.html
    //
    //----------------------------------------------------------------------------------------------

    public static void createNotificationChannel(
            @NonNull final Context context,
            @StringRes final int idRes,
            @StringRes final int nameRes,
            @StringRes final int descriptionRes
    ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            final NotificationManager service = context.getSystemService(NotificationManager.class);

            if (service == null) { return; }

            final NotificationChannel channel = new NotificationChannel(
                    context.getString(idRes),
                    context.getString(nameRes),
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            channel.setDescription(context.getString(descriptionRes));
            service.createNotificationChannel(channel);
        }
    }

    public static void showNotification(
            @NonNull final Context context,
            @NonNull final BaseNotification notification) {

        final NotificationManager service = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ?
                context.getSystemService(NotificationManager.class) :
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (service == null) { return; }

        final NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context, notification.channelId)
                        .setSmallIcon(notification.getIcon())
                        .setContentTitle(notification.getTitle())
                        .setContentText(notification.getText())
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                        .setDefaults(
                                Notification.DEFAULT_SOUND |
                                Notification.DEFAULT_VIBRATE |
                                Notification.DEFAULT_LIGHTS
                        )
                        .setAutoCancel(true);

        if (notification.getIntent() != null) {
            builder.setContentIntent(notification.getIntent());
        }

        if (notification.getLargeIcon() != null) {
            builder.setLargeIcon(notification.getLargeIcon());
        }

        service.notify(notification.id, builder.build());
    }

    //----------------------------------------------------------------------------------------------
    //
    // SNACKBAR
    //
    //----------------------------------------------------------------------------------------------

    public static void showSnackbar(@NonNull final View view, @StringRes final int stringRes) {
        if (stringRes != 0) {
            Snackbar.make(view, stringRes, Snackbar.LENGTH_LONG).show();
        }
    }

    public static void showSnackbar(
            @NonNull final View view,
            @StringRes final int stringRes,
            @StringRes final int actionRes,
            @NonNull final View.OnClickListener action) {
        if (stringRes != 0) {
            final Snackbar snackbar = Snackbar.make(view, stringRes, Snackbar.LENGTH_LONG);
            snackbar.setAction(actionRes, action);
            snackbar.show();
        }
    }

    /*
    public static void showInfoSnackbar(@NonNull final View view, final int stringResId) {
        showThemedSnackbar(view, stringResId, SnackbarTheme.infoText, SnackbarTheme.infoBackground);
    }

    public static void showWarningSnackbar(@NonNull final View view, final int stringResId) {
        showThemedSnackbar(view, stringResId, SnackbarTheme.warningText, SnackbarTheme.warningBackground);
    }

    public static void showErrorSnackbar(@NonNull final View view, final int stringResId) {
        showThemedSnackbar(view, stringResId, SnackbarTheme.errorText, SnackbarTheme.errorBackground);
    }

    private static void showThemedSnackbar(
            @NonNull final View view,
            final int stringResId,
            final int textColor,
            final int backgroundColor) {

        final Snackbar snackbar = Snackbar.make(
                view,
                view.getContext().getString(stringResId),
                Snackbar.LENGTH_SHORT
        );

        final View snackbarView = snackbar.getView();
        final TextView textView = snackbarView.findViewById(
                com.google.android.material.R.id.snackbar_text
        );

        textView.setTextColor(textColor);
        snackbarView.setBackgroundColor(backgroundColor);
        snackbarView.setElevation(SnackbarTheme.elevation);

        snackbar.show();
    }
    */

    //----------------------------------------------------------------------------------------------
    //
    // TOAST
    //
    //----------------------------------------------------------------------------------------------

    public static void showToast(@NonNull final Context context, @StringRes final int stringRes) {
        if (stringRes != 0) {
            Toast.makeText(context, stringRes, Toast.LENGTH_SHORT).show();
        }
    }

    /*
    public static void showToast(@NonNull final Context context, @NonNull final String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }
    */
}
