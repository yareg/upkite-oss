package com.yareg.base.manager;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.yareg.base.R;

public class BaseUrlManager {

    public static void openInBrowser(@NonNull final Context context, @NonNull final String url) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        } catch(ActivityNotFoundException e) {
            BaseNotificationManager.showToast(context, R.string.error_no_browser);
        }
    }
}
