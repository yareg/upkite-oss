package com.yareg.base.manager;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;

import com.yareg.base.model.DatabaseTable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

public class BaseDatabaseManager extends SQLiteOpenHelper {

    private static volatile BaseDatabaseManager instance;

    public static String          DATABASE_NAME;
    public static int             DATABASE_VERSION;
    public static DatabaseTable[] DATABASE_TABLES;

    private final ArrayList<String> existingTables;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    private BaseDatabaseManager(@NonNull final Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        existingTables = new ArrayList<>(DATABASE_TABLES.length);
    }

    //----------------------------------------------------------------------------------------------
    //
    // INSTANCE
    //
    //----------------------------------------------------------------------------------------------

    public static BaseDatabaseManager getInstance(@NonNull final Context context) {
        BaseDatabaseManager i = instance;
        if (i == null){
            synchronized (BaseDatabaseManager.class) {
                i = instance;
                if (i == null) {
                    instance = i = new BaseDatabaseManager(context);
                }
            }
        }
        return i;
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public void onConfigure(@NonNull final SQLiteDatabase db) {
        //db.enableWriteAheadLogging();
    }

    @Override
    public void onCreate(@NonNull final SQLiteDatabase db) {
        //Log.i(TAG, "onCreate");
    }

    @Override
    public void onUpgrade(
            @NonNull final SQLiteDatabase db,
            final int oldVersion,
            final int newVersion) { upgrade(db); }

    @Override
    public void onOpen(@NonNull final SQLiteDatabase db) {
        //existingTables.clear();
        existingTables.addAll(getExistingTables(db));
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- CREATE TABLES ---------------------------------------------------------------------------

    public boolean createTables() {

        //existingTables.clear();

        final SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();

        try {
            for (final DatabaseTable table : DATABASE_TABLES) {
                createTable(db, table);
                existingTables.add(table.name);
            }
            db.setTransactionSuccessful();

        } finally {
            db.endTransaction();
        }

        return (existingTables.size() > 0);
    }

    // --- TABLE EXISTS ----------------------------------------------------------------------------

    public boolean tableExists(@NonNull final String table) {
        return existingTables.contains(table);
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- CREATE TABLE ----------------------------------------------------------------------------

    private void createTable(
            @NonNull final SQLiteDatabase db,
            @NonNull final DatabaseTable table) {

        String query = "CREATE TABLE IF NOT EXISTS " + table.name.concat(" (");
        int    index = 0;

        for(final Map.Entry<String, String> item : table.entrySet()) {
            query = query
                    .concat(item.getKey())
                    .concat(" ")
                    .concat(item.getValue())
                    .concat((index < (table.size() - 1)) ? ", " : ");");
            index++;
        }

        db.execSQL(query);
    }

    // --- GET EXISTING TABLES ---------------------------------------------------------------------

    private ArrayList<String> getExistingTables(@NonNull final SQLiteDatabase db) {

        final ArrayList<String> names = new ArrayList<>();

        final Cursor cursor = db.rawQuery(
                "SELECT * FROM sqlite_master WHERE type=?", new String[]{ "table" }
        );

        if (cursor == null) { return names; }

        while (cursor.moveToNext()) {
            final String tableName = cursor.getString(cursor.getColumnIndex("name"));
            names.add(tableName);
        }

        cursor.close();

        return names;
    }

    // --- UPGRADE ---------------------------------------------------------------------------------

    private void upgrade(@NonNull final SQLiteDatabase db) {

        final ArrayList<String> existingTables = getExistingTables(db);

        db.beginTransaction();

        try {
            for (final DatabaseTable table : DATABASE_TABLES) {
                if (existingTables.contains(table.name)) {
                    addColumns(db, table);
                    continue;
                }
                createTable(db, table);
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    // --- ADD COLUMNS -----------------------------------------------------------------------------

    private void addColumns(
            @NonNull final SQLiteDatabase db,
            @NonNull final DatabaseTable table) {

        final Cursor cursor = db.query(
                table.name,
                null,
                null,
                null,
                null,
                null,
                null
        );

        if (cursor == null) { return; }

        final ArrayList<String> existingColumns = new ArrayList<>();
        Collections.addAll(existingColumns, cursor.getColumnNames());
        cursor.close();

        for (Map.Entry<String, String> entry : table.entrySet()) {
            if (!existingColumns.contains(entry.getKey())) {
                final String sql = "ALTER TABLE " + table.name
                        .concat(" ADD COLUMN ")
                        .concat(entry.getKey())
                        .concat(" ")
                        .concat(entry.getValue())
                        .concat(";");
                db.execSQL(sql);
            }
        }
    }
}
