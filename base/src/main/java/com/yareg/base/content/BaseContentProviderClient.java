package com.yareg.base.content;

import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.util.LongSparseArray;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yareg.base.model.BaseModel;

import java.util.ArrayList;

public class BaseContentProviderClient {

    @NonNull
    public static String[] getByIdArg(final long id) {
        return new String[] { Long.toString(id) };
    }

    private final ContentProviderClient provider;
    private final ArrayList<ContentProviderOperation> batch = new ArrayList<>();

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    private BaseContentProviderClient(@NonNull final ContentProviderClient provider) {
        this.provider = provider;
    }

    //----------------------------------------------------------------------------------------------
    //
    // INSTANCE
    //
    //----------------------------------------------------------------------------------------------

    @NonNull
    public static BaseContentProviderClient getInstance(
            @NonNull final ContentProviderClient provider) {
        return new BaseContentProviderClient(provider);
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- GET ALL IDS -----------------------------------------------------------------------------

    @NonNull
    public long[] getAllIds(@NonNull final Uri uri) {
        return getIds(uri, null, null);
    }

    // --- GET TRASH IDS ---------------------------------------------------------------------------

    @NonNull
    public long[] getTrashIds(@NonNull final Uri uri) {
        return getIds(uri, BaseModel.KEY_ID + "<?", new String[]{"0"});
    }

    // --- GET BY PROTOTYPE ------------------------------------------------------------------------

    @Nullable
    public <T extends BaseModel<T>> T getByPrototype(@NonNull final T prototype) {

        final Uri uri = prototype.baseUri
                .buildUpon()
                .appendPath(Long.toString(prototype.id))
                .build();

        try {

            final Cursor cursor = provider.query(
                    uri,
                    null,
                    null,
                    null,
                    null
            );

            if (cursor != null && cursor.moveToNext()) {
                final T item = prototype.create(cursor);
                cursor.close();
                return item;
            }

            return null;

        } catch (RemoteException e) {
            return null;
        }
    }

    // --- COMPLETE PROTOTYPE ----------------------------------------------------------------------

    @NonNull
    public <T extends BaseModel<T>> T completePrototype(@NonNull final T prototype) {
        T item = getByPrototype(prototype);
        return (item == null) ? prototype : item;
    }

    // --- LIST ------------------------------------------------------------------------------------

    @NonNull
    public <T extends BaseModel<T>> LongSparseArray<T> list(
            @NonNull final T prototype,
            @Nullable final String selection,
            @Nullable final String[] selectionArgs) {

        try {

            final Cursor cursor = provider.query(
                    prototype.baseUri,
                    null,
                    selection,
                    selectionArgs,
                    null
            );

            if (cursor == null) {
                return new LongSparseArray<>(0);
            }

            final LongSparseArray<T> items = new LongSparseArray<>(cursor.getCount());

            while (cursor.moveToNext()) {
                final T item = prototype.create(cursor);
                items.put(item.id, item);
            }

            cursor.close();
            return items;

        } catch (RemoteException e) {
            return new LongSparseArray<>(0);
        }
    }

    // --- ADD INSERT TO BATCH ---------------------------------------------------------------------

    public <T extends BaseModel<T>> void addInsertToBatch(@NonNull final T data) {
        batch.add(
                ContentProviderOperation
                        .newInsert(data.baseUri)
                        .withValues(data.getContentValues())
                        .build()
        );
    }

    // --- ADD DELETE TO BATCH ---------------------------------------------------------------------

    public void addDeleteToBatch(@NonNull final Uri baseUri, final long id) {
        batch.add(
                ContentProviderOperation
                        .newDelete(baseUri)
                        .withSelection(BaseContentTasks.WHERE_ID, getByIdArg(id))
                        .build()
        );
    }

    // --- APPLY BATCH -----------------------------------------------------------------------------

    public void applyBatch() {
        try {
            provider.applyBatch(batch);
        } catch(RemoteException | OperationApplicationException e) {
            //Log.e(TAG, e.toString() + ", authority: " + AUTHORITY);
        }
    }

    // --- APPLY BATCH -----------------------------------------------------------------------------

/*    public void applyBatch(@NonNull final ArrayList<ContentProviderOperation> operations) {
        try {
            provider.applyBatch(operations);
        } catch(RemoteException | OperationApplicationException e) {
            //Log.e(TAG, e.toString() + ", authority: " + AUTHORITY);
        }
    }*/

    // --- SYNC DATA -------------------------------------------------------------------------------

    public <T extends BaseModel<T>> void syncData(@NonNull final T data) {
        final T parent = getByPrototype(data);
        if (parent == null || BaseModel.isChanged(parent, data)) {
            addInsertToBatch(data);
        }
    }

    // REMOVE OBSOLETE DATA ------------------------------------------------------------------------
    // This also purges trash

    public void removeObsoleteData(
            @NonNull final Uri uri,
            @NonNull final ArrayList<Long> actualIds) {
        for (final long id : getAllIds(uri)) {
            if (actualIds.contains(id)) { continue; }
            addDeleteToBatch(uri, id);
        }
    }

    // PURGE TRASH ---------------------------------------------------------------------------------

    public void purgeTrash(@NonNull final Uri uri) {
        batch.add(
                ContentProviderOperation
                        .newDelete(uri)
                        .withSelection(BaseModel.KEY_ID + "<?", new String[]{"0"})
                        .build()
        );
    }

    // --- CLOSE -----------------------------------------------------------------------------------

    public void close() {
        provider.close();
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE METHODS
    //
    //----------------------------------------------------------------------------------------------

    @NonNull
    private long[] getIds(
            @NonNull final Uri uri,
            @Nullable String selection,
            @Nullable String[] selectionArgs) {

        try {

            final Cursor cursor = provider.query(
                    uri,
                    new String[]{BaseModel.KEY_ID},
                    selection,
                    selectionArgs,
                    null
            );

            if (cursor == null) { return new long[0]; }

            final long[] ids = new long[cursor.getCount()];

            int i = 0;
            while (cursor.moveToNext()) {
                ids[i] = cursor.getLong(cursor.getColumnIndex(BaseModel.KEY_ID));
                i++;
            }
            cursor.close();

            return ids;

        } catch (RemoteException e) {
            return new long[0];
        }
    }
}
