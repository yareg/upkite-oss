package com.yareg.base.content;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.util.LongSparseArray;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yareg.base.async.AsyncWork;
import com.yareg.base.model.BaseModel;

public abstract class BaseContentTasks {

    public static final String WHERE_ID  = BaseModel.KEY_ID + "=?";

    //----------------------------------------------------------------------------------------------
    //
    // CALLBACKS
    //
    //----------------------------------------------------------------------------------------------

    public interface GetCallback<T> {
        void execute(@Nullable final T item);
    }

    public interface ListCallback<T> {
        void execute(@NonNull final T list);
    }

    public interface SetCallback {
        void execute(final boolean isSet);
    }

    public interface IdResult {
        void post(final long id);
    }

    //----------------------------------------------------------------------------------------------
    //
    // TASKS
    //
    //----------------------------------------------------------------------------------------------

    // --- GET TASK --------------------------------------------------------------------------------

    static class GetTask<T extends BaseModel<T>> extends AsyncWork<Void, T> {

        private final ContentResolver resolver;
        private final T prototype;
        private final GetCallback<T> callback;

        GetTask(
                @NonNull final ContentResolver resolver,
                @NonNull final T prototype,
                @NonNull final GetCallback<T> callback) {
            this.resolver = resolver;
            this.prototype = prototype;
            this.callback = callback;
        }

        @Override
        protected T doInBackground(Void... voids) {

            final Cursor cursor = resolver.query(
                    prototype.baseUri.buildUpon().appendPath(Long.toString(prototype.id)).build(),
                    null,
                    null,
                    null,
                    null
            );

            if (cursor != null && cursor.moveToNext()) {
                final T item = prototype.create(cursor);
                cursor.close();
                return item;
            }

            return null;
        }

        @Override
        protected void onPostExecute(@Nullable final T item) {
            callback.execute(item);
        }
    }

    // --- LIST TASK -------------------------------------------------------------------------------

    static class ListTask<T extends BaseModel<T>> extends AsyncWork<Void, LongSparseArray<T>> {

        private final ContentResolver resolver;
        private final T prototype;
        private final ListCallback<LongSparseArray<T>> callback;
        private final String selection;
        private final String[] selectionArgs;

        ListTask(
                @NonNull final ContentResolver resolver,
                @NonNull final T prototype,
                @NonNull final ListCallback<LongSparseArray<T>> callback,
                @Nullable final String selection,
                @Nullable final String[] selectionArgs) {
            this.resolver = resolver;
            this.prototype = prototype;
            this.callback = callback;
            this.selection = selection;
            this.selectionArgs = selectionArgs;
        }

        @Override
        protected LongSparseArray<T> doInBackground(Void... voids) {

            final Cursor cursor = resolver.query(
                    prototype.baseUri,
                    null,
                    selection,
                    selectionArgs,
                    null
            );

            if (cursor == null) { return new LongSparseArray<>(0); }

            final LongSparseArray<T> items = new LongSparseArray<>(cursor.getCount());
            while (cursor.moveToNext()) {
                final T item = prototype.create(cursor);
                items.put(item.id, item);
            }
            cursor.close();
            return items;
        }

        @Override
        protected void onPostExecute(@Nullable final LongSparseArray<T> items) {
            callback.execute(items == null ? new LongSparseArray<>() : items);
        }
    }

    // --- SET TASK --------------------------------------------------------------------------------

    static class SetTask<T extends BaseModel<T>> extends AsyncWork<Void, Boolean> {

        private final ContentResolver resolver;
        private final T item;
        private final SetCallback callback;

        SetTask(
                @NonNull final ContentResolver resolver,
                @NonNull final T item,
                @Nullable final SetCallback callback) {
            this.resolver = resolver;
            this.item = item;
            this.callback = callback;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            return (resolver.insert(item.baseUri, item.getContentValues()) != null);
        }

        @Override
        protected void onPostExecute(@Nullable final Boolean isSet) {
            if (callback != null) { callback.execute(isSet == null ? false : isSet); }
        }
    }

    // --- MULTIPLE SET TASK -----------------------------------------------------------------------

    // TODO: BATCH

    /*
    static class MultipleSetTask<T extends BaseModel<T>>
            extends AsyncTask<Void, Void, Boolean> {

        private final ArrayList<T> items;
        private final SetCallback callback;

        public MultipleSetTask(
                @NonNull final ArrayList<T> items,
                @Nullable final SetCallback callback) {
            this.items = items;
            this.callback = callback;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            int inserts = 0;
            for(final T item : items) {
                if(instance.service.insert(
                        item.baseUri,
                        item.getContentValues()) instanceof Uri) { inserts++; }
            }
            return (inserts == items.size());
        }

        @Override
        protected void onPostExecute(@NonNull final Boolean isSet) {
            if(callback == null) { return; }
            callback.onPostExecute(isSet);
        }
    }
    */

    // --- UPDATE TASK -----------------------------------------------------------------------------

    /*
    static class UpdateTask<T extends BaseModel<T>>
            extends AsyncTask<Void, Void, Boolean> {

        private final T item;
        private final SetCallback callback;

        public UpdateTask(@NonNull final T item, @Nullable final SetCallback callback) {
            this.item = item;
            this.callback = callback;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            return (1 == instance.service.update(
                    item.baseUri,
                    item.getContentValues(),
                    WHERE_ID,
                    new String[] { Long.toString(item.id) }));
        }

        @Override
        protected void onPostExecute(@NonNull final Boolean isSet) {
            if(callback == null) { return; }
            callback.onPostExecute(isSet);
        }
    }
    */

    // --- MULTIPLE UPDATE TASK --------------------------------------------------------------------

    // TODO: BATCH

    /*
    static class MultipleUpdateTask<T extends BaseModel<T>>
            extends AsyncTask<Void, Void, Boolean> {

        private final ArrayList<T> items;
        private final SetCallback callback;

        public MultipleUpdateTask(
                @NonNull final ArrayList<T> items,
                @Nullable final SetCallback callback) {
            this.items = items;
            this.callback = callback;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            int updates = 0;
            for(final T item : items) {
                if(1 == instance.service.update(
                        item.baseUri,
                        item.getContentValues(),
                        WHERE_ID,
                        new String[] { Long.toString(item.id) })) { updates++; }
            }
            return (updates == items.size());
        }

        @Override
        protected void onPostExecute(@NonNull final Boolean isSet) {
            if(callback == null) { return; }
            callback.onPostExecute(isSet);
        }
    }
    */

    // --- TRASH TASK ------------------------------------------------------------------------------

    static class TrashTask extends AsyncWork<Void, Long> {

        private final ContentResolver resolver;
        private final Uri uri;
        private final long id;
        private final IdResult callback;

        TrashTask(
                @NonNull final ContentResolver resolver,
                @NonNull final Uri uri,
                final long id,
                @Nullable final IdResult callback) {
            this.resolver = resolver;
            this.uri = uri;
            this.id = id;
            this.callback = callback;
        }

        @Override
        protected Long doInBackground(Void... voids) {

            final long trashId = -Math.abs(id);
            final ContentValues values = new ContentValues();
            values.put(BaseModel.KEY_ID, trashId);

            return resolver.update(
                    uri, values, WHERE_ID, new String[]{ Long.toString(id) }
                    ) == 1 ? trashId : 0L;
        }

        @Override
        protected void onPostExecute(@Nullable final Long trashId) {
            if (callback != null) { callback.post(trashId == null ? 0L : trashId); }
        }
    }
}
