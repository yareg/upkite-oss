package com.yareg.base.content;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.util.LongSparseArray;

import com.yareg.base.model.BaseModel;

public class BaseContentResolver {

    private static ContentResolver resolver;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    //
    // INSTANCE
    // https://en.wikipedia.org/wiki/Initialization_on_demand_holder_idiom
    //
    //----------------------------------------------------------------------------------------------

    private static class Singleton {
        static final BaseContentResolver INSTANCE = new BaseContentResolver();
    }

    @NonNull
    public static BaseContentResolver getInstance() {
        return BaseContentResolver.Singleton.INSTANCE;
    }

    public static void setContentResolver(@NonNull final ContentResolver contentResolver) {
        resolver = contentResolver;
    }

    /*private BaseContentResolver(@NonNull final ContentResolver resolver) {
        this.resolver = resolver;
    }*/

    /*@NonNull
    public static BaseContentResolver getInstance(@NonNull final Context context) {
        return new BaseContentResolver(context.getContentResolver());
    }*/

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- GET -------------------------------------------------------------------------------------

    public <T extends BaseModel<T>> void get(
            @NonNull final T prototype,
            @NonNull final BaseContentTasks.GetCallback<T> callback) {
        new BaseContentTasks.GetTask<>(resolver, prototype, callback).execute();
    }

    // --- SET -------------------------------------------------------------------------------------

    public <T extends BaseModel<T>> void set(
            @NonNull final T item,
            @Nullable final BaseContentTasks.SetCallback callback) {
        new BaseContentTasks.SetTask<>(resolver, item, callback).execute();
    }

    // --- LIST ------------------------------------------------------------------------------------

    public <T extends BaseModel<T>> void list(
            @NonNull final T prototype,
            @NonNull final BaseContentTasks.ListCallback<LongSparseArray<T>> callback,
            @Nullable final String selection,
            @Nullable final String[] selectionArgs) {
        new BaseContentTasks.ListTask<>(
                resolver, prototype, callback, selection, selectionArgs
        ).execute();
    }

    // --- REGISTER CONTENT OBSERVER ---------------------------------------------------------------

    public void registerContentObserver(@NonNull final Uri uri, @NonNull ContentObserver observer) {
        resolver.registerContentObserver(uri, false, observer);
    }

    // --- UNREGISTER CONTENT OBSERVER -------------------------------------------------------------

    public void unregisterContentObserver(@NonNull final ContentObserver observer) {
        resolver.unregisterContentObserver(observer);
    }

    // --- GET BY PROTOTYPE ------------------------------------------------------------------------

    @Nullable
    private <T extends BaseModel<T>> T getByPrototype(@NonNull final T prototype) {

        final Uri uri = prototype.baseUri
                .buildUpon()
                .appendPath(Long.toString(prototype.id))
                .build();

        final Cursor cursor = resolver.query(
                uri,
                null,
                null,
                null,
                null
        );

        if (cursor != null) {
            if (cursor.moveToNext()) {
                final T item = prototype.create(cursor);
                cursor.close();
                return item;
            }
            cursor.close();
        }
        return null;
    }

    // --- COMPLETE PROTOTYPE ----------------------------------------------------------------------

    @NonNull
    public <T extends BaseModel<T>> T completePrototype(@NonNull final T prototype) {
        final T item = getByPrototype(prototype);
        return (item == null) ? prototype : item;
    }

    // --- GET ALL IDS -----------------------------------------------------------------------------

/*    @NonNull
    public long[] getAllIds(@NonNull final Uri uri) {
        return getIds(uri, null, null);
    }*/

    // --- GET TRASH IDS ---------------------------------------------------------------------------

/*    @NonNull
    public long[] getTrashIds(@NonNull final Uri uri) {
        return getIds(uri, BaseModel.KEY_ID + "<?", new String[]{"0"});
    }*/

    // --- NOTIFY CHANGE ---------------------------------------------------------------------------

    /*
    public void notifyChange(@NonNull Uri uri) {
        service.notifyChange(uri, null, false);
    }
    */

    // --- TRASH -----------------------------------------------------------------------------------

    public void trash(
            @NonNull final Uri uri,
            final long id,
            @Nullable final BaseContentTasks.IdResult callback) {
        new BaseContentTasks.TrashTask(resolver, uri, id, callback).execute();
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE METHODS
    //
    //----------------------------------------------------------------------------------------------

/*    @NonNull
    private long[] getIds(
            @NonNull final Uri uri,
            @Nullable String selection,
            @Nullable String[] selectionArgs) {

        final Cursor cursor = service.query(
                uri,
                new String[]{ BaseModel.KEY_ID },
                selection,
                selectionArgs,
                null
        );

        if(cursor == null) { return new long[0]; }

        final long[] ids = new long[cursor.getCount()];

        int i = 0;
        while (cursor.moveToNext()) {
            ids[i] = cursor.getLong(cursor.getColumnIndex(BaseModel.KEY_ID));
            i++;
        }
        cursor.close();

        return ids;
    }*/
}
