package com.yareg.base.content;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yareg.base.manager.BaseDatabaseManager;
import com.yareg.base.model.DatabaseTable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class BaseContentProvider extends ContentProvider {

    private static final String INVALID_URI = "Invalid URI: ";

    private static final String LIMIT  = "limit";
    private static final String OFFSET = "offset";
    private static final String nullColumnhack = null;

    public static String AUTHORITY;
    public static String TYPE_DIR;
    public static String TYPE_ITEM;

    private final ThreadLocal<Boolean> isApplyingBatch = new ThreadLocal<>();
    private final Set<Uri>             changedUris     = new HashSet<>();

    //----------------------------------------------------------------------------------------------
    //
    // BASE CONTRACT
    //
    //----------------------------------------------------------------------------------------------

    public abstract static class BaseContract {

        /*
        Each value stored in an SQLite database (or manipulated by the database engine) has one of the following storage classes:
        INTEGER. The value is a signed integer, stored in 1, 2, 3, 4, 6, or 8 bytes depending on the magnitude of the value.
        REAL. The value is a floating point value, stored as an 8-byte IEEE floating point number.
        TEXT. The value is a text string, stored using the database encoding (UTF-8, UTF-16BE or UTF-16LE).
        BLOB. The value is a blob of data, stored exactly as it was input.
        NULL. The value is a NULL value.
        */

        protected static final String TYPE_INTEGER = "INTEGER";
        protected static final String TYPE_ID      = TYPE_INTEGER.concat(" PRIMARY KEY");
        protected static final String TYPE_REAL    = "REAL";
        protected static final String TYPE_TEXT    = "TEXT";
        //protected static final String TYPE_BLOB  = "BLOB";
        //protected static final String TYPE_NULL  = "NULL";

        //------------------------------------------------------------------------------------------
        //
        // BASE KEYS
        //
        //------------------------------------------------------------------------------------------

        public static final String KEY_ID    = BaseColumns._ID;
        public static final String KEY_VALUE = "value";

        //------------------------------------------------------------------------------------------
        //
        // BASE URI
        //
        //------------------------------------------------------------------------------------------

        static final Uri BASE_URI = Uri.parse("content://".concat(AUTHORITY));

        //------------------------------------------------------------------------------------------
        //
        // PUBLIC VARS
        //
        //------------------------------------------------------------------------------------------

        public final Uri uri;
        public final DatabaseTable table;

        //------------------------------------------------------------------------------------------
        //
        // CONSTRUCTOR
        //
        //------------------------------------------------------------------------------------------

        protected BaseContract(@NonNull final String name) {
            table = getTable(name);
            uri   = BASE_URI.buildUpon().appendEncodedPath(table.name).build();
        }

        protected DatabaseTable getTable(@NonNull final String name) {
            final DatabaseTable table = new DatabaseTable(name);
            table.put(KEY_ID, TYPE_ID);
            return table;
        }
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public boolean onCreate() {
        //Log.i(TAG, "authority: " + AUTHORITY + ", dir type: " + TYPE_DIR + ", item type: " + TYPE_ITEM);
        return true;
    }

    // --- GET TYPE --------------------------------------------------------------------------------

    @Override
    public String getType(@NonNull final Uri uri) {

        //------------------------------------------------------------------------------------------
        //
        // content://com.yareg.app.provider/table/
        // content://com.yareg.app.provider/table/id
        //
        //------------------------------------------------------------------------------------------

        if (uri.getPathSegments().size() == 0) {
            throw new UnsupportedOperationException(INVALID_URI.concat(uri.toString()));
        }

        final String name = uri.getPathSegments().get(0);

        if (uri.getPathSegments().size() > 1) {
            final long id = parseId(uri);
            if (id > 0) { return TYPE_ITEM.concat(name); }
        }

        return TYPE_DIR.concat(name);
    }

    // --- QUERY -----------------------------------------------------------------------------------

    @Override
    public Cursor query(
            @NonNull final Uri uri,
            @Nullable final String[] projection,
            @Nullable String selection,
            @Nullable String[] args,
            @Nullable final String sortOrder) {

        //------------------------------------------------------------------------------------------
        //
        // content://com.yareg.app.provider/table/
        // content://com.yareg.app.provider/table/id
        //
        //------------------------------------------------------------------------------------------

        if (uri.getPathSegments().size() == 0) {
            throw new UnsupportedOperationException(INVALID_URI + uri);
        }

        final SQLiteDatabase database = getReadableDatabase();
        final String         table    = uri.getPathSegments().get(0);

        if (database != null && tableExists(table)) {

            String limit = null;

            if (uri.getPathSegments().size() == 1) {
                limit = parseLimit(uri);
            } else {
                if (selection == null || args == null) {
                    selection = BaseColumns._ID.concat("=?");
                    args = new String[]{Long.toString(parseId(uri))};
                    limit = "0,1";
                }
            }

            return database.query(
                    table, projection, selection, args, null, null, sortOrder, limit
            );
        }

        return null;
    }

    // --- INSERT ----------------------------------------------------------------------------------

    @Override
    public Uri insert(@NonNull final Uri uri, @Nullable final ContentValues values) {

        //------------------------------------------------------------------------------------------
        //
        // content://com.yareg.app.provider/table
        //
        //------------------------------------------------------------------------------------------

        Uri createdUri;

        final SQLiteDatabase database = getWritableDatabase();
        if (database == null) { return null; }

        if (getIsApplyingBatch()) {
            createdUri = insertInTransaction(database, uri, values);
        } else {
            database.beginTransactionNonExclusive();
            try {
                createdUri = insertInTransaction(database, uri, values);
                database.setTransactionSuccessful();
            } finally {
                database.endTransaction();
            }
            notifyChanges();
        }

        return createdUri;
    }

    // --- UPDATE ----------------------------------------------------------------------------------

    @Override
    public int update(
            @NonNull final Uri uri,
            @Nullable final ContentValues values,
            @Nullable String selection,
            @Nullable String[] args) {

        //------------------------------------------------------------------------------------------
        //
        // content://com.yareg.app.provider/table/id
        //
        //------------------------------------------------------------------------------------------

        int rowsAffected = 0;

        final SQLiteDatabase database = getWritableDatabase();
        if (database == null) { return rowsAffected; }

        if (getIsApplyingBatch()) {
            rowsAffected = updateInTransaction(database, uri, values, selection, args);
        } else {
            database.beginTransactionNonExclusive();
            try {
                rowsAffected = updateInTransaction(database, uri, values, selection, args);
                database.setTransactionSuccessful();
            } finally {
                database.endTransaction();
            }
            notifyChanges();
        }

        return rowsAffected;
    }

    // --- DELETE ----------------------------------------------------------------------------------

    @Override
    public int delete(@NonNull final Uri uri, @Nullable String selection, @Nullable String[] args) {

        //------------------------------------------------------------------------------------------
        //
        // content://com.yareg.app.provider/table/id
        //
        //------------------------------------------------------------------------------------------

        int rowsAffected = 0;

        final SQLiteDatabase database = getWritableDatabase();
        if (database == null) { return rowsAffected; }

        if (getIsApplyingBatch()) {
            rowsAffected = deleteInTransaction(database, uri, selection, args);
        } else {
            database.beginTransactionNonExclusive();
            try {
                rowsAffected = deleteInTransaction(database, uri, selection, args);
                database.setTransactionSuccessful();
            } finally {
                database.endTransaction();
            }
            notifyChanges();
        }

        return rowsAffected;
    }

    // --- APPLY BATCH -----------------------------------------------------------------------------

    @Override
    public @NonNull ContentProviderResult[] applyBatch(
            @NonNull ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException {

        final int operationCount = operations.size();
        if (operationCount == 0) {
            return new ContentProviderResult[0];
        }

        final SQLiteDatabase database = getWritableDatabase();
        if (database == null) { return new ContentProviderResult[0]; }

        isApplyingBatch.set(true);

        database.beginTransactionNonExclusive();

        final ContentProviderResult[] results = new ContentProviderResult[operationCount];

        try {
            for (int i = 0; i < operationCount; i++) {
                operations.get(i).apply(this, results, i);
            }
            database.setTransactionSuccessful();
        } finally {
            isApplyingBatch.set(false);
            database.endTransaction();
            notifyChanges();
        }

        return results;
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- GET READABLE DATABASE -------------------------------------------------------------------

    @Nullable
    private SQLiteDatabase getReadableDatabase() {
        final Context context = getContext();
        if (context != null) {
            return BaseDatabaseManager.getInstance(context).getReadableDatabase();
        }
        return null;
    }

    // --- GET WRITABLE DATABASE -------------------------------------------------------------------

    @Nullable
    private SQLiteDatabase getWritableDatabase() {
        final Context context = getContext();
        if (context != null) {
            return BaseDatabaseManager.getInstance(getContext()).getWritableDatabase();
        }
        return null;
    }

    // --- TABLE EXISTS ----------------------------------------------------------------------------

    private boolean tableExists(@NonNull final String tableName) {
        final Context context = getContext();
        if (context != null) {
            return BaseDatabaseManager.getInstance(getContext()).tableExists(tableName);
        }
        return false;
    }

    // --- PARSE ID --------------------------------------------------------------------------------

    private static long parseId(@NonNull final Uri uri) {

        long id = 0;
        final int segments = uri.getPathSegments().size();

        if (segments > 1) {
            try {
                id = Long.parseLong(uri.getPathSegments().get(segments - 1));
            } catch (NumberFormatException e) {
                return id;
            }
        }

        return id;
    }

    // --- PARSE LIMIT -----------------------------------------------------------------------------

    @Nullable
    private static String parseLimit(@NonNull final Uri uri) {

        final String offset = uri.getQueryParameter(OFFSET);
        final String limit  = uri.getQueryParameter(LIMIT);

        if (limit  == null) { return null; }
        if (offset == null) { return "0,".concat(limit); }

        return offset.concat(",".concat(limit));
    }

    // --- GET IS APPLYING BATCH -------------------------------------------------------------------

    private boolean getIsApplyingBatch() {
        final Boolean value = isApplyingBatch.get();
        return value == null ? false : value;
    }

    // --- NOTIFY CHANGE ---------------------------------------------------------------------------

    private void notifyChanges() {
        final Context context = getContext();
        if (context != null) {

            Set<Uri> changes;
            synchronized (changedUris) {
                changes = new HashSet<>(changedUris);
                changedUris.clear();
            }

            if(changes.size() == 0) { return; }

            final ContentResolver resolver = context.getContentResolver();

            for (final Uri uri : changes) {
                resolver.notifyChange(uri, null);
                //Log.i("BaseContentProvider", "change: " + uri.toString());
            }
        }
    }

    // --- INSERT IN TRANSACTION -------------------------------------------------------------------

    @NonNull
    private Uri insertInTransaction(
            @NonNull final SQLiteDatabase database,
            @NonNull final Uri uri,
            @Nullable final ContentValues values) throws UnsupportedOperationException {

        if (uri.getPathSegments().size() == 0) {
            throw new UnsupportedOperationException(INVALID_URI.concat(uri.toString()));
        }

        if (values == null) {
            throw new IllegalArgumentException("content values is null");
        }

        final String table = uri.getPathSegments().get(0);

        if (tableExists(table)) {

            final long id = database.replace(table, nullColumnhack, values);
            final Uri createdUri = uri.buildUpon().appendPath(Long.toString(id)).build();

            //changedUris.add(createdUri);
            changedUris.add(uri);

            return createdUri;

        } else {
            throw new UnsupportedOperationException("table '" + table + "' doesn't exist");
        }
    }

    // --- UPDATE IN TRANSACTION -------------------------------------------------------------------

    private int updateInTransaction(
            @NonNull final SQLiteDatabase database,
            @NonNull final Uri uri,
            @Nullable final ContentValues values,
            @Nullable String selection,
            @Nullable String[] args) throws UnsupportedOperationException {

        if (uri.getPathSegments().size() == 0) {
            throw new UnsupportedOperationException(INVALID_URI.concat(uri.toString()));
        }

        if (values == null) {
            throw new IllegalArgumentException("content values is null");
        }

        final String table = uri.getPathSegments().get(0);

        if (tableExists(table)) {

            if(selection == null || args == null) {
                selection = BaseColumns._ID.concat("=?");
                args = new String[] { Long.toString(parseId(uri)) };
            }

            final int rowsAffected = database.update(table, values, selection, args);

            if (rowsAffected < 1) { return 0; }

            changedUris.add(uri);

            return rowsAffected;

        } else {
            throw new UnsupportedOperationException("table '" + table + "' doesn't exist");
        }
    }

    // --- DELETE IN TRANSACTION -------------------------------------------------------------------

    private int deleteInTransaction(
            @NonNull final SQLiteDatabase database,
            @NonNull final Uri uri,
            @Nullable String selection,
            @Nullable String[] args) throws UnsupportedOperationException {

        if (uri.getPathSegments().size() == 0) {
            throw new UnsupportedOperationException(INVALID_URI + uri);
        }

        final String table = uri.getPathSegments().get(0);

        if (tableExists(table)) {

            if (selection == null || args == null) {
                selection = BaseColumns._ID.concat("=?");
                args = new String[] { Long.toString(parseId(uri)) };
            }

            final int rowsAffected = database.delete(table, selection, args);

            if (rowsAffected < 1) { return 0; }

            changedUris.add(uri);

            return rowsAffected;

        } else {
            throw new UnsupportedOperationException("table '" + table + "' doesn't exist");
        }
    }
}
