package com.yareg.base.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.util.SparseBooleanArray;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

public class BaseDrawerLayout extends DrawerLayout {

    private static final int GRAVITY = GravityCompat.START;

    private final SparseBooleanArray touches = new SparseBooleanArray(10);

    private final SimpleDrawerListener drawerListener = new SimpleDrawerListener() {
        @Override
        public void onDrawerClosed(View drawerView) {
            super.onDrawerClosed(drawerView);
            touches.clear();
        }
    };

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public BaseDrawerLayout(@NonNull Context context) {
        super(context);
        init();
    }

    public BaseDrawerLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs, 0);
        init();
    }

    //----------------------------------------------------------------------------------------------
    //
    // SUPER OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        switch(ev.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                touches.put(0, true);
                break;

            case MotionEvent.ACTION_POINTER_DOWN:
                touches.put(ev.getPointerId(ev.getActionIndex()), true);
                break;

            case MotionEvent.ACTION_MOVE:
                if(!touches.get(ev.getPointerId(ev.getActionIndex()))) { return false; }
                break;

            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                final int pointerId = ev.getPointerId(ev.getActionIndex());
                if(!touches.get(pointerId)) { return false; }
                touches.put(pointerId, false);
                break;
        }
        return super.onInterceptTouchEvent(ev);
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- IS DRAWER OPEN --------------------------------------------------------------------------

    public boolean isDrawerOpen() { return isDrawerOpen(GRAVITY); }

    // --- IS DRAWER LOCKED ------------------------------------------------------------------------

    public boolean isDrawerLocked() {
        final int lockMode = getDrawerLockMode(GRAVITY);
        return (lockMode == DrawerLayout.LOCK_MODE_LOCKED_CLOSED) ||
                (lockMode == DrawerLayout.LOCK_MODE_LOCKED_OPEN);
    }

    // --- CLOSE DRAWER ----------------------------------------------------------------------------

    public boolean closeDrawer() {
        if(isDrawerOpen()) {
            closeDrawer(GRAVITY, true);
            return true;
        }
        return false;
    }

    // --- LOCK DRAWER -----------------------------------------------------------------------------

    public void lockDrawer(final boolean isLocked) {
        setDrawerLockMode(
                isLocked ?
                        (isDrawerOpen() ?
                                DrawerLayout.LOCK_MODE_LOCKED_OPEN :
                                DrawerLayout.LOCK_MODE_LOCKED_CLOSED) :
                        DrawerLayout.LOCK_MODE_UNLOCKED
        );
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE METHODS
    //
    //----------------------------------------------------------------------------------------------

    private void init() {
        //setScrimColor(0x66000000);
        addDrawerListener(drawerListener);
    }
}
