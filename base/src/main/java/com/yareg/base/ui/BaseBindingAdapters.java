package com.yareg.base.ui;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;

public class BaseBindingAdapters {

    @BindingAdapter("isOn")
    public static void isOn(@NonNull final View view, @NonNull final Boolean isOn) {
        view.setVisibility(isOn ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter("onClick")
    public static void onClick(
            @NonNull final View view,
            @NonNull final View.OnClickListener onClickListener) {
        if(view.hasOnClickListeners()) { return; }
        view.setOnClickListener(onClickListener);
    }

    @BindingAdapter("onEditorAction")
    public static void onEditorAction(
            @NonNull final TextView view,
            @NonNull final TextView.OnEditorActionListener listener) {
        view.setOnEditorActionListener(listener);
    }
}
