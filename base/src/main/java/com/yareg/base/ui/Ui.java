package com.yareg.base.ui;

import android.app.Application;
import android.transition.Fade;
import android.util.SparseArray;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.yareg.base.fragment.FragmentItem;
import com.yareg.base.fragment.FragmentStack;
import com.yareg.base.model.FabData;

public class Ui extends AndroidViewModel {

    //----------------------------------------------------------------------------------------------
    //
    // STATIC
    //
    //----------------------------------------------------------------------------------------------

    public static FragmentItem START_FRAGMENT;

    public static final int FRAME_MILLIS = 16;

    private static final int  FADE_DURATION = 32;
    public  static final Fade FADE_IN       = new Fade();
    public  static final Fade FADE_OUT      = new Fade();

    static {
        FADE_IN.setStartDelay(FADE_DURATION / 2);
        FADE_IN.setDuration(FADE_DURATION);
        FADE_OUT.setDuration(FADE_DURATION);
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE
    //
    //----------------------------------------------------------------------------------------------

    private final FragmentStack fragmentStack;

    private final MutableLiveData<FabData> fabData      = new MutableLiveData<>();
    private final MutableLiveData<Boolean> isFabEnabled = new MutableLiveData<>();

    public int optionsMenu = 0;

    public boolean isDrawerOpen     = false;
    public boolean forceCloseDrawer = false;

    private final SparseArray<Runnable> fabActions = new SparseArray<>();

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public Ui(@NonNull final Application application) {
        super(application);
        fragmentStack = FragmentStack.getInstance();
        isFabEnabled.setValue(true);
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- FRAGMENTS -------------------------------------------------------------------------------

    public FragmentStack getFragmentStack() { return fragmentStack; }

    public void setStartFragment() { fragmentStack.init(START_FRAGMENT); }

    public void pushFragment(@NonNull final FragmentItem item) {
        forceCloseDrawer = isDrawerOpen;
        fragmentStack.push(item);
    }

    public void pushFragment(@NonNull final FragmentItem item, boolean isBackEnabled) {
        forceCloseDrawer = isDrawerOpen;
        fragmentStack.push(item, isBackEnabled);
    }

    public void setFragmentData(@NonNull final FragmentItem item) {
        final FabData fd   = fabData.getValue();
        final int hashCode = fd == null ? 0 : fd.hashCode();
        fabData.setValue(item.fabData);
        if (item.fabData != null && hashCode != item.fabData.hashCode()) { setFabEnabled(true); }
    }

    // --- FAB -------------------------------------------------------------------------------------

    public LiveData<FabData> getFabData()      { return fabData; }
    public LiveData<Boolean> getIsFabEnabled() { return isFabEnabled; }
    
    public void setFabEnabled(@NonNull final Boolean isEnabled) {
        if (isFabEnabled.getValue() == null || isEnabled != isFabEnabled.getValue()) {
            isFabEnabled.setValue(isEnabled);
        }
    }

    public void addFabAction(final int id, @NonNull final Runnable action) {
        fabActions.put(id, action);
    }

    public void runFabAction() {
        final FragmentItem item = fragmentStack.getValue();
        if (item == null || fabActions.size() == 0 || fabActions.get(item.id) == null) {
            return;
        }
        fabActions.get(item.id).run();
    }

    public void removeFabAction(final int id) {
        fabActions.remove(id);
    }
}
