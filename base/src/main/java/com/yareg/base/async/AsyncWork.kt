package com.yareg.base.async

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

abstract class AsyncWork<I, O> {

    private var result: O? = null

    protected abstract fun doInBackground(vararg params: I): O
    protected open fun onPostExecute(result: O?) {}

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    fun <T> execute(vararg input: I) {
        GlobalScope.launch(Dispatchers.Main) { doWork(*input) }
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE METHODS
    //
    //----------------------------------------------------------------------------------------------

    private suspend fun doWork(vararg input: I) {
        withContext(Dispatchers.IO) { result = doInBackground(*input) }
        GlobalScope.launch(Dispatchers.Main) { onPostExecute(result) }
    }
}