package com.yareg.base.model;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

public class SnackbarData {

    @StringRes
    public final int stringRes;

    @StringRes
    public final int actionRes;

    public final View.OnClickListener action;

    public SnackbarData(@StringRes final int stringRes) {
        this.stringRes = stringRes;
        this.actionRes = 0;
        this.action    = null;
    }

    public SnackbarData(@StringRes final int stringRes,
                        @StringRes final int actionRes,
                        @NonNull final View.OnClickListener action) {
        this.stringRes = stringRes;
        this.actionRes = actionRes;
        this.action    = action;
    }
}
