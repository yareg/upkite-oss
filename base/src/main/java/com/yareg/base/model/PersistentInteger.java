package com.yareg.base.model;

import android.content.ContentValues;
import android.database.Cursor;

import androidx.annotation.NonNull;

import com.yareg.base.content.BaseContentProvider;

public class PersistentInteger extends BaseModel<PersistentInteger> {

    //----------------------------------------------------------------------------------------------
    //
    // DATABASE CONTRACT
    //
    //----------------------------------------------------------------------------------------------

    public static class Contract extends BaseContentProvider.BaseContract {
        Contract() { super("integer"); }

        @Override
        protected DatabaseTable getTable(@NonNull final String name) {
            final DatabaseTable table = super.getTable(name);
            table.put(KEY_VALUE, TYPE_INTEGER);
            return table;
        }
    }

    public static final Contract contract = new Contract();

    //----------------------------------------------------------------------------------------------
    //
    // PROPERTIES
    //
    //----------------------------------------------------------------------------------------------

    public long value;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTORS
    //
    //----------------------------------------------------------------------------------------------

    public PersistentInteger() { super(contract.uri); }

    public PersistentInteger(final long id) { super(contract.uri, id); }

    public PersistentInteger(final long key, final long value) {
        super(contract.uri, key);
        this.value = value;
    }

    public PersistentInteger(final long key, final int value) {
        super(contract.uri, key);
        this.value = value;
    }

    public PersistentInteger(final long key, final boolean value) {
        super(contract.uri, key);
        this.value = value ? 1 : 0;
    }

    private PersistentInteger(@NonNull final Cursor cursor) {
        super(contract.uri, cursor);
        value = cursor.getLong(cursor.getColumnIndex(Contract.KEY_VALUE));
    }

    //----------------------------------------------------------------------------------------------
    //
    // FACTORY IMPLEMENTATION
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public PersistentInteger create(final long id) { return new PersistentInteger(id); }

    @Override
    public PersistentInteger create(@NonNull final Cursor cursor) { return new PersistentInteger(cursor); }

    //----------------------------------------------------------------------------------------------
    //
    // CONTENT VALUES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public ContentValues getContentValues() {
        ContentValues values = super.getContentValues();
        values.put(Contract.KEY_VALUE, value);
        return values;
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    public int asInt() { return (int) value; }
    public boolean asBoolean() { return (value > 0); }
}
