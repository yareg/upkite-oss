package com.yareg.base.model;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

public class FabData {

    public final int iconResId;
    public final int textResId;

    public FabData(@DrawableRes final int iconResId, @StringRes final int textResId) {
        this.iconResId = iconResId;
        this.textResId = textResId;
    }
}
