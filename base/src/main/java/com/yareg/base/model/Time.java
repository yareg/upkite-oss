package com.yareg.base.model;

import android.content.Context;
import android.text.format.DateUtils;

import androidx.annotation.NonNull;

import com.yareg.base.R;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.Locale;
import java.util.Date;

public abstract class Time {

    public static final long SECOND = DateUtils.SECOND_IN_MILLIS;
    public static final long MINUTE = DateUtils.MINUTE_IN_MILLIS;
    public static final long HOUR   = DateUtils.HOUR_IN_MILLIS;
    public static final long DAY    = DateUtils.DAY_IN_MILLIS;
    public static final long WEEK   = DateUtils.WEEK_IN_MILLIS;
    public static final long YEAR   = DateUtils.YEAR_IN_MILLIS;

    public static class Pattern {
        public static final String UTC           = "YYYY-MM-dd'T'HH:mm:ss.SSSXXX";
        public static final String DEFAULT       = "yyyy-MM-dd HH:mm:ss";
        public static final String DATE          = "yyyy-MM-dd";
        public static final String HOURS_MINUTES = "HH:mm";
        public static final String DAY           = "d";
        public static final String MONTH_FULL    = "MMMM";
        //public static final String SHORT_DATE    = MONTH_FULL + " " + DAY;
        public static final String THIS_YEAR     = "d MMMM, HH:mm";
        public static final String FULL          = "d MMMM yyyy, HH:mm";
    }

    public static final String ZONE_UTC = "UTC";
    public static final String ZONE_GMT = "GMT";

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    public static TimeZone getUtc() {
        return TimeZone.getTimeZone(Time.ZONE_UTC);
    }

    //----------------------------------------------------------------------------------------------
    //
    // EPOCH TIME
    //
    //----------------------------------------------------------------------------------------------

    // --- GET UNIX TIME ---------------------------------------------------------------------------

    public static long getUnixTime() { return (System.currentTimeMillis()); }

    public static long getUnixTime(
            @NonNull final String timestamp,
            @NonNull final TimeZone timeZone) {
        return getUnixTime(timestamp, Pattern.DEFAULT, timeZone);
    }

    public static long getUnixTime(
            @NonNull final String timestamp,
            @NonNull final String pattern,
            @NonNull final TimeZone timeZone) {
        try {
            final SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
            dateFormat.setTimeZone(timeZone);
            final Date date = dateFormat.parse(timestamp);
            return date == null ? 0 : date.getTime();
        } catch (Exception e) {
            return 0;
        }
    }

    //----------------------------------------------------------------------------------------------
    //
    // DATE
    //
    //----------------------------------------------------------------------------------------------

    //--- GET DATE ---------------------------------------------------------------------------------

    @NonNull
    public static String getDate(final long unixTime, final @NonNull String pattern) {
        try{
            final SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
            dateFormat.setTimeZone(TimeZone.getDefault());
            return dateFormat.format(new Date(unixTime));
        } catch(Exception e) {
            return pattern;
        }
    }

    @NonNull
    public static String getDate(@NonNull final Context context, final long unixTime) {
        final long timeSpan = getUnixTime() - unixTime;

        if(Math.abs(timeSpan) < DAY) {
            final String time = getDate(unixTime, Pattern.HOURS_MINUTES);
            return context.getString(
                    isToday(unixTime) ?
                            R.string.today_s :
                            (timeSpan < 0) ? R.string.tomorrow_s : R.string.yesterday_s,
                    time
            );
        }
        return getDate(unixTime, isThisYear(unixTime) ? Pattern.THIS_YEAR : Pattern.FULL);
    }

    /*
    @NonNull
    public static String getDate(final long unixTime) {
        if(isToday(unixTime)) { return getDate(unixTime, Pattern.HOURS_MINUTES); }
        return getDate(unixTime, isThisYear(unixTime) ? Pattern.THIS_YEAR : Pattern.FULL);
    }
    */

    //--- GET SINCE --------------------------------------------------------------------------------

    @NonNull
    public static String getSince(final long unixTime) {

        final long timeSpan = Math.abs(getUnixTime() - unixTime);

        long resolution = WEEK;

        if(timeSpan < MINUTE)    { resolution = 0; }
        else if(timeSpan < HOUR) { resolution = MINUTE; }
        else if(timeSpan < DAY)  { resolution = HOUR; }
        else if(timeSpan < WEEK) { resolution = DAY; }

        return DateUtils
                .getRelativeTimeSpanString(unixTime, getUnixTime(), resolution)
                .toString();
    }

    //--- GET IN THIS MONTH ------------------------------------------------------------------------

/*
    @NonNull
    public static String getInThisMonth(@NonNull final Context context) {
        switch(new GregorianCalendar().get(GregorianCalendar.MONTH)) {
            case 0:  return context.getString(R.string.in_01_month);
            case 1:  return context.getString(R.string.in_02_month);
            case 2:  return context.getString(R.string.in_03_month);
            case 3:  return context.getString(R.string.in_04_month);
            case 4:  return context.getString(R.string.in_05_month);
            case 5:  return context.getString(R.string.in_06_month);
            case 6:  return context.getString(R.string.in_07_month);
            case 7:  return context.getString(R.string.in_08_month);
            case 8:  return context.getString(R.string.in_09_month);
            case 9:  return context.getString(R.string.in_10_month);
            case 10: return context.getString(R.string.in_11_month);
            case 11: return context.getString(R.string.in_12_month);
            default: return context.getString(R.string.in_this_month);
        }
    }
*/

    //--- GET IN MONTH -----------------------------------------------------------------------------

    @NonNull
    public static String getInMonth(@NonNull final Context context, final long unixTime) {

        final GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(unixTime);

        switch(calendar.get(GregorianCalendar.MONTH)) {
            case 0:  return context.getString(R.string.in_01_month);
            case 1:  return context.getString(R.string.in_02_month);
            case 2:  return context.getString(R.string.in_03_month);
            case 3:  return context.getString(R.string.in_04_month);
            case 4:  return context.getString(R.string.in_05_month);
            case 5:  return context.getString(R.string.in_06_month);
            case 6:  return context.getString(R.string.in_07_month);
            case 7:  return context.getString(R.string.in_08_month);
            case 8:  return context.getString(R.string.in_09_month);
            case 9:  return context.getString(R.string.in_10_month);
            case 10: return context.getString(R.string.in_11_month);
            case 11: return context.getString(R.string.in_12_month);
            default: return context.getString(R.string.in_this_month);
        }
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- IS TODAY --------------------------------------------------------------------------------

    private static boolean isToday(final long unixTime) {
        final GregorianCalendar calendar = new GregorianCalendar();
        final int day   = calendar.get(GregorianCalendar.DAY_OF_MONTH);
        final int month = calendar.get(GregorianCalendar.MONTH);
        final int year  = calendar.get(GregorianCalendar.YEAR);
        calendar.setTimeInMillis(unixTime);
        return (
                day   == calendar.get(GregorianCalendar.DAY_OF_MONTH) &&
                month == calendar.get(GregorianCalendar.MONTH) &&
                year  == calendar.get(GregorianCalendar.YEAR)
        );
    }

    // --- IS THIS YEAR ----------------------------------------------------------------------------

    private static boolean isThisYear(final long unixTime) {
        final GregorianCalendar calendar = new GregorianCalendar();
        final int year = calendar.get(GregorianCalendar.YEAR);
        calendar.setTimeInMillis(unixTime);
        return (year == calendar.get(GregorianCalendar.YEAR));
    }
}