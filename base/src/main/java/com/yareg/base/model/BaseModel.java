package com.yareg.base.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import com.yareg.base.content.BaseContentProvider.BaseContract;

import androidx.annotation.NonNull;

import java.lang.reflect.Field;
import java.util.Objects;

public abstract class BaseModel<T> {

    public static final String KEY_ID = BaseContract.KEY_ID;

    private static long LAST_GENERATED_ID;

    public final Uri  baseUri;
    public final long id;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTORS
    //
    //----------------------------------------------------------------------------------------------

    protected BaseModel(@NonNull final Uri baseUri) {
        this.baseUri = baseUri;
        id = Math.max(Time.getUnixTime(), LAST_GENERATED_ID++);
        LAST_GENERATED_ID = id;
    }

    protected BaseModel(@NonNull final Uri baseUri, final long id) {
        this.baseUri = baseUri;
        this.id = id;
    }

    protected BaseModel(@NonNull final Uri baseUri, @NonNull final Cursor cursor) {
        this.baseUri = baseUri;
        id = cursor.getLong(cursor.getColumnIndex(KEY_ID));
    }

    //----------------------------------------------------------------------------------------------
    //
    // FACTORY
    //
    //----------------------------------------------------------------------------------------------

    public abstract T create(final long id);
    public abstract T create(@NonNull final Cursor cursor);

    //----------------------------------------------------------------------------------------------
    //
    // CONTENT VALUES
    //
    //----------------------------------------------------------------------------------------------

    public ContentValues getContentValues() {
        final ContentValues values = new ContentValues();
        values.put(KEY_ID, id);
        return values;
    }

    //----------------------------------------------------------------------------------------------
    //
    // VALIDITY
    //
    //----------------------------------------------------------------------------------------------

    public boolean isValid() {
        return (id > 0);
    }

    //----------------------------------------------------------------------------------------------
    //
    // IS CHANGED
    //
    //----------------------------------------------------------------------------------------------

    public static <T> boolean isChanged(@NonNull final T parent, @NonNull final T child) {
        for (final Field field : parent.getClass().getDeclaredFields()) {
            try {
                if (!Objects.equals(field.get(parent), field.get(child))) {
                    return true;
                }
            } catch (IllegalAccessException e) {
                return false;
            }
        }
        return false;
    }
}
