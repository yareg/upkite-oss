package com.yareg.base.model;

import androidx.annotation.NonNull;

import java.util.HashMap;

public class DatabaseTable extends HashMap<String, String> {

    public final String name;

    public DatabaseTable(@NonNull final String name) {
        this.name = name;
    }
}
