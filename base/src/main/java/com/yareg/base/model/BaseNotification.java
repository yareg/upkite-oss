package com.yareg.base.model;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;

import androidx.annotation.NonNull;

public class BaseNotification {

    private static final boolean hasTitle = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N);

    public final int     id;
    public final String  channelId;

    private int           icon;
    private String        title;
    private String        text;
    private PendingIntent intent;
    private Bitmap        largeIcon;

    protected BaseNotification(
            final int id,
            final String channelId,
            final int icon,
            @NonNull String appName,
            @NonNull String title,
            @NonNull String text) {
        this.id        = id;
        this.channelId = channelId;
        this.icon      = icon;
        this.title     = hasTitle ? title : appName;
        this.text      = text;
    }

    public int           getIcon()      { return icon; }
    public String        getTitle()     { return title; }
    public String        getText()      { return text; }
    public PendingIntent getIntent()    { return intent; }
    public Bitmap        getLargeIcon() { return largeIcon; }

    public void setIcon(final int value)              { icon = value; }
    public void setTitle(@NonNull final String value) { title = value; }
    public void setText(@NonNull final String value)  { text = value; }

    public void setIntent(@NonNull final Context context, @NonNull Intent intent) {
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        this.intent = PendingIntent.getActivity(
                context,
                0,
                intent,
                PendingIntent.FLAG_IMMUTABLE
        );
    }

    public void setLargeIcon(@NonNull final Bitmap value) { largeIcon = value; }
}
