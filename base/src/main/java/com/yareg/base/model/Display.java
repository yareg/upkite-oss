package com.yareg.base.model;

import android.content.Context;
import android.util.DisplayMetrics;

abstract class Display {

    public static int getPxFromDp(final Context context, final int dp) {
        
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static float getWidthDp(final Context context)
    {
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (displayMetrics.widthPixels / displayMetrics.density);
    }

    public static float getHeightDp(final Context context)
    {
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (displayMetrics.heightPixels / displayMetrics.density);
    }

}
