package com.yareg.base.model;

import android.content.Context;

import androidx.annotation.NonNull;

import com.yareg.base.R;

import java.util.Locale;

public abstract class Text {

    private static final String DOLLAR_FORMAT  = "$%.2f";

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    @NonNull
    public static String asDollars(final float value) {
        return ((value < 0) ? "-" : "") + String.format(
                Locale.getDefault(), DOLLAR_FORMAT, Math.abs(value)
        );
    }

    @NonNull
    public static String asGB(@NonNull final Context context, final float value) {
        return context.getString(R.string.as_gb, value);
    }
}
