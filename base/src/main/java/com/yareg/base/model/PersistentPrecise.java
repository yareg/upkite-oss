package com.yareg.base.model;

import android.content.ContentValues;
import android.database.Cursor;

import androidx.annotation.NonNull;

import com.yareg.base.content.BaseContentProvider;

public class PersistentPrecise extends BaseModel<PersistentPrecise> {

    //----------------------------------------------------------------------------------------------
    //
    // DATABASE CONTRACT
    //
    //----------------------------------------------------------------------------------------------

    public static class Contract extends BaseContentProvider.BaseContract {
        Contract() { super("precise"); }

        @Override
        protected DatabaseTable getTable(@NonNull final String name) {
            final DatabaseTable table = super.getTable(name);
            table.put(KEY_VALUE, TYPE_REAL);
            return table;
        }
    }

    public static final Contract contract = new Contract();

    //----------------------------------------------------------------------------------------------
    //
    // PROPERTIES
    //
    //----------------------------------------------------------------------------------------------

    public double value;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTORS
    //
    //----------------------------------------------------------------------------------------------

    public PersistentPrecise() { super(contract.uri); }

    public PersistentPrecise(final long id) { super(contract.uri, id); }

    public PersistentPrecise(final long key, final float value) {
        super(contract.uri, key);
        this.value = value;
    }

    private PersistentPrecise(@NonNull final Cursor cursor) {
        super(contract.uri, cursor);
        value = cursor.getDouble(cursor.getColumnIndex(Contract.KEY_VALUE));
    }

    //----------------------------------------------------------------------------------------------
    //
    // FACTORY IMPLEMENTATION
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public PersistentPrecise create(final long id) { return new PersistentPrecise(id); }

    @Override
    public PersistentPrecise create(@NonNull final Cursor cursor) { return new PersistentPrecise(cursor); }

    //----------------------------------------------------------------------------------------------
    //
    // CONTENT VALUES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public ContentValues getContentValues() {
        final ContentValues values = super.getContentValues();
        values.put(Contract.KEY_VALUE, value);
        return values;
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    public float asFloat() { return (float) value; }
}
