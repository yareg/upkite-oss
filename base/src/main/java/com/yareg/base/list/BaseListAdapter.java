package com.yareg.base.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseListAdapter<T>
        extends RecyclerView.Adapter<BaseListAdapter<T>.ViewHolder> {

    private final int           layout;
    private OnItemClickListener listener;
    private final List<T>       dataset = new ArrayList<>();

    //----------------------------------------------------------------------------------------------
    //
    // INTERFACE
    //
    //----------------------------------------------------------------------------------------------

    public interface OnItemClickListener {
        void onItemClicked(final int position);
    }

    public void setOnItemClickListener(@NonNull final OnItemClickListener clickListener) {
        listener = clickListener;
    }

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    protected BaseListAdapter(int layout, @Nullable final List<T> items) {
        this.layout = layout;
        if (items != null) { dataset.addAll(items); }
    }

    //----------------------------------------------------------------------------------------------
    //
    // VIEW HOLDER
    //
    //----------------------------------------------------------------------------------------------

    protected class ViewHolder
            extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        public ViewHolder(@NonNull final View view) {
            super(view);
            if (listener != null) { view.setOnClickListener(this); }
        }

        @Override
        public void onClick(@NonNull final View view) {
            if (listener != null) { listener.onItemClicked(getLayoutPosition()); }
        }

        protected void bind(final int position) {
            final ViewDataBinding binding = DataBindingUtil.bind(itemView);
            if (binding != null) {
                onBindItemData(binding, getItem(position));
                binding.executePendingBindings();
            }
        }
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int viewType) {
        return new ViewHolder(
                LayoutInflater
                        .from(viewGroup.getContext())
                        .inflate(layout, viewGroup, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {
        viewHolder.bind(position);
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    public void updateList(@NonNull final List<T> items) {

        final DiffUtil.DiffResult result = DiffUtil.calculateDiff(getDiff(items));

        dataset.clear();
        dataset.addAll(items);

        result.dispatchUpdatesTo(this);
    }

    public T getItem(final int position) { return dataset.get(position); }

    //----------------------------------------------------------------------------------------------
    //
    // PROTECTED METHODS
    //
    //----------------------------------------------------------------------------------------------

    protected List<T> getList() { return dataset; }

    protected DiffUtil.Callback getDiff(@NonNull final List<T> items) {
        return new BaseListDiff<>(dataset, items);
    }

    protected abstract void onBindItemData(final ViewDataBinding binding, final T data);
}
