package com.yareg.base.list;

import androidx.annotation.NonNull;

import com.yareg.base.model.BaseModel;

import java.util.List;

public class BaseModelListDiff<T extends BaseModel<T>> extends BaseListDiff<T> {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public BaseModelListDiff(@NonNull final List<T> oldList, @NonNull final List<T> newList) {
        super(oldList, newList);
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public boolean areItemsTheSame(final int oldPosition, final int newPosition) {
        return (oldList.get(oldPosition).id == newList.get(newPosition).id);
    }
}
