package com.yareg.base.list;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.DiffUtil;

import com.yareg.base.model.BaseModel;

import java.util.List;

public class BaseModelListAdapter<T extends BaseModel<T>> extends BaseListAdapter<T> {

    private final int itemId;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public BaseModelListAdapter(final int layout, final int itemId, @Nullable final List<T> items) {
        super(layout, items);
        this.itemId = itemId;
        setHasStableIds(true);
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    protected void onBindItemData(@NonNull final ViewDataBinding binding, @NonNull final T data) {
        binding.setVariable(itemId, data);
    }

    @Override
    public long getItemId(final int position) { return getItem(position).id; }

    @Override
    protected DiffUtil.Callback getDiff(@NonNull final List<T> items) {
        return new BaseModelListDiff<>(getList(), items);
    }
}
