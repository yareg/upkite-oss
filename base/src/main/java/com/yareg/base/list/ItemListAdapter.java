package com.yareg.base.list;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;

import com.yareg.base.model.BaseModel;

import java.util.List;

public class ItemListAdapter<T extends BaseModel<T>> extends ListAdapter<T> {

    private final int itemId;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public ItemListAdapter(final int layout, final int itemId, @Nullable final List<T> items) {
        super(layout, items);
        this.itemId = itemId;
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    protected void onBindItemData(@NonNull final ViewDataBinding binding, @NonNull final T data) {
        binding.setVariable(itemId, data);
    }
}
