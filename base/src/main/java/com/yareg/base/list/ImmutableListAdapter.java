package com.yareg.base.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class ImmutableListAdapter<T, VH extends ImmutableListAdapter.ViewHolder>
        extends RecyclerView.Adapter<VH> {

    //private OnItemClickListener listener;
    private final int           layout;

    private final List<T> dataset = new ArrayList<>();

    //----------------------------------------------------------------------------------------------
    //
    // INTERFACE
    //
    //----------------------------------------------------------------------------------------------


/*    interface OnItemClickListener {
        void onItemClicked(final int position);
    }*/

    /*
    public void setOnItemClickListener(@NonNull final OnItemClickListener clickListener) {
        listener = clickListener;
    }
    */

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    protected ImmutableListAdapter(int layout, @Nullable final List<T> items) {
        this.layout = layout;
        if (items != null) { dataset.addAll(items); }
    }

    //----------------------------------------------------------------------------------------------
    //
    // VIEW HOLDER
    //
    //----------------------------------------------------------------------------------------------

    public abstract static class ViewHolder
            extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        public ViewHolder(@NonNull final View view) {
            super(view);
            //if(listener != null) { view.setOnClickListener(this); }
        }

        @Override
        public void onClick(@NonNull final View view) {
            //if(listener != null) { listener.onItemClicked(getLayoutPosition()); }
        }

        protected abstract void bind(final int position);
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    @NonNull
    public VH onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int viewType) {
        return createViewHolder(LayoutInflater
                .from(viewGroup.getContext())
                .inflate(layout, viewGroup, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull VH viewHolder, final int position) {
        viewHolder.bind(position);
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    public T getItem(final int position) { return dataset.get(position); }

    //----------------------------------------------------------------------------------------------
    //
    // PROTECTED METHODS
    //
    //----------------------------------------------------------------------------------------------

    protected abstract VH createViewHolder(View view);
}
