package com.yareg.base.list;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import java.util.List;

public class BaseListDiff<T> extends DiffUtil.Callback {

    protected final List<T> oldList;
    protected final List<T> newList;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public BaseListDiff(@NonNull final List<T> oldList, @NonNull final List<T> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(final int oldPosition, final int newPosition) {
        return (oldList.get(oldPosition).hashCode() == newList.get(newPosition).hashCode());
    }

    @Override
    public boolean areContentsTheSame(final int oldPosition, final int newPosition) {
        return (oldList.get(oldPosition).equals(newList.get(newPosition)));
    }

    @Nullable
    @Override
    public Object getChangePayload(final int oldPosition, final int newPosition) {
        return super.getChangePayload(oldPosition, newPosition);
    }
}
