package com.yareg.base.livedata;

import android.view.View;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.annotation.WorkerThread;

import com.yareg.base.model.SnackbarData;

public class Snap extends SingleEvent<SnackbarData> {

    //----------------------------------------------------------------------------------------------
    //
    // INSTANCE
    // https://en.wikipedia.org/wiki/Initialization_on_demand_holder_idiom
    //
    //----------------------------------------------------------------------------------------------

    private static class Singleton {
        static final Snap INSTANCE = new Snap();
    }

    @NonNull
    public static Snap getInstance() { return Singleton.INSTANCE; }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- MAKE ------------------------------------------------------------------------------------

    @MainThread
    public static void make(@StringRes final int stringRes) {
        getInstance().setValue(new SnackbarData(stringRes));
    }

    @MainThread
    public static void make(
            @StringRes final int stringRes,
            @StringRes final int actionRes,
            @NonNull final View.OnClickListener action) {
        getInstance().setValue(new SnackbarData(stringRes, actionRes, action));
    }

    // --- POST ------------------------------------------------------------------------------------

    @WorkerThread
    public static void post(@StringRes final int stringRes) {
        getInstance().postValue(new SnackbarData(stringRes));
    }
}
