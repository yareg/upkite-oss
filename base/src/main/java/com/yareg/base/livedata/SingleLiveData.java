package com.yareg.base.livedata;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;

import com.yareg.base.model.BaseModel;
import com.yareg.base.content.BaseContentTasks;
import com.yareg.base.content.BaseContentResolver;

public abstract class SingleLiveData<T extends BaseModel<T>> extends LiveData<T> {

    private T prototype;
    private final BaseContentResolver resolver;
    private final ContentObserver observer;

    private boolean isObserverRegistered = false;

    //----------------------------------------------------------------------------------------------
    //
    // INSTANCE
    //
    //----------------------------------------------------------------------------------------------

    protected SingleLiveData(@NonNull final T prototype) {

        this.prototype = prototype;
        this.observer  = new Observer(new Handler(Looper.getMainLooper()));
        this.resolver  = BaseContentResolver.getInstance();

        if (prototype.isValid()) {
            registerContentObserver();
            loadData();
        }
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    /*@Override
    protected void onActive() {

    }

    @Override
    protected void onInactive() {

    }*/

    //----------------------------------------------------------------------------------------------
    //
    // OBSERVER
    //
    //----------------------------------------------------------------------------------------------

    private class Observer extends ContentObserver {

        private Observer(@NonNull final Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(final boolean selfChange, @Nullable final Uri uri) {
            //Log.i(TAG, "selfChange: " + selfChange + ", uri: " + uri);
            loadData();
        }
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    //public long getId() { return this.prototype.id; }

    public void setId(final long id) {
        if (id > 0 && id != prototype.id) {
            prototype = prototype.create(id);
            registerContentObserver();
            loadData();
        }
    }

    // --- SET -------------------------------------------------------------------------------------

    public void set(@NonNull final T data, @Nullable final BaseContentTasks.SetCallback callback) {
        if (data.isValid()) {
            if (data.id != prototype.id) {
                prototype = prototype.create(data.id);
                registerContentObserver();
            }
            resolver.set(data, callback);
        } else {
            if (callback != null) { callback.execute(false); }
        }
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- LOAD DATA -------------------------------------------------------------------------------

    private void loadData() {
        resolver.get(prototype, this::setValue);
    }

    // --- REGISTER CONTENT OBSERVER ---------------------------------------------------------------

    private void registerContentObserver() {
        if (isObserverRegistered) { resolver.unregisterContentObserver(observer); }

        resolver.registerContentObserver(
                prototype.baseUri.buildUpon().appendPath(Long.toString(prototype.id)).build(),
                observer
        );

        isObserverRegistered = true;
    }
}
