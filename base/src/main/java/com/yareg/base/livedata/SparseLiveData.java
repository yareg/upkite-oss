package com.yareg.base.livedata;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.LongSparseArray;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;

import com.yareg.base.model.BaseModel;
import com.yareg.base.content.BaseContentResolver;

public abstract class SparseLiveData<T extends BaseModel<T>> extends LiveData<LongSparseArray<T>> {

    // TODO: don't forget to remove static mod if changing these per instance
    private final static String   selection = BaseModel.KEY_ID + ">?";
    private final static String[] args      = { "0" };

    private final T prototype;
    private final BaseContentResolver resolver;
    private final LongSparseArray<LiveData<T>> observables = new LongSparseArray<>();

    //----------------------------------------------------------------------------------------------
    //
    // INSTANCE
    //
    //----------------------------------------------------------------------------------------------

    protected SparseLiveData(@NonNull final T prototype) {

        this.prototype = prototype;
        resolver = BaseContentResolver.getInstance();
        resolver.registerContentObserver(
                prototype.baseUri,
                new Observer(new Handler(Looper.getMainLooper()))
        );

        loadData(selection, args);
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    /*@Override
    protected void onActive() {

    }

    @Override
    protected void onInactive() {

    }*/

    //----------------------------------------------------------------------------------------------
    //
    // OBSERVER
    //
    //----------------------------------------------------------------------------------------------

    private class Observer extends ContentObserver {

        private Observer(@NonNull final Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(final boolean selfChange, @Nullable final Uri uri) {
            loadData(selection, args);
        }
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- GET ITEM --------------------------------------------------------------------------------

    @NonNull
    public T getItem(final long key) {
        if (getValue() == null) { return resolver.completePrototype(prototype.create(key)); }
        if (getValue().indexOfKey(key) < 0) { return prototype.create(key); }
        return getValue().get(key);
    }

    // --- GET OBSERVABLE --------------------------------------------------------------------------

    @NonNull
    public LiveData<T> getObservable(final long key) {
        if (observables.indexOfKey(key) < 0) {
            observables.put(key, Transformations.map(this, items -> items.get(key)));
        }
        return observables.get(key);
    }

    // --- SET ITEM --------------------------------------------------------------------------------

    protected void setItem(@NonNull final T item) {
        resolver.set(item, null);
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- LOAD DATA -------------------------------------------------------------------------------

    private void loadData(@Nullable final String selection, @Nullable final String[] args) {
        resolver.list(prototype, this::setValue, selection, args);
    }
}
