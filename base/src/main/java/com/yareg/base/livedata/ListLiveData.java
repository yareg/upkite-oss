package com.yareg.base.livedata;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import android.os.Looper;
import android.util.LongSparseArray;

import com.yareg.base.async.AsyncWork;
import com.yareg.base.model.BaseModel;
import com.yareg.base.content.BaseContentTasks;
import com.yareg.base.content.BaseContentResolver;

import java.util.ArrayList;
import java.util.Collections;

public class ListLiveData<T extends BaseModel<T>> extends LiveData<ArrayList<T>> {

    private final static String   BASE_SELECTION      = BaseModel.KEY_ID.concat(">0");
    private final static String[] BASE_SELECTION_ARGS = {};

    private final T prototype;

    private final BaseContentResolver resolver;
    private final Handler             handler;

    private final MutableLiveData<LongSparseArray<T>> cache = new MutableLiveData<>();
    private final LongSparseArray<T>                  trash = new LongSparseArray<>();

    private String   selection     = BASE_SELECTION;
    private String[] selectionArgs = BASE_SELECTION_ARGS;

    //----------------------------------------------------------------------------------------------
    //
    // INSTANCE
    //
    //----------------------------------------------------------------------------------------------

    public ListLiveData(@NonNull final T prototype, final boolean autoLoad) {

        this.prototype = prototype;

        handler  = new Handler(Looper.getMainLooper());
        resolver = BaseContentResolver.getInstance();

        resolver.registerContentObserver(prototype.baseUri, new Observer(handler));

        if (autoLoad) { loadData(); }
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    /*@Override
    protected void onActive() {

    }

    @Override
    protected void onInactive() {

    }*/

    //----------------------------------------------------------------------------------------------
    //
    // OBSERVER
    //
    //----------------------------------------------------------------------------------------------

    private class Observer extends ContentObserver {

        Observer(@NonNull final Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(final boolean selfChange, @Nullable final Uri uri) { loadData(); }
    }

    //----------------------------------------------------------------------------------------------
    //
    // SORTING
    //
    //----------------------------------------------------------------------------------------------

    private interface SortCallback<T> {
        void execute(@Nullable final T list);
    }

    private static class SortTask<T extends BaseModel<T>> extends AsyncWork<Void, ArrayList<T>> {

        private final LongSparseArray<T> items;
        private final LongSparseArray<T> trash;
        private final SortCallback<ArrayList<T>> callback;

        SortTask(
                @NonNull final LongSparseArray<T> items,
                @NonNull final LongSparseArray<T> trash,
                @NonNull final SortCallback<ArrayList<T>> callback) {
            this.items = items;
            this.trash = trash;
            this.callback = callback;
        }

        @Override
        protected ArrayList<T> doInBackground(Void... voids) {
            final ArrayList<T> sorted = new ArrayList<>(items.size());

            for (int i = 0; i < items.size(); i++) {
                final long key = items.keyAt(i);
                if(trash.indexOfKey(key) < 0) { sorted.add(items.get(key)); }
            }

            if (sorted.size() > 1) {
                Collections.sort(sorted, (a, b) -> Long.compare(b.id, a.id));
            }

            return sorted;
        }

        @Override
        protected void onPostExecute(@Nullable final ArrayList<T> sorted) {
            callback.execute(sorted);
        }
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- SET SCOPE -------------------------------------------------------------------------------

    public void setScope(@NonNull final String selection, @NonNull final String[] selectionArgs) {
        this.selection     = BASE_SELECTION.concat(" AND ".concat(selection));
        this.selectionArgs = selectionArgs;
        loadData();
    }

    // --- GET OBSERVABLE ITEM ---------------------------------------------------------------------

    @NonNull
    public LiveData<T> getObservableItem(final long key) {
        return Transformations.map(cache, items -> items.get(key));
    }

    // --- GET OBSERVABLE SIZE ---------------------------------------------------------------------

    @NonNull
    public LiveData<Integer> getObservableSize() {
        return Transformations.map(
                this,
                value -> (value == null || value.size() == 0) ? null : value.size()
        );
    }

    // --- GET OBSERVABLE ITEM BY INDEX ------------------------------------------------------------

    @NonNull
    public LiveData<T> getObservableItemByIndex(final int index) {
        return Transformations.map(
                this,
                value -> (value == null || value.isEmpty()) ? null : value.get(index)
        );
    }

    // --- SET ITEM --------------------------------------------------------------------------------

    public void setItem(
            @NonNull final T item,
            @Nullable final BaseContentTasks.SetCallback callback) {
        resolver.set(item, callback);
    }

    // --- MOVE TO TRASH ---------------------------------------------------------------------------

    public void moveToTrash(
            final long id,
            @Nullable final BaseContentTasks.SetCallback callback,
            @Nullable final BaseContentTasks.IdResult idResult,
            final int commitDelay) {

        if (cache.getValue() == null || cache.getValue().indexOfKey(id) < 0) {
            if (callback != null) { callback.execute(false); }
            return;
        }

        trash.put(id, cache.getValue().get(id));
        cache.getValue().remove(id);

        new SortTask<>(cache.getValue(), trash, (value)-> {
            setValue(value);
            if (callback != null) { callback.execute(true); }
        }).execute();

        handler.postDelayed(
                () -> {
                    if (isNotFoundInTrash(id)) { return; }
                    trash.remove(id);
                    resolver.trash(
                            prototype.baseUri,
                            id,
                            (trashId) -> {
                                if (trashId < 0) {
                                    if (idResult != null ) { idResult.post(trashId); }
                                } else {
                                    loadData();
                                }
                            }
                    );
                },
                commitDelay
        );
    }

    // --- RESTORE FROM TRASH ----------------------------------------------------------------------

    public void restoreFromTrash(final long id) {
        if (cache.getValue() == null || isNotFoundInTrash(id)) { return; }
        trash.remove(id);
        loadData();
    }

    // --- CLEAR DATA ------------------------------------------------------------------------------

    public void clearData() {
        setValue(new ArrayList<>());
        cache.setValue(new LongSparseArray<>());
        trash.clear();
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- LOAD DATA -------------------------------------------------------------------------------

    private void loadData() {
        resolver.list(
                prototype,
                (value) -> {
                    cache.setValue(value);
                    new SortTask<>(value, trash, this::setValue).execute();
                },
                selection,
                selectionArgs
        );
    }

    // --- IS NOT FOUND IN TRASH -------------------------------------------------------------------

    private boolean isNotFoundInTrash(final long id) {
        return (trash.size() == 0 || trash.indexOfKey(id) < 0);
    }

    // --- JOIN ARRAYS -----------------------------------------------------------------------------

    /*private static String[] joinArrays(@NonNull final String[] a, @NonNull final String[] b) {
        final String[] result = new String[a.length + b.length];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }*/
}
