package com.yareg.base.livedata;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import java.util.concurrent.atomic.AtomicBoolean;

public class SingleEvent<T> extends MutableLiveData<T> {

    //----------------------------------------------------------------------------------------------
    //
    // NOTE: only one observer will be notified of change
    //
    //----------------------------------------------------------------------------------------------

    private final AtomicBoolean isPending = new AtomicBoolean(false);

    //----------------------------------------------------------------------------------------------
    //
    // SUPER OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @MainThread
    @Override
    public void observe(
            @NonNull final LifecycleOwner owner,
            @NonNull final Observer<? super T> observer) {
        super.observe(owner, (@Nullable final T value)-> {
            if (isPending.compareAndSet(true, false)) { observer.onChanged(value); }
        });
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    @MainThread
    public void setValue(@Nullable final T value) {
        isPending.set(true);
        super.setValue(value);
    }
}
