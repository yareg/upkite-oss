package com.yareg.base.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.yareg.base.livedata.SingleLiveData;
import com.yareg.base.model.BaseModel;

public abstract class EntityViewModel<T extends BaseModel<T>> extends AndroidViewModel {

    protected final SingleLiveData<T> entity;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public EntityViewModel(@NonNull final Application application) {
        super(application);
        entity = getEntityInstance();
    }

    protected abstract SingleLiveData<T> getEntityInstance();

    //----------------------------------------------------------------------------------------------
    //
    // ENTITY
    //
    //----------------------------------------------------------------------------------------------

/*    protected class Entity extends SingleLiveData<T> {
        public Entity(@NonNull final Context context, @NonNull final T prototype) {
            super(context, prototype);
        }
    }*/
}
