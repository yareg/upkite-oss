package com.yareg.base.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.yareg.base.R;
import com.yareg.base.content.BaseContentTasks;
import com.yareg.base.livedata.ListLiveData;
import com.yareg.base.livedata.Snap;
import com.yareg.base.model.BaseModel;

import java.util.ArrayList;

public abstract class RepositoryViewModel<T extends BaseModel<T>> extends AndroidViewModel {

    private static final int COMMIT_DELAY = 5000;

    protected final ListLiveData<T> repository;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public RepositoryViewModel(@NonNull final Application application) {
        super(application);
        repository = getRepositoryInstance();
    }

    protected abstract ListLiveData<T> getRepositoryInstance();

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- GET REPOSITORY --------------------------------------------------------------------------

    public LiveData<ArrayList<T>> getRepository() { return repository; }

    // --- GET ITEM COUNT --------------------------------------------------------------------------

    @NonNull
    public LiveData<Integer> getItemCount() { return repository.getObservableSize(); }

    // --- GET ITEM --------------------------------------------------------------------------------

    @NonNull
    public LiveData<T> getItem(final long id) { return repository.getObservableItem(id); }

    // --- DELETE ITEM -----------------------------------------------------------------------------

    public void trashItem(
            final long id,
            @Nullable final BaseContentTasks.IdResult trashCallback) {
        repository.moveToTrash(id, (isMoved)-> {
            if (isMoved) {
                Snap.make(
                        R.string.info_trash,
                        R.string.action_undo,
                        (view) -> repository.restoreFromTrash(id)
                );
            } else { Snap.make(R.string.error_trash); }
        },  trashCallback, COMMIT_DELAY);
    }

    // --- CLEAR REPOSITORY ------------------------------------------------------------------------

    public void clearRepository() { repository.clearData(); }
}
