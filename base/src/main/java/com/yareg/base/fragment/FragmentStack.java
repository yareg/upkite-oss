package com.yareg.base.fragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

import java.util.Stack;

public class FragmentStack extends LiveData<FragmentItem> {

    private static class StackItem {
        final FragmentItem fragmentItem;
        final boolean isBackEnabled;

        StackItem(final @NonNull FragmentItem fragmentItem, final boolean isBackEnabled) {
            this.fragmentItem = fragmentItem;
            this.isBackEnabled = isBackEnabled;
        }
    }

    private final Stack<StackItem> stack = new Stack<>();
    private boolean isInitialized, isBackEnabled = false;
    private FragmentItem startItem, previousItem;

    //----------------------------------------------------------------------------------------------
    //
    // INSTANCE
    // https://en.wikipedia.org/wiki/Initialization_on_demand_holder_idiom
    //
    //----------------------------------------------------------------------------------------------

    private static class Singleton {
        static final FragmentStack INSTANCE = new FragmentStack();
    }

    public static FragmentStack getInstance() { return Singleton.INSTANCE; }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    //--- INIT -------------------------------------------------------------------------------------

    public void init(@NonNull final FragmentItem item) {
        if (!isInitialized) {
            isInitialized = true;
            startItem = item;
            pushValue(startItem, false);
        }
    }

    // --- CLEAR AND PUSH --------------------------------------------------------------------------

    public void clearAndPush(@NonNull final FragmentItem item) {
        stack.clear();
        startItem = item;
        pushValue(startItem, false);
    }

    //--- PUSH -------------------------------------------------------------------------------------

    public void push(@NonNull final FragmentItem item, final boolean isBackEnabled) {
        if (item.equals(getValue())) { return; }
        pushValue(item, isBackEnabled);
    }

    public void push(@NonNull final FragmentItem item) {
        final FragmentItem value = getValue();
        if (item.equals(value)) { return; }
        pushValue(item, this.isBackEnabled || item.drawerItemId == 0);
    }

    //--- GET PREVIOUS ITEM ------------------------------------------------------------------------

    @Nullable
    public FragmentItem getPreviousItem() { return previousItem; }

    //--- IS BACK ENABLED --------------------------------------------------------------------------

    public boolean isBackEnabled() { return isBackEnabled; }

    //--- ON BACK PRESSED --------------------------------------------------------------------------

    public boolean onBackPressed() {
        if (stack.size() > 0) {
            final StackItem value = stack.pop();
            popValue(value.fragmentItem, value.isBackEnabled);
            return true;
        } else if (startItem != null && !startItem.equals(getValue())){
            popValue(startItem, false);
            return true;
        }
        return false;
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- PUSH VALUE ------------------------------------------------------------------------------

    private void pushValue(@NonNull final FragmentItem item, final boolean isBackEnabled) {
        previousItem = getValue();
        if (isBackEnabled && previousItem != null) {
            stack.push(new StackItem(previousItem, this.isBackEnabled));
        }
        this.isBackEnabled = isBackEnabled;
        setValue(item);
    }

    // --- POP VALUE -------------------------------------------------------------------------------

    private void popValue(@NonNull final FragmentItem item, final boolean isBackEnabled) {
        previousItem = getValue();
        this.isBackEnabled = isBackEnabled;
        setValue(item);
    }
}
