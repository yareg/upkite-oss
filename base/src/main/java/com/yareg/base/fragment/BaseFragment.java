package com.yareg.base.fragment;

import androidx.fragment.app.Fragment;

public abstract class BaseFragment extends Fragment {

    public static abstract class Creator {
        public abstract Fragment createInstance();
    }
}