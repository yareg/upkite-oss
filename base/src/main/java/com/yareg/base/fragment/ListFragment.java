package com.yareg.base.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yareg.base.R;
import com.yareg.base.activity.BaseActivity;
import com.yareg.base.databinding.FragmentListBinding;

public class ListFragment extends BaseFragment {

    private static final String KEY_STATE = "state";

    protected LinearLayoutManager layoutManager;
    protected RecyclerView        list;

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        if (layoutManager != null) {
            savedInstanceState.putParcelable(KEY_STATE, layoutManager.onSaveInstanceState());
        }
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {

        final BaseActivity activity = (BaseActivity) getActivity();
        if (activity == null) { return null; }

        layoutManager = new LinearLayoutManager(activity);
        if (savedInstanceState != null) {
            layoutManager.onRestoreInstanceState(savedInstanceState.getParcelable(KEY_STATE));
        }

        final FragmentListBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_list, container, false
        );

        onBindView(activity, binding);

        return binding.getRoot();
    }

    //----------------------------------------------------------------------------------------------
    //
    // PROTECTED METHODS
    //
    //----------------------------------------------------------------------------------------------

    protected void onBindView(
            @NonNull final BaseActivity activity,
            @NonNull final FragmentListBinding binding) {
        list = binding.rvList;
    }
}
