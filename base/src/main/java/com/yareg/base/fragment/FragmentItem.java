package com.yareg.base.fragment;

import androidx.annotation.Nullable;

import com.yareg.base.model.FabData;

public class FragmentItem {

    public final int                  id;
    public final long                 subId;
    public final String               tag;
    public final int                  titleStringId;
    public final int                  optionsMenu;
    public final int                  optionsItemId;
    public final boolean              drawerMenu;
    public final int                  drawerItemId;
    public final FabData              fabData;
    public final BaseFragment.Creator creator;
    public final boolean              isDisposable;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public FragmentItem(
            final int id,
            final long subId,
            final int titleStringId,
            final int optionsMenu,
            final int optionsItemId,
            final boolean drawerMenu,
            final int drawerItemId,
            @Nullable final FabData fabData,
            final BaseFragment.Creator creator,
            final boolean isDisposable) {

        this.id            = id;
        this.subId         = subId;
        this.titleStringId = titleStringId;
        this.optionsItemId = optionsItemId;
        this.drawerItemId  = drawerItemId;

        this.tag = "f" + ((this.subId > 0) ? this.id + "-" + this.subId : this.id);

        this.optionsMenu  = optionsMenu;
        this.drawerMenu   = drawerMenu;
        this.fabData      = fabData;
        this.creator      = creator;
        this.isDisposable = isDisposable;
    }

    //----------------------------------------------------------------------------------------------
    //
    // METHODS
    //
    //----------------------------------------------------------------------------------------------

    boolean equals(@Nullable final FragmentItem item) {
        return (item != null && id == item.id && subId == item.subId);
    }
}
