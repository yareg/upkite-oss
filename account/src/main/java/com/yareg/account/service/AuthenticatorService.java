package com.yareg.account.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.yareg.account.app.AppAuthenticator;

import androidx.annotation.Nullable;

public class AuthenticatorService extends Service {

    private AppAuthenticator accountAuthenticator;

    @Override
    public void onCreate() {
        this.accountAuthenticator = new AppAuthenticator(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) { return accountAuthenticator.getIBinder(); }
}
