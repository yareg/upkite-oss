package com.yareg.account.model;

public class SyncState {

    public static final short UNKNOWN   = -1;
    public static final short IDLE      = 0;
    public static final short ACTIVE    = 1;
    //public static final short REQUESTED = 2;
    public static final short RUNNING   = 3;
    public static final short FAILED    = 4;
    public static final short SUCCEEDED = 5;

    public final short primary;
    public final short secondary;

    public SyncState(final short primary, final short secondary) {
        this.primary   = primary;
        this.secondary = secondary;
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    public boolean isIdle() { return primary == IDLE; }
}