package com.yareg.account.manager;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class BaseAccountManager {

    //private static final String TAG = BaseAccountManager.class.getSimpleName();

    public static final String KEY_PROFILE_ID = "profile_id";

    private static BaseAccountManager instance;

    private final AccountManager service;

    public static String AUTHORITY;
    public static String ACCOUNT_TYPE;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    private BaseAccountManager(@NonNull final Context context) {
        service = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
    }

    //----------------------------------------------------------------------------------------------
    //
    // INSTANCE
    //
    //----------------------------------------------------------------------------------------------

    /*
    public static BaseAccountManager getInstance(@NonNull final Context context) {
        return new BaseAccountManager(context);
    }
    */

    public static BaseAccountManager getInstance(@NonNull final Context context) {
        BaseAccountManager i = instance;
        if (i == null){
            synchronized (BaseAccountManager.class) {
                i = instance;
                if (i == null) {
                    instance = i = new BaseAccountManager(context);
                }
            }
        }
        return i;
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    //--- ADD ACCOUNT ------------------------------------------------------------------------------

    @Nullable
    public Account addAccount(@NonNull final String accountName, @NonNull final Bundle userData) {
        final Account account = new Account(accountName, ACCOUNT_TYPE);
        if (service.addAccountExplicitly(account, null, userData)) { return account; }
        return null;
    }

    //--- GET ACCOUNT ------------------------------------------------------------------------------

    @Nullable
    public Account getAccount(@NonNull final String accountName) {

        final Account[] accounts = service.getAccountsByType(ACCOUNT_TYPE);

        for (final Account account: accounts) {
            if (account.name.equalsIgnoreCase(accountName)) { return account; }
        }

        return null;
    }

    //--- GET ACCOUNT DATA -------------------------------------------------------------------------

    /*
    public String getAccountData(@NonNull final Account account, final String key) {
        return service.getUserData(account, key);
    }
    */

    //--- SYNC ACCOUNT -----------------------------------------------------------------------------

    public static boolean syncAccount(@NonNull final Account account) {

        if (isSyncActive(account)) { return false; }

        final Bundle settings = new Bundle();

        /*
            Forces a manual sync. The sync adapter framework ignores the existing settings, such as
            the flag set by setSyncAutomatically().
        */
        settings.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);

        /*
            Forces the sync to start immediately. If you don't set this, the system may wait several
            seconds before running the sync request, because it tries to optimize battery use by
            scheduling many requests in a short period of time.
        */
        settings.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);

        ContentResolver.requestSync(account, AUTHORITY, settings);

        return true;
    }

    //--- SET ACCOUNT POLL FREQUENCY ---------------------------------------------------------------

    public static void setSyncFrequency(@NonNull final Account account, final long syncFrequency) {

        /*
        syncFrequency - how frequently the sync should be performed, in seconds. On Android API
        level 24 and above, a minimal interval of 15 minutes is enforced. On previous versions, the
        minimum interval is 1 hour.
        */

        if (syncFrequency > 0) {
            ContentResolver.addPeriodicSync(
                    account, AUTHORITY, Bundle.EMPTY, syncFrequency / 1000L
            );
        } else {
            ContentResolver.removePeriodicSync(account, AUTHORITY, Bundle.EMPTY);
        }
    }

    //--- GET ACCOUNT IS SYNCABLE ------------------------------------------------------------------

    private static boolean getIsSyncable(@NonNull final Account account) {
        return (ContentResolver.getIsSyncable(account, AUTHORITY) == 1);
    }

    //--- SET ACCOUNT IS SYNCABLE ------------------------------------------------------------------

    public static void setIsSyncable(@NonNull final Account account, final boolean isSyncable) {
        if (getIsSyncable(account) == isSyncable) { return; }
        ContentResolver.setIsSyncable(account, AUTHORITY, isSyncable ? 1 : 0);
    }

    //--- GET SYNC AUTOMATICALLY -------------------------------------------------------------------

    private static boolean getSyncAutomatically(@NonNull final Account account) {
        return ContentResolver.getSyncAutomatically(account, AUTHORITY);
    }

    //--- SET SYNC AUTOMATICALLY -------------------------------------------------------------------

    public static void setSyncAutomatically(@NonNull final Account account, final boolean isSet) {
        if (getSyncAutomatically(account) == isSet) { return; }
        ContentResolver.setSyncAutomatically(account, AUTHORITY, isSet);
    }

    // --- GET ACCOUNT USER DATA -------------------------------------------------------------------

    public String getAccountUserData(@NonNull final Account account, @NonNull final String key) {
        return service.getUserData(account, key);
    }

    // --- SET ACCOUNT USER DATA -------------------------------------------------------------------

    public void setAccountUserData(
            @NonNull final Account account,
            @NonNull final String key,
            @NonNull final String value) {
        service.setUserData(account, key, value);
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE METHODS
    //
    //----------------------------------------------------------------------------------------------

    //--- IS SYNC ACTIVE ---------------------------------------------------------------------------

    private static boolean isSyncActive(@NonNull final Account account) {
        return (ContentResolver.isSyncActive(account, AUTHORITY) ||
                ContentResolver.isSyncPending(account, AUTHORITY));
    }
}
