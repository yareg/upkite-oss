package com.yareg.network.callback;

import androidx.annotation.NonNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class ApiCallback<T> implements Callback<T> {

    //----------------------------------------------------------------------------------------------
    //
    // ERRORS
    //
    //----------------------------------------------------------------------------------------------

    private static final String ERROR_DEFAULT = "Internal server error. Try again at a later time.";
    private static final String ERROR_400     = "Invalid API location. Check the URL that you are using.";
    private static final String ERROR_403     = "Invalid or missing API token. Check that your API token is present and matches your assigned token.";
    private static final String ERROR_405     = "Invalid HTTP method. Check that the method (POST|GET) matches what the documentation indicates.";
    private static final String ERROR_412     = "Request failed. Check the response body for a more detailed description.";
    private static final String ERROR_503     = "Rate limit hit. API requests are limited to an average of 2/s. Try your request again later.";

    //----------------------------------------------------------------------------------------------
    //
    // CALLBACKS
    //
    //----------------------------------------------------------------------------------------------

    protected abstract void onApiResponse(final T response);
    protected abstract void onApiError(final String message);

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {

        final int code = response.code();
        final T   body = response.body();

        if(body == null) {

            if(code == 200) {
                onApiResponse(null);
                return;
            }

            onApiError(getErrorMessage(code));
            return;
        }

        onApiResponse(body);
    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        onApiError(t.getMessage());
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE METHODS
    //
    //----------------------------------------------------------------------------------------------

    private String getErrorMessage(final int code) {
        switch(code) {
            case 400: return ERROR_400;
            case 403: return ERROR_403;
            case 405: return ERROR_405;
            case 412: return ERROR_412;
            case 503: return ERROR_503;
            default:  return ERROR_DEFAULT;
        }
    }
}
