package com.yareg.network.manager;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Build;

import androidx.annotation.NonNull;

public class BaseConnectivityManager {

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- IS CONNECTED ----------------------------------------------------------------------------

    public static boolean isOffline(@NonNull final Context context) {
        final ConnectivityManager service =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(service == null) { return true; }

        // Don't use deprecated API, just return true :)
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M) { return false; }

        final Network network = service.getActiveNetwork();
        final NetworkCapabilities capabilities = service.getNetworkCapabilities(network);

        return (capabilities == null) || !capabilities.hasCapability(
                NetworkCapabilities.NET_CAPABILITY_INTERNET
        );
    }
}
