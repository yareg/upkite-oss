package com.yareg.upkite.ui;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.yareg.base.ui.BaseBindingAdapters;

public class BindingAdapters extends BaseBindingAdapters {

    @BindingAdapter("isOff")
    public static void isOff(@NonNull final View view, @NonNull final Boolean isGone) {
        view.setVisibility(isGone ? View.GONE : View.VISIBLE);
    }

    /*
    @BindingAdapter("isVisible")
    public static void isVisible(@NonNull final View view, @NonNull final Boolean isVisible) {
        view.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
    }
    */

    @BindingAdapter("isInvisible")
    public static void isInvisible(@NonNull final View view, @NonNull final Boolean isInvisible) {
        view.setVisibility(isInvisible ? View.INVISIBLE : View.VISIBLE);
    }

    /*@BindingAdapter("imageResId")
    public static void imageResId(@NonNull final ImageView view, @NonNull final Integer resId) {
        view.setImageResource(resId);
    }*/

    @BindingAdapter("extFabIconResId")
    public static void extFabIconResId(
            @NonNull final ExtendedFloatingActionButton extFab,
            @Nullable final Integer resId) {
        if(resId != null) { extFab.setIconResource(resId); } else { extFab.setIcon(null); }
    }

    @BindingAdapter(value = {"stringArray", "stringIndex"})
    public static void stringArray(
            @NonNull final TextView textView,
            @NonNull final Integer stringArray,
            @NonNull final Short stringIndex) {
        textView.setText(textView.getResources().getStringArray(stringArray)[stringIndex]);
    }

    @BindingAdapter("drawable")
    public static void drawable(@NonNull final ImageView view, @NonNull final Drawable drawable) {
        view.setImageDrawable(drawable);
    }

    /*
    @BindingAdapter("extFabTextResId")
    public static void extFabTextResId(
            @NonNull final ExtendedFloatingActionButton extFab,
            @NonNull final Integer resId) {
        extFab.setText(resId);
    }
    */

    /*
    @BindingAdapter("isExtFabVisible")
    public static void isExtFabVisible(
            @NonNull final ExtendedFloatingActionButton extFab,
            @NonNull final Boolean isVisible) {
        if(isVisible) { extFab.show(); } else { extFab.hide(); }
    }
    */

    /*
    @BindingAdapter("isExtFabExtended")
    public static void isExtFabExtended(
            @NonNull final ExtendedFloatingActionButton extFab,
            @NonNull final Boolean isExtended) {
        if(isExtended) { extFab.extend(); } else { extFab.shrink(); }
    }
    */
}
