package com.yareg.upkite.ui;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yareg.base.model.Text;
import com.yareg.base.model.Time;

public class Converters {

    @Nullable
    public static String emptyIsNull(@Nullable final String string) {
        return string == null ? null : string.isEmpty() ? null : string;
    }

    @NonNull
    public static String asDollars(@Nullable final Float value) {
        return Text.asDollars((value == null) ? 0.0f : value);
    }

    @NonNull
    public static String asDate(@NonNull final Context context, @Nullable final Long value) {
        return Time.getDate(context, value == null ? 0 : value);
    }
}
