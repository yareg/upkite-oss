package com.yareg.upkite.notification;

import android.content.res.Resources;

import androidx.annotation.NonNull;

import com.yareg.base.model.BaseNotification;
import com.yareg.upkite.R;

public class PermissionLackNotification extends BaseNotification {

    private static final int ID = 3;

    public PermissionLackNotification(@NonNull final Resources resources) {
        super(
                ID,
                resources.getString(R.string.nc_authentication_id),
                R.drawable.icon_upkite,
                resources.getString(R.string.app_name),
                resources.getString(R.string.lack_of_permission),
                resources.getString(R.string.notification_permission_lack)
        );
    }
}
