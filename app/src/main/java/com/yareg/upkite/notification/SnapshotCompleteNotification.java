package com.yareg.upkite.notification;

import android.content.res.Resources;

import androidx.annotation.NonNull;

import com.yareg.base.model.BaseNotification;
import com.yareg.upkite.R;

public class SnapshotCompleteNotification extends BaseNotification {

    private static final int ID = 4;

    public SnapshotCompleteNotification(
            @NonNull final Resources resources,
            @NonNull final String snapshotId) {
        super(
                ID,
                resources.getString(R.string.nc_snapshots_id),
                R.drawable.icon_upkite,
                resources.getString(R.string.app_name),
                resources.getString(R.string.notification_snapshot_complete),
                snapshotId
        );
    }
}
