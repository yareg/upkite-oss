package com.yareg.upkite.notification;

import android.content.res.Resources;

import androidx.annotation.NonNull;

import com.yareg.base.model.BaseNotification;
import com.yareg.base.model.Text;
import com.yareg.upkite.R;

public class TopUpNotification extends BaseNotification {

    private static final int ID = 1;

    public TopUpNotification(@NonNull final Resources resources, final float amount) {
        super(
                ID,
                resources.getString(R.string.nc_billing_id),
                R.drawable.icon_upkite,
                resources.getString(R.string.app_name),
                resources.getString(R.string.top_up),
                resources.getString(R.string.notification_top_up, Text.asDollars(amount))
        );
    }
}
