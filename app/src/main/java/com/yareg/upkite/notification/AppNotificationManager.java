package com.yareg.upkite.notification;

import android.content.Context;
import android.os.Build;

import androidx.annotation.NonNull;

import com.yareg.base.manager.BaseNotificationManager;
import com.yareg.upkite.R;

public abstract class AppNotificationManager extends BaseNotificationManager {

    public static void createNotificationChannels(@NonNull final Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            createNotificationChannel(
                    context,
                    R.string.nc_authentication_id,
                    R.string.nc_authentication_name,
                    R.string.nc_authentication_desc
            );

            createNotificationChannel(
                    context,
                    R.string.nc_billing_id,
                    R.string.nc_billing_name,
                    R.string.nc_billing_desc
            );

            createNotificationChannel(
                    context,
                    R.string.nc_snapshots_id,
                    R.string.nc_snapshots_name,
                    R.string.nc_snapshots_desc
            );
        }
    }
}
