package com.yareg.upkite.notification;

import android.content.res.Resources;

import androidx.annotation.NonNull;

import com.yareg.base.model.BaseNotification;
import com.yareg.base.model.Text;
import com.yareg.upkite.R;

public class LowBalanceNotification extends BaseNotification {

    private static final int ID = 2;

    public LowBalanceNotification(@NonNull final Resources resources, final float balance) {
        super(
                ID,
                resources.getString(R.string.nc_billing_id),
                R.drawable.icon_upkite,
                resources.getString(R.string.app_name),
                resources.getString(R.string.low_balance),
                resources.getString(R.string.notification_low_balance,Text.asDollars(balance)
                )
        );
    }
}
