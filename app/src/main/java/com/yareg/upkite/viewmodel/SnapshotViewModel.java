package com.yareg.upkite.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.yareg.base.content.BaseContentResolver;
import com.yareg.base.livedata.ListLiveData;
import com.yareg.base.livedata.Snap;
import com.yareg.base.viewmodel.RepositoryViewModel;
import com.yareg.upkite.R;
import com.yareg.upkite.constant.Key;
import com.yareg.upkite.model.Snapshot;
import com.yareg.upkite.model.Task;
import com.yareg.upkite.repository.SnapshotRepository;
import com.yareg.upkite.work.AppWorkManager;

public class SnapshotViewModel extends RepositoryViewModel<Snapshot> {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public SnapshotViewModel(@NonNull final Application application) {
        super(application);
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    protected ListLiveData<Snapshot> getRepositoryInstance() {
        return SnapshotRepository.getInstance();
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    public void deleteSnapshot(final long snapshotId) {

        final Task task = new Task();
        task.typeId     = Key.Task.DESTROY_SNAPSHOT;
        task.objectId   = Long.toString(snapshotId);
        task.status     = Key.Status.PENDING;

        BaseContentResolver
                .getInstance()
                .set(
                        task,
                        (isOk)-> {
                            Snap.make(isOk ? R.string.info_task_add : R.string.error_task_add);
                            AppWorkManager
                                    .getInstance(getApplication().getApplicationContext())
                                    .destroySnapshot(task.id, snapshotId);
                        }
                );
    }
}
