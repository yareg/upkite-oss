package com.yareg.upkite.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.yareg.base.content.BaseContentResolver;
import com.yareg.base.livedata.ListLiveData;
import com.yareg.base.livedata.Snap;
import com.yareg.base.viewmodel.RepositoryViewModel;
import com.yareg.upkite.R;
import com.yareg.upkite.constant.Key;
import com.yareg.upkite.model.Server;
import com.yareg.upkite.model.Task;
import com.yareg.upkite.repository.ServerRepository;
import com.yareg.upkite.work.AppWorkManager;

public class ServerViewModel extends RepositoryViewModel<Server> {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public ServerViewModel(@NonNull final Application application) {
        super(application);
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    protected ListLiveData<Server> getRepositoryInstance() {
        return ServerRepository.getInstance();
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    public void createSnapshot(final long serverId) {

        final Task task = new Task();
        task.typeId     = Key.Task.CREATE_SNAPSHOT;
        task.objectId   = Long.toString(serverId);
        task.status     = Key.Status.PENDING;

        BaseContentResolver
                .getInstance()
                .set(
                        task,
                        (isOk)-> {
                            Snap.make(isOk ? R.string.info_task_add : R.string.error_task_add);
                            AppWorkManager
                                    .getInstance(getApplication().getApplicationContext())
                                    .createSnapshot(task.id, serverId);
                        }
                );
    }
}
