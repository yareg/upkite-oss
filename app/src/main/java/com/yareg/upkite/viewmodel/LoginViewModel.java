package com.yareg.upkite.viewmodel;

import android.app.Application;
import android.view.View;

import com.yareg.base.livedata.Snap;
import com.yareg.base.manager.BaseUrlManager;
import com.yareg.network.manager.BaseConnectivityManager;
import com.yareg.upkite.R;
import com.yareg.upkite.model.Profile;
import com.yareg.upkite.vultr.VultrClient;
import com.yareg.upkite.vultr.model.VultrAuth;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class LoginViewModel extends AndroidViewModel {

    private final MutableLiveData<Boolean> isInputEnabled = new MutableLiveData<>();

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public LoginViewModel(@NonNull final Application application) {
        super(application);
        isInputEnabled.setValue(true);
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- GET INPUT STATE -------------------------------------------------------------------------

    public LiveData<Boolean> getIsInputEnabled() {
        return isInputEnabled;
    }

    // --- LOGIN -----------------------------------------------------------------------------------

    public void login(
            @NonNull final ProfileViewModel profileViewModel,
            @NonNull final String token) {

        final class Help {
            private void show(@StringRes int stringRes) {
                Snap.make(
                        stringRes,
                        R.string.help,
                        (@NonNull final View view)-> BaseUrlManager.openInBrowser(
                                view.getContext(),
                                view.getContext().getString(R.string.url_help)
                        )
                );
            }
        }

        if(token.isEmpty() || (token.length() < VultrClient.TOKEN_LENGTH)) {
            new Help().show(R.string.info_api_key);
            return;
        }

        if(BaseConnectivityManager.isOffline(getApplication().getApplicationContext())) {
            Snap.make(R.string.error_internet);
            return;
        }

        isInputEnabled.setValue(false);

        VultrClient.getInstance().getAuth(token, new VultrClient.ResponseListener<VultrAuth>() {

            @Override
            public void onResponse(@NonNull final VultrAuth auth) {

                if(auth.isValid()) {

                    if(auth.lacksPermissions(VultrClient.REQUIRED_PERMISSIONS)) {
                        new Help().show(R.string.error_permissions);
                        isInputEnabled.setValue(true);
                        return;
                    }

                    final Profile profile = new Profile();
                    profile.token = token;
                    profile.email = auth.email;
                    profile.permissions = auth.permissions;

                    if (!auth.name.isEmpty() && auth.name.contains(Profile.NAME_SPLITTER)) {
                        final String[] name = auth.name.split(Profile.NAME_SPLITTER);
                        profile.firstName = name[0];
                        profile.lastName  = name[1];
                    } else {
                        profile.firstName = auth.name;
                    }

                    profileViewModel.createProfile(profile);

                } else {
                    Snap.make(R.string.error_api_response);
                    isInputEnabled.setValue(true);
                }
            }

            @Override
            public void onError(String message) {
                new Help().show(R.string.error_api_key);
                isInputEnabled.setValue(true);
            }
        });
    }
}
