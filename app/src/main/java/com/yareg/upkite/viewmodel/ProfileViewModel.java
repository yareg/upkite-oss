package com.yareg.upkite.viewmodel;

import android.accounts.Account;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.yareg.base.livedata.SingleLiveData;
import com.yareg.base.livedata.Snap;
import com.yareg.base.manager.BaseDatabaseManager;
import com.yareg.base.model.PersistentInteger;
import com.yareg.base.model.Time;
import com.yareg.base.repository.IntegerRepository;

import com.yareg.account.manager.BaseAccountManager;
import com.yareg.account.model.SyncState;

import com.yareg.base.viewmodel.EntityViewModel;
import com.yareg.network.manager.BaseConnectivityManager;

import com.yareg.upkite.R;
import com.yareg.upkite.app.AppFeatures;
import com.yareg.upkite.constant.Key;
import com.yareg.upkite.constant.Value;
import com.yareg.upkite.model.Profile;
import com.yareg.upkite.repository.ProfileRepository;

public class ProfileViewModel extends EntityViewModel<Profile> {

    private final MediatorLiveData<SyncState> syncState = new MediatorLiveData<>();
    private final MutableLiveData<SyncState> mutableSyncState = new MutableLiveData<>();

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public ProfileViewModel(@NonNull final Application application) {
        super(application);

        syncState.addSource(mutableSyncState, syncState::setValue);
        syncState.addSource(
                Transformations.map(
                        WorkManager
                                .getInstance(application.getApplicationContext())
                                .getWorkInfosForUniqueWorkLiveData(Key.Work.SYNC),
                        workInfos -> {
                            int finished = 0;
                            for (final WorkInfo work : workInfos) {
                                if (!work.getState().isFinished()) {
                                    return new SyncState(SyncState.ACTIVE, SyncState.RUNNING);
                                }
                                finished++;
                            }
                            if (finished > 0 && workInfos.size() == finished) {
                                return new SyncState(SyncState.IDLE, SyncState.SUCCEEDED);
                            }
                            return new SyncState(SyncState.IDLE, SyncState.UNKNOWN);
                        }),
                syncState::setValue
        );

        mutableSyncState.setValue(new SyncState(SyncState.IDLE, SyncState.UNKNOWN));
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    protected SingleLiveData<Profile> getEntityInstance() {
        return ProfileRepository.getInstance();
    }

    //----------------------------------------------------------------------------------------------
    //
    // PROFILE
    //
    //----------------------------------------------------------------------------------------------

    // --- CREATE PROFILE --------------------------------------------------------------------------

    void createProfile(@NonNull final Profile profile) {

        final Context context = getApplication().getApplicationContext();

        if (BaseDatabaseManager.getInstance(context).createTables()) {
            entity.set(
                    profile,
                    (isSet) -> {
                        if (isSet) {

                            IntegerRepository
                                    .getInstance()
                                    .putLong(
                                            Key.Integer.PROFILE_ID,
                                            profile.id
                                    );

                            if (AppFeatures.ACCOUNT) {
                                if (createAccount(profile) != null) {
                                    syncAccount(profile, true);
                                }
                            }
                        }
                    }
            );
        }
    }

    // --- GET PROFILE ID --------------------------------------------------------------------------

    public LiveData<PersistentInteger> getProfileId() {
        return IntegerRepository
                .getInstance()
                .getObservable(Key.Integer.PROFILE_ID);
    }

    // --- GET PROFILE -----------------------------------------------------------------------------

    public LiveData<Profile> getProfile() {
        return entity;
    }

    //----------------------------------------------------------------------------------------------
    //
    // ACCOUNT SYNC
    //
    //----------------------------------------------------------------------------------------------

    // --- CREATE ACCOUNT --------------------------------------------------------------------------

    @Nullable
    private Account createAccount(@NonNull final Profile profile) {

        final BaseAccountManager accountManager = BaseAccountManager.getInstance(
                getApplication().getApplicationContext()
        );

        Account account = accountManager.getAccount(profile.getAccountName());

        if (account != null) {
            accountManager.setAccountUserData(
                    account,
                    BaseAccountManager.KEY_PROFILE_ID,
                    Long.toString(profile.id)
            );

            return account;
        }

        final Bundle profileData = new Bundle();
        profileData.putString(BaseAccountManager.KEY_PROFILE_ID, Long.toString(profile.id));

        account = accountManager.addAccount(profile.getAccountName(), profileData);

        if (account == null) {
            Snap.make(R.string.error_account);
            return null;
        }

        BaseAccountManager.setIsSyncable(account, true);
        BaseAccountManager.setSyncAutomatically(account, true);
        BaseAccountManager.setSyncFrequency(account, Value.Account.SYNC_FREQUENCY);

        return account;
    }

    // --- GET PAIRED ACCOUNT ----------------------------------------------------------------------

    @Nullable
    public Account getPairedAccount() {
        final Profile profile = getProfile().getValue();
        if (profile == null) { return null; }
        return getPairedAccount(profile);
    }

    @Nullable
    private Account getPairedAccount(@NonNull final Profile profile) {
        if (profile.getAccountName().isEmpty()) { return null; }

        Account account = BaseAccountManager
                .getInstance(getApplication().getApplicationContext())
                .getAccount(profile.getAccountName());

        if (account == null) { account = createAccount(profile); }

        return account;
    }

    // --- SYNC ACCOUNT ----------------------------------------------------------------------------

    public void syncAccount(final boolean isVerbose) {
        if (getProfile().getValue() != null) {
            syncAccount(getProfile().getValue(), isVerbose);
        }
    }

    private void syncAccount(@NonNull final Profile profile, final boolean isVerbose) {

        if (BaseConnectivityManager.isOffline(getApplication())) {
            if (isVerbose) { Snap.make(R.string.error_internet); }
            mutableSyncState.setValue(new SyncState(SyncState.IDLE, SyncState.FAILED));
            return;
        }

        if ((Time.getUnixTime() - profile.lastSync) < Value.Account.SYNC_RESOLUTION) {
            if (isVerbose) { Snap.make(R.string.error_frequent_refresh); }
            mutableSyncState.setValue(new SyncState(SyncState.IDLE, SyncState.FAILED));
            return;
        }

        final Account account = getPairedAccount(profile);

        if (account == null) {
            if (isVerbose) { Snap.make(R.string.error_account); }
            mutableSyncState.setValue(new SyncState(SyncState.IDLE, SyncState.FAILED));
            return;
        }

        BaseAccountManager.syncAccount(account);
    }

    // --- GET SYNC STATE --------------------------------------------------------------------------

    @NonNull
    public LiveData<SyncState> getSyncState() { return syncState; }
}
