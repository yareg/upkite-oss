package com.yareg.upkite.viewmodel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;

import com.yareg.base.fragment.FragmentStack;
import com.yareg.base.model.PersistentInteger;
import com.yareg.base.model.Text;
import com.yareg.base.model.Time;
import com.yareg.base.repository.IntegerRepository;
import com.yareg.upkite.R;
import com.yareg.upkite.constant.Key;
import com.yareg.upkite.fragment.BillingFragment;
import com.yareg.upkite.fragment.ServersFragment;
import com.yareg.upkite.fragment.SnapshotsFragment;
import com.yareg.upkite.model.Billing;
import com.yareg.upkite.model.Server;
import com.yareg.upkite.repository.BillingRepository;
import com.yareg.upkite.repository.ProfileRepository;
import com.yareg.upkite.repository.ServerRepository;
import com.yareg.upkite.repository.SnapshotRepository;

import java.util.ArrayList;

public class DashboardViewModel extends AndroidViewModel {

    /*

    All good if:
    1. All servers are powered on and are ok
    2. Total balance is enough to cover current month
    3. Bandwidth usage is lower than 90%
    4. Last snapshot made no more than 4 weeks ago
    5. Data is refreshed no more than 1 day ago

    Warning:
    1. One of servers is not powered on or has issues (not ok)
    2. Total balance is less than current month total cost
    3. Bandwidth usage is more than 90%
    4. Last snapshot made never or more than 4 weeks ago
    5. Last sync was more than 1 day ago

    */

    private final LiveData<Billing> billingSource;
    private final LiveData<Server>  bandwidthSource;
    private final ServerRepository  serverRepository;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public DashboardViewModel(@NonNull final Application application) {
        super(application);

        billingSource    = BillingRepository.getInstance();
        serverRepository = ServerRepository.getInstance();

        final IntegerRepository integers = IntegerRepository.getInstance();

        bandwidthSource = Transformations.switchMap(
                integers.getObservable(Key.Integer.WORST_BANDWIDTH_SERVER_ID),
                item -> (item == null) ? null : serverRepository.getObservableItem(item.value)
        );
    }

    //----------------------------------------------------------------------------------------------
    //
    // DATA BINDING
    //
    //----------------------------------------------------------------------------------------------

    // --- IS DATA READY ---------------------------------------------------------------------------

    public LiveData<Boolean> isDataReady() {
        return Transformations.map(
                ProfileRepository.getInstance(),
                profile -> (profile != null) && (profile.lastSync > 0)
        );
    }

    // --- LAST SYNC TIME --------------------------------------------------------------------------

    /*
    public LiveData<String> getLastSyncTime() {
        return Transformations.map(profileSource, profile ->
                (profile == null) ? null : Time.getSince(
                        profile.lastSync
                )
        );
    }
    */

    // --- SERVER ----------------------------------------------------------------------------------

    public LiveData<String> getServerPrimary()   {
        return Transformations.map(serverRepository, servers -> {
                    if(servers == null) { return null; }
                    switch(servers.size()) {
                        case 0:  return null;
                        case 1:  return servers.get(0).label;
                        default: return getApplication()
                                .getApplicationContext()
                                .getResources()
                                .getQuantityString(
                                        R.plurals.d_servers, servers.size(), servers.size()
                        );
                    }
                }
        );
    }

    public LiveData<String> getServerSecondary() {
        return Transformations.map(serverRepository, this::buildServerSecondary);
    }

    // --- STATUS ----------------------------------------------------------------------------------

    public LiveData<String> getStatusCaption() {
        return Transformations.map(serverRepository.getObservableSize(),
                size -> getApplication().getApplicationContext().getString(
                        (size == null || size == 1) ? R.string.server_status : R.string.servers_status
                )
        );
    }

    public LiveData<String> getStatusPrimary() {
        return Transformations.map(
                IntegerRepository
                        .getInstance()
                        .getObservable(Key.Integer.RUNNING_SERVERS),
                this::buildStatusPrimary
        );
    }

    public LiveData<String> getStatusSecondary() {
        final Context context = getApplication().getApplicationContext();
        return Transformations.map(
                IntegerRepository
                        .getInstance()
                        .getObservable(Key.Integer.SERVER_ISSUES),
                data -> (data == null || data.value == 0L) ?
                        context.getString(R.string.no_issues) :
                        context.getResources().getQuantityString(
                                R.plurals.d_issues, data.asInt(), data.asInt()
                        )
        );
    }

    // --- BILLING ---------------------------------------------------------------------------------

    public LiveData<String> getBillingPrimary() {
        return Transformations.map(billingSource, billing -> (billing == null) ? null :
                Text.asDollars(billing.remainingCredit)
        );
    }

    public LiveData<String> getBillingSecondary() {
        return Transformations.map(billingSource, billing -> (billing == null) ? null :
                Text.asDollars(billing.balance)
        );
    }

    public LiveData<String> getBillingTertiary() {
        return Transformations.map(billingSource, billing -> (billing == null) ? null :
                Text.asDollars(billing.pendingCharges)
        );
    }

    public LiveData<String> getBillingMonth() {
        final Context context = getApplication().getApplicationContext();
        return Transformations.map(
                ProfileRepository.getInstance(),
                profile -> (profile == null) ? null : Time.getInMonth(context, profile.lastSync)
        );
    }

    public LiveData<Integer> getBillingPrimaryProgress() {
        return Transformations.map(billingSource, billing -> (billing == null) ? null :
                Math.round(billing.pendingCharges / billing.balance * 100)
        );
    }

    public LiveData<Integer> getBillingSecondaryProgress() {
        return Transformations.map(billingSource, billing -> (billing == null) ? null :
                Math.round(billing.remainingCredit / billing.balance * 100)
        );
    }

    // --- BANDWIDTH -------------------------------------------------------------------------------

    public LiveData<String> getBandwidthPrimary()   {
        return Transformations.map(bandwidthSource,
                server -> Text.asGB(
                        getApplication().getApplicationContext(),
                        (server == null ? 0f : (float) server.currentBandwidth)
                )
        );
    }

    public LiveData<String> getBandwidthSecondary() {
        return Transformations.map(bandwidthSource,
                server -> Text.asGB(
                        getApplication().getApplicationContext(),
                        (server == null ? 0f : (float) server.allowedBandwidth)
                )
        );
    }

    public LiveData<String> getBandwidthTertiary() {
        return Transformations.map(bandwidthSource, server -> (server == null) ? null :
                getApplication().getString(
                        R.string.f_of_quota_on_s, server.bandwidthPercent, server.label
                )
        );
    }

    public LiveData<Integer> getBandwidthProgress()  {
        return Transformations.map(bandwidthSource,
                server -> (server == null) ? 0 : Math.round(server.bandwidthPercent)
        );
    }

    // --- LAST SNAPSHOT ---------------------------------------------------------------------------

    public LiveData<String> getLastSnapshotDate()   {
        return Transformations.map(
                SnapshotRepository
                        .getInstance()
                        .getObservableItemByIndex(0),
                snapshot -> (snapshot == null) ? null : Time.getSince(snapshot.id)
        );
    }

    public LiveData<Integer> getSnapshotCount() {
        return SnapshotRepository
                .getInstance()
                .getObservableSize();
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    public void onServerClick() {
        FragmentStack
                .getInstance()
                .push(ServersFragment.ITEM, true);
    }

    public void onBillingClick() {
        FragmentStack
                .getInstance()
                .push(BillingFragment.ITEM, true);
    }

    public void onSnapshotClick() {
        FragmentStack
                .getInstance()
                .push(SnapshotsFragment.ITEM, true);
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- BUILD SERVER SECONDARY DATA -------------------------------------------------------------

    @Nullable
    private String buildServerSecondary(@Nullable final ArrayList<Server> servers) {
        if(servers == null) { return null; }
        switch(servers.size()) {
            case 0: return null;
            case 1: return servers.get(0).location;
            default:
                final Context context = getApplication().getApplicationContext();
                final int locationCount = IntegerRepository
                        .getInstance()
                        .getItem(Key.Integer.LOCATIONS)
                        .asInt();
                return (locationCount > 1) ? context.getResources().getQuantityString(
                        R.plurals.d_locations, locationCount, locationCount
                ) : servers.get(0).location;
        }
    }

    // --- BUILD STATUS PRIMARY DATA ---------------------------------------------------------------

    @NonNull
    private String buildStatusPrimary(@Nullable final PersistentInteger runningServersCount) {
        final Context context    = getApplication().getApplicationContext();
        final int runningServers = (runningServersCount == null) ? 0 : runningServersCount.asInt();

        final int activeServers = IntegerRepository
                .getInstance()
                .getItem(Key.Integer.ACTIVE_SERVERS)
                .asInt();

        switch(runningServers) {
            case 0:
                return context.getString(activeServers > 0 ? R.string.powered_off : R.string.n_a);
            case 1:
                return (activeServers == 1) ?
                        context.getString(R.string.running) :
                        context.getString(
                                R.string.running_d_of_d, runningServers, activeServers
                        );
            default:
                return (activeServers == runningServers) ?
                        context.getResources().getQuantityString(
                                R.plurals.d_running, runningServers, runningServers
                        ) : context.getString(
                                R.string.running_d_of_d, runningServers, activeServers
                        );
        }
    }
}