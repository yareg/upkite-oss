package com.yareg.upkite.viewmodel;

import android.app.Application;
import android.content.Context;

import com.yareg.base.fragment.FragmentStack;
import com.yareg.base.livedata.SingleLiveData;
import com.yareg.base.model.PersistentInteger;
import com.yareg.base.model.PersistentPrecise;
import com.yareg.base.model.Text;
import com.yareg.base.model.Time;
import com.yareg.base.repository.IntegerRepository;
import com.yareg.base.repository.PreciseRepository;
import com.yareg.base.viewmodel.EntityViewModel;
import com.yareg.upkite.R;
import com.yareg.upkite.constant.Key;
import com.yareg.upkite.fragment.PaymentsFragment;
import com.yareg.upkite.model.Billing;
import com.yareg.upkite.repository.BillingRepository;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Transformations;

public class BillingViewModel extends EntityViewModel<Billing> {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public BillingViewModel(@NonNull Application application) { super(application); }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    protected SingleLiveData<Billing> getEntityInstance() {
        return BillingRepository.getInstance();
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- REMAINING CREDIT ------------------------------------------------------------------------

    public LiveData<String> getRemainingCreditPrimary() {
        return Transformations.map(entity, data ->
                data == null ? null : Text.asDollars(data.remainingCredit)
        );
    }

    public LiveData<String> getRemainingCreditSecondary() {
        return Transformations.map(
                IntegerRepository
                        .getInstance()
                        .getObservable(Key.Integer.MILLIS_PAID),
                this::buildRemainingCreditSecondary
        );
    }

    // --- PENDING CHARGES -------------------------------------------------------------------------

    public LiveData<String> getPendingChargesPrimary() {
        return Transformations.map(entity, data ->
                data == null ? null : Text.asDollars(data.pendingCharges)
        );
    }

    public LiveData<Integer> getPendingChargesProgress() {
        return Transformations.map(entity, billing ->
                (billing == null || billing.balance <= 0) ? null :
                        Math.round(billing.pendingCharges / billing.balance * 100)
        );
    }

    // --- ACCOUNT BALANCE -------------------------------------------------------------------------

    public LiveData<String> getAccountBalancePrimary() {
        return Transformations.map(entity, data ->
                data == null ? null : Text.asDollars(data.balance)
        );
    }

    public LiveData<String> getAccountBalanceSecondary() {
        final LiveData<PersistentPrecise> vatMultiplier = PreciseRepository
                .getInstance()
                .getObservable(Key.Precise.VAT_MULTIPLIER);

        final MediatorLiveData<String> totalMonthlyExpense = new MediatorLiveData<>();

        totalMonthlyExpense.addSource(
                entity,
                value -> totalMonthlyExpense.setValue(
                        getTotalMonthlyCostWithVat(value, vatMultiplier.getValue())
                )
        );

        totalMonthlyExpense.addSource(
                vatMultiplier,
                value -> totalMonthlyExpense.setValue(
                        getTotalMonthlyCostWithVat(entity.getValue(), value)
                )
        );

        return totalMonthlyExpense;
    }

    // --- LAST TOP UP -----------------------------------------------------------------------------

    public LiveData<String> getLastTopUpPrimary() {
        return Transformations.map(entity, data ->
                data == null ? null : Text.asDollars(data.lastPaymentAmount)
        );
    }

    public LiveData<String> getLastTopUpSecondary() {
        return Transformations.map(entity, data ->
                data == null ? null : Time.getDate(
                        getApplication().getApplicationContext(), data.lastPaymentDate
                )
        );
    }

    public void onLastTopUpClick() {
        FragmentStack.getInstance().push(PaymentsFragment.ITEM);
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- BUILD REMAINING CREDIT SECONDARY --------------------------------------------------------

    @NonNull
    private String buildRemainingCreditSecondary(@Nullable final PersistentInteger millis) {
        final Context context = getApplication().getApplicationContext();
        if(millis == null) {
            return context.getString(R.string.remaining_credit);
        }
        if(millis.value < 0) {
            return context.getString(R.string.account_is_overdrawn);
        }
        final long time = millis.value + Time.getUnixTime();
        return context.getString(
                R.string.enough_until,
                Time.getDate(time, Time.Pattern.MONTH_FULL),
                Time.getDate(time, Time.Pattern.DAY)
        );
    }

    // --- GET TOTAL MONTHLY COST WITH VAT ---------------------------------------------------------
    @Nullable
    private String getTotalMonthlyCostWithVat(
            @Nullable final Billing billing,
            @Nullable final PersistentPrecise vatMultiplier) {
        if(billing == null) { return null; }
        return Text.asDollars(
                billing.totalMonthlyCost *
                        ((vatMultiplier == null) ? 1.0f : vatMultiplier.asFloat())
        );
    }
}
