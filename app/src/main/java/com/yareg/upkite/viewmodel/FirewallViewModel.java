package com.yareg.upkite.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.yareg.base.livedata.ListLiveData;
import com.yareg.base.viewmodel.RepositoryViewModel;
import com.yareg.upkite.model.FirewallGroup;

public class FirewallViewModel extends RepositoryViewModel<FirewallGroup> {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public FirewallViewModel(@NonNull final Application application) {
        super(application);
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    protected ListLiveData<FirewallGroup> getRepositoryInstance() {
        return new ListLiveData<>(new FirewallGroup(), true);
    }
}
