package com.yareg.upkite.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.yareg.base.livedata.ListLiveData;
import com.yareg.base.viewmodel.RepositoryViewModel;
import com.yareg.upkite.model.Payment;

public class PaymentViewModel extends RepositoryViewModel<Payment> {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public PaymentViewModel(@NonNull final Application application) {
        super(application);
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    protected ListLiveData<Payment> getRepositoryInstance() {
        return new ListLiveData<>(new Payment(), true);
    }
}
