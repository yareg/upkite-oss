package com.yareg.upkite.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;

import com.yareg.base.livedata.ListLiveData;
import com.yareg.base.viewmodel.RepositoryViewModel;
import com.yareg.upkite.model.Task;

public class TaskViewModel extends RepositoryViewModel<Task> {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public TaskViewModel(@NonNull final Application application) {
        super(application);
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    protected ListLiveData<Task> getRepositoryInstance() {
        return new ListLiveData<>(new Task(), true);
    }
}
