package com.yareg.upkite.fragment;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.yareg.base.activity.BaseActivity;
import com.yareg.base.list.ItemListAdapter;
import com.yareg.base.databinding.FragmentListBinding;
import com.yareg.base.fragment.BaseFragment;
import com.yareg.base.fragment.FragmentItem;
import com.yareg.base.fragment.ListFragment;
import com.yareg.upkite.BR;
import com.yareg.upkite.R;
import com.yareg.upkite.dialog.PaymentDialog;
import com.yareg.upkite.model.Payment;
import com.yareg.upkite.viewmodel.PaymentViewModel;

public class PaymentsFragment extends ListFragment {

    public static final FragmentItem ITEM = new FragmentItem(
            PaymentsFragment.class.hashCode(),
            0,
            R.string.top_up_history,
            0,
            0,
            false,
            0,
            null,
            new Creator(),
            false
    );

    public static class Creator extends BaseFragment.Creator {
        public Fragment createInstance() { return new PaymentsFragment(); }
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    protected void onBindView(
            @NonNull final BaseActivity activity,
            @NonNull final FragmentListBinding binding) {
        super.onBindView(activity, binding);

        final PaymentViewModel viewModel = activity
                .getViewModelProvider()
                .get(PaymentViewModel.class);

        binding.setItemCount(viewModel.getItemCount());
        binding.setLifecycleOwner(getViewLifecycleOwner());

        final ItemListAdapter<Payment> adapter = new ItemListAdapter<>(
                R.layout.list_item_payment,
                BR.item,
                viewModel.getRepository().getValue()
        );

        list.setLayoutManager(layoutManager);
        list.setAdapter(adapter);

        adapter.setOnItemClickListener((position)->
                PaymentDialog
                        .createInstance(adapter.getItemId(position))
                        .show(activity.getSupportFragmentManager())
        );

        viewModel.getRepository().observe(getViewLifecycleOwner(), adapter::updateList);
    }
}
