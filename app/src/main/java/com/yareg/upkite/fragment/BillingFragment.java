package com.yareg.upkite.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;

import com.yareg.base.fragment.BaseFragment;
import com.yareg.base.fragment.FragmentItem;
import com.yareg.base.manager.BaseUrlManager;
import com.yareg.base.model.FabData;
import com.yareg.upkite.R;
import com.yareg.upkite.activity.AppActivity;
import com.yareg.upkite.databinding.FragmentBillingBinding;
import com.yareg.upkite.viewmodel.BillingViewModel;

public class BillingFragment extends BaseFragment {

    public static final FragmentItem ITEM = new FragmentItem(
            BillingFragment.class.hashCode(),
            0,
            R.string.remaining_credit,
            0,
            0,
            false,
            0,
            new FabData(R.drawable.icon_add, R.string.add_money),
            new Creator(),
            false
    );

    public static class Creator extends BaseFragment.Creator {
        public Fragment createInstance() { return new BillingFragment(); }
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedState) {

        final AppActivity activity = (AppActivity) getActivity();
        if(activity == null) { return null; }

        final FragmentBillingBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_billing, container, false
        );

        final LifecycleOwner lifecycleOwner = getViewLifecycleOwner();

        final BillingViewModel viewModel = activity
                .getViewModelProvider()
                .get(BillingViewModel.class);

        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(lifecycleOwner);

        activity.getUi().addFabAction(ITEM.id, ()-> BaseUrlManager.openInBrowser(
                activity, getString(R.string.url_vultr_login)
        ));

        return binding.getRoot();
    }
}
