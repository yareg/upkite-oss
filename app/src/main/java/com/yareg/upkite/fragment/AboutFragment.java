package com.yareg.upkite.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yareg.base.fragment.BaseFragment;
import com.yareg.base.fragment.FragmentStack;
import com.yareg.base.fragment.FragmentItem;
import com.yareg.base.manager.BaseUrlManager;
import com.yareg.upkite.BuildConfig;
import com.yareg.upkite.R;
import com.yareg.upkite.constant.Value;
import com.yareg.upkite.databinding.FragmentAboutBinding;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

public class AboutFragment extends BaseFragment {

    public static final FragmentItem ITEM = new FragmentItem(
            AboutFragment.class.hashCode(),
            0,
            R.string.about,
            0,
            0,
            true,
            R.id.menu_navigation_about,
            null,
            new Creator(),
            true
    );

    public static class Creator extends BaseFragment.Creator {
        public Fragment createInstance() { return new AboutFragment(); }
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedState) {

        final FragmentAboutBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_about, container, false
        );

        binding.setAppIdSecondary(BuildConfig.APPLICATION_ID);
        binding.setAppVersionSecondary(BuildConfig.VERSION_NAME);
        binding.setBuildTypeSecondary(BuildConfig.BUILD_TYPE);
        binding.setDatabaseVersion(Integer.toString(Value.Database.VERSION));

        binding.setOnDeveloperClick((@NonNull final View view)-> BaseUrlManager.openInBrowser(
                view.getContext(), getString(R.string.url_developer)
        ));
        binding.setOnPrivacyClick((@NonNull final View view)-> BaseUrlManager.openInBrowser(
                view.getContext(), getString(R.string.url_privacy_policy)
        ));
        binding.setOnIconsClick((@NonNull final View view)-> BaseUrlManager.openInBrowser(
                view.getContext(), getString(R.string.url_material_design_icons)
        ));
        binding.setOnDependenciesClick((@NonNull final View view)-> FragmentStack.getInstance().push(
                DependenciesFragment.ITEM, true
        ));

        return binding.getRoot();
    }
}