package com.yareg.upkite.fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yareg.base.fragment.BaseFragment;
import com.yareg.base.fragment.FragmentItem;
import com.yareg.upkite.R;
import com.yareg.upkite.activity.AppActivity;
import com.yareg.upkite.databinding.FragmentMainBinding;
import com.yareg.upkite.viewmodel.DashboardViewModel;
import com.yareg.upkite.viewmodel.ProfileViewModel;

public class MainFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    public static final FragmentItem ITEM = new FragmentItem(
            MainFragment.class.hashCode(),
            0,
            R.string.dashboard,
            R.menu.menu_options_main,
            0,
            true,
            R.id.menu_navigation_main,
            null,
            new Creator(),
            false
    );

    public static final class Creator extends BaseFragment.Creator {
        public Fragment createInstance() { return new MainFragment(); }
    }

    //----------------------------------------------------------------------------------------------
    //
    // DATA BINDING
    //
    //----------------------------------------------------------------------------------------------

    private SwipeRefreshLayout swipeRefreshLayout;

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedState) {

        final AppActivity activity = (AppActivity) getActivity();
        if (activity == null) { return null; }

        activity.setIsLoading(true);

        final DashboardViewModel viewModel = activity
                .getViewModelProvider()
                .get(DashboardViewModel.class);

        final LifecycleOwner lifecycleOwner = getViewLifecycleOwner();

        viewModel.isDataReady().observe(lifecycleOwner, new Observer<Boolean>() {
            @Override
            public void onChanged(@NonNull final Boolean isDataReady) {
                if (isDataReady) {
                    viewModel.isDataReady().removeObserver(this);
                    activity.setIsLoading(false);
                }
            }
        });

        activity.getViewModelProvider().get(ProfileViewModel.class).getSyncState().observe(
                getViewLifecycleOwner(), (syncState) -> {
                    if (syncState.isIdle()) { swipeRefreshLayout.setRefreshing(false); }
                }
        );

        final FragmentMainBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_main, container, false
        );

        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(lifecycleOwner);

        swipeRefreshLayout = binding.srFragmentMainRefresh;
        swipeRefreshLayout.setColorSchemeResources(R.color.accent);
        swipeRefreshLayout.setOnRefreshListener(this);

        return binding.getRoot();
    }

    @Override
    public void onRefresh () {
        final AppActivity activity = (AppActivity) getActivity();
        if (activity == null) { return; }
        activity.getViewModelProvider().get(ProfileViewModel.class).syncAccount(true);
    }
}
