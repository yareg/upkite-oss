package com.yareg.upkite.fragment;

import android.accounts.Account;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.view.inputmethod.EditorInfo;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.preference.EditTextPreference;
import androidx.preference.ListPreference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreference;

import com.yareg.account.manager.BaseAccountManager;
import com.yareg.base.fragment.BaseFragment;
import com.yareg.base.fragment.FragmentItem;
import com.yareg.base.model.Time;
import com.yareg.base.repository.IntegerRepository;
import com.yareg.base.repository.PreciseRepository;
import com.yareg.upkite.R;
import com.yareg.upkite.activity.AppActivity;
import com.yareg.upkite.app.App;
import com.yareg.upkite.constant.Key;

public class SettingsFragment extends PreferenceFragmentCompat {

    public static final FragmentItem ITEM = new FragmentItem(
            SettingsFragment.class.hashCode(),
            0,
            R.string.settings,
            R.menu.menu_options_settings,
            0,
            true,
            R.id.menu_navigation_settings,
            null,
            new Creator(),
            false
    );

    public static class Creator extends BaseFragment.Creator {
        public Fragment createInstance() { return new SettingsFragment(); }
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE
    //
    //----------------------------------------------------------------------------------------------

    private SharedPreferences.OnSharedPreferenceChangeListener changeListener;

    private ListPreference     themeList;
    private SwitchPreference   syncSwitch;
    private ListPreference     syncFrequency;
    private SwitchPreference   notificationTopUp;
    private SwitchPreference   notificationLowBalance;
    private SwitchPreference   additionalVatSwitch;
    private EditTextPreference additionalVatPercent;

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public void onCreatePreferences(Bundle savedState, String rootKey) {
        addPreferencesFromResource(R.xml.app_preferences);
        bindPreferences();
        setDependentPreferences();

        changeListener = (final SharedPreferences preferences, final String key)-> {
            switch(key)
            {
                case Key.Preference.Appearance.ThemeId:
                    onThemeChanged();
                    break;

                case Key.Preference.Sync.Enabled:
                    onSyncSwitched();
                    break;

                case Key.Preference.Sync.Frequency:
                    onSyncFrequencyChanged();
                    break;

                case Key.Preference.Notification.TopUp:
                    onNotificationTopUpSwitched();
                    break;

                case Key.Preference.Notification.LowBalance:
                    onNotificationLowBalanceSwitched();
                    break;

                case Key.Preference.Billing.AdditionalVatEnabled:
                    onAdditionalVatSwitched();
                    break;

                case Key.Preference.Billing.AdditionalVatPercent:
                    onAdditionalVatChanged();
                    break;

                default: //Log.e(TAG, "unrecognized preference token: " + key);
            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen()
                .getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(changeListener);

    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen()
                .getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(changeListener);
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- BIND PREFERENCES ------------------------------------------------------------------------

    private void bindPreferences() {
        themeList              = findPreference(Key.Preference.Appearance.ThemeId);
        syncSwitch             = findPreference(Key.Preference.Sync.Enabled);
        syncFrequency          = findPreference(Key.Preference.Sync.Frequency);
        notificationTopUp      = findPreference(Key.Preference.Notification.TopUp);
        notificationLowBalance = findPreference(Key.Preference.Notification.LowBalance);
        additionalVatSwitch    = findPreference(Key.Preference.Billing.AdditionalVatEnabled);
        additionalVatPercent   = findPreference(Key.Preference.Billing.AdditionalVatPercent);
    }

    // --- SET DEPENDENT PREFERENCES ---------------------------------------------------------------

    private void setDependentPreferences() {
        syncFrequency.setVisible(syncSwitch.isChecked());
        themeList.setSummary(themeList.getEntry());
        syncFrequency.setSummary(syncFrequency.getEntry());

        additionalVatPercent.setVisible(additionalVatSwitch.isChecked());
        if (additionalVatSwitch.isChecked()) {
            additionalVatPercent.setOnBindEditTextListener((editText)-> {
                editText.setInputType(
                        InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL
                );
                editText.setImeOptions(EditorInfo.IME_ACTION_DONE);
                //editText.setMaxLines(1);
            });
            additionalVatPercent.setTitle(
                    additionalVatPercent
                            .getContext()
                            .getString(R.string.s_percent, additionalVatPercent.getText())
            );
        }
    }

    // --- ON THEME CHANGED ------------------------------------------------------------------------

    private void onThemeChanged() {

        int themeId   = 0;
        int nightMode = Key.Theme.Auto;

        try {
            themeId = java.lang.Integer.parseInt(themeList.getValue());
        } catch (NumberFormatException e) {
            //Log.e(TAG, e.toString());
        }

        switch(themeId){
            case 0: break;
            case 1: nightMode = Key.Theme.Light; break;
            case 2: nightMode = Key.Theme.Dark; break;
            default: //Log.e(TAG, "unexpected theme id: " + themeId);
        }

        if (nightMode == App.THEME_ID) {
            //Log.e(TAG, "Switching to the same theme, from " + App.THEME_ID + " to " + themeId);
            return;
        }

        //Log.i(TAG, "Switching theme to " + themeId);

        App.THEME_ID = nightMode;

        IntegerRepository
                .getInstance()
                .putLong(Key.Integer.THEME_ID, App.THEME_ID);

        AppCompatDelegate.setDefaultNightMode(App.THEME_ID);
    }

    // --- ON SYNC SWITCHED ------------------------------------------------------------------------

    private void onSyncSwitched() {

        final AppActivity activity = (AppActivity) getActivity();
        if (activity == null) { return; }

        final Account account = activity.getProfileViewModel().getPairedAccount();
        if (account == null) { return; }

        final boolean isEnabled = syncSwitch.isChecked();

        syncFrequency.setVisible(isEnabled);

        BaseAccountManager.setSyncAutomatically(account, isEnabled);
        BaseAccountManager.setSyncFrequency(account, isEnabled ? getSyncFrequencyValue() : 0);
    }

    // --- ON SYNC FREQUENCY CHANGED ---------------------------------------------------------------

    private void onSyncFrequencyChanged() {

        final AppActivity activity = (AppActivity) getActivity();
        if (activity == null) { return; }

        final Account account = activity.getProfileViewModel().getPairedAccount();
        if (account == null) { return; }

        syncFrequency.setSummary(syncFrequency.getEntry());

        BaseAccountManager.setSyncFrequency(account, getSyncFrequencyValue());

        activity.getProfileViewModel().syncAccount(false);
    }

    // --- GET SYNC FREQUENCY VALUE ----------------------------------------------------------------

    private long getSyncFrequencyValue() {
        return Integer.parseInt(syncFrequency.getValue()) * Time.MINUTE;
    }

    // --- ON NOTIFICATION TOP UP SWITCHED ---------------------------------------------------------

    private void onNotificationTopUpSwitched() {
        IntegerRepository
                .getInstance()
                .putBoolean(
                        Key.Integer.NOTIFY_ON_TOP_UP,
                        notificationTopUp.isChecked()
                );
    }

    // --- ON NOTIFICATION LOW BALANCE SWITCHED ----------------------------------------------------

    private void onNotificationLowBalanceSwitched() {
        IntegerRepository
                .getInstance()
                .putBoolean(
                        Key.Integer.NOTIFY_ON_LOW_BALANCE,
                        notificationLowBalance.isChecked()
                );
    }

    // --- ON VAT SWITCHED -------------------------------------------------------------------------

    private void onAdditionalVatSwitched() {
        additionalVatPercent.setVisible(additionalVatSwitch.isChecked());

        if (additionalVatSwitch.isChecked()) {
            onAdditionalVatChanged();
        } else {
            PreciseRepository
                    .getInstance()
                    .putFloat(Key.Precise.VAT_MULTIPLIER, 1.0f);
        }
    }

    // --- ON VAT CHANGED --------------------------------------------------------------------------

    private void onAdditionalVatChanged() {
        final float percent  = Float.parseFloat(additionalVatPercent.getText());
        final float newValue = (percent > 0.0f) ? (1.0f + percent / 100.00f) : 1.0f;

        final float currentValue = PreciseRepository
                .getInstance()
                .getItem(Key.Precise.VAT_MULTIPLIER)
                .asFloat();

        if (newValue != currentValue) {
            PreciseRepository
                    .getInstance()
                    .putFloat(Key.Precise.VAT_MULTIPLIER, newValue);

            additionalVatPercent.setTitle(
                    additionalVatPercent
                            .getContext()
                            .getString(R.string.s_percent, additionalVatPercent.getText())
            );
        }
    }
}
