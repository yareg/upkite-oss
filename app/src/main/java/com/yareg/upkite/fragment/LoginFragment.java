package com.yareg.upkite.fragment;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.yareg.account.model.SyncState;
import com.yareg.base.fragment.BaseFragment;
import com.yareg.base.fragment.FragmentItem;
import com.yareg.base.fragment.FragmentStack;
import com.yareg.base.manager.BaseUrlManager;
import com.yareg.base.model.FabData;
import com.yareg.upkite.R;
import com.yareg.upkite.activity.AppActivity;
import com.yareg.upkite.databinding.FragmentLoginBinding;
import com.yareg.upkite.viewmodel.LoginViewModel;
import com.yareg.upkite.viewmodel.ProfileViewModel;

public class LoginFragment extends BaseFragment {

    public static final FragmentItem ITEM = new FragmentItem(
            LoginFragment.class.hashCode(),
            0,
            R.string.account_setup,
            R.menu.menu_options_login,
            0,
            false,
            0,
            new FabData(R.drawable.icon_add, R.string.add_account),
            new Creator(),
            true
    );

    public static final class Creator extends BaseFragment.Creator {
        public Fragment createInstance() { return new LoginFragment(); }
    }

    //----------------------------------------------------------------------------------------------
    //
    // DATA BINDING
    //
    //----------------------------------------------------------------------------------------------

    private FragmentLoginBinding binding;

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedState) {

        final AppActivity activity = (AppActivity) getActivity();
        if (activity == null) { return null; }

        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_login, container, false
        );

        activity
                .getViewModelProvider()
                .get(LoginViewModel.class)
                .getIsInputEnabled()
                .observe(getViewLifecycleOwner(), this::onInputStateUpdated);

        activity
               .getViewModelProvider()
               .get(ProfileViewModel.class)
               .getSyncState()
               .observe(getViewLifecycleOwner(), this::onSyncStateUpdated);

        binding.setOnEditorAction(this::onEditorAction);
        binding.setOnPrivacyClick((@NonNull final View view)-> BaseUrlManager.openInBrowser(
                view.getContext(), getString(R.string.url_privacy_policy)
        ));

        activity.getUi().addFabAction(ITEM.id, this::fabAction);

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        final AppActivity activity = (AppActivity) getActivity();
        if (activity == null) { return; }

        activity.getUi().removeFabAction(ITEM.id);
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- FAB ACTION ------------------------------------------------------------------------------

    private void fabAction() {

        final AppActivity activity = (AppActivity) getActivity();
        if (activity == null) { return; }

        final String token = binding.etFragmentLoginKey
                .getText()
                .toString()
                .trim()
                .toUpperCase();

        activity
                .getViewModelProvider()
                .get(LoginViewModel.class)
                .login(activity.getViewModelProvider().get(ProfileViewModel.class), token);
    }

    // --- ON EDITOR ACTION ------------------------------------------------------------------------

    private boolean onEditorAction(
            @NonNull final TextView view,
            final int actionId,
            @NonNull final KeyEvent event) {

        if (actionId == EditorInfo.IME_ACTION_NEXT) {
            final AppActivity activity = (AppActivity) getActivity();
            if (activity == null) { return false; }
            activity.getUi().runFabAction();
            return true;
        }
        return false;
    }

    // --- ON INPUT STATE UPDATED ------------------------------------------------------------------

    private void onInputStateUpdated(@Nullable final Boolean isEnabled) {
        if (isEnabled == null) { return; }
        final AppActivity activity = (AppActivity) getActivity();
        if (activity == null) { return; }
        binding.setInputEnabled(isEnabled);
        activity.getUi().setFabEnabled(isEnabled);
    }

    // --- ON SYNC STATUS UPDATED ------------------------------------------------------------------

    private void onSyncStateUpdated(@NonNull final SyncState syncState) {
        if (syncState.secondary == SyncState.SUCCEEDED) {
            FragmentStack.getInstance().clearAndPush(MainFragment.ITEM);
        }
    }
}
