package com.yareg.upkite.fragment;

import com.yareg.base.activity.BaseActivity;
import com.yareg.base.list.ItemListAdapter;
import com.yareg.base.databinding.FragmentListBinding;
import com.yareg.base.fragment.BaseFragment;
import com.yareg.base.fragment.FragmentItem;
import com.yareg.base.fragment.ListFragment;
import com.yareg.upkite.BR;
import com.yareg.upkite.R;
import com.yareg.upkite.dialog.ServerDialog;
import com.yareg.upkite.model.Server;
import com.yareg.upkite.viewmodel.ServerViewModel;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

public class ServersFragment extends ListFragment {

    public static final FragmentItem ITEM = new FragmentItem(
            ServersFragment.class.hashCode(),
            0,
            R.string.servers,
            0,
            0,
            true,
            R.id.menu_navigation_servers,
            null,
            new Creator(),
            false
    );

    public static class Creator extends BaseFragment.Creator {
        public Fragment createInstance() { return new ServersFragment(); }
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    protected void onBindView(
            @NonNull final BaseActivity activity,
            @NonNull final FragmentListBinding binding) {
        super.onBindView(activity, binding);

        final ServerViewModel viewModel = activity
                .getViewModelProvider()
                .get(ServerViewModel.class);

        binding.setItemCount(viewModel.getItemCount());
        binding.setLifecycleOwner(getViewLifecycleOwner());

        final ItemListAdapter<Server> adapter = new ItemListAdapter<>(
                R.layout.list_item_server,
                BR.item,
                viewModel.getRepository().getValue()
        );

        list.setLayoutManager(layoutManager);
        list.setAdapter(adapter);

        adapter.setOnItemClickListener((position)->
                ServerDialog
                        .createInstance(adapter.getItemId(position))
                        .show(activity.getSupportFragmentManager())

        );

        viewModel.getRepository().observe(getViewLifecycleOwner(), adapter::updateList);
    }
}
