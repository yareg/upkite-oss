package com.yareg.upkite.fragment;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.yareg.base.activity.BaseActivity;
import com.yareg.base.list.ItemListAdapter;
import com.yareg.base.databinding.FragmentListBinding;
import com.yareg.base.fragment.BaseFragment;
import com.yareg.base.fragment.FragmentItem;
import com.yareg.base.fragment.ListFragment;
import com.yareg.upkite.BR;
import com.yareg.upkite.R;
import com.yareg.upkite.dialog.TaskDialog;
import com.yareg.upkite.model.Task;
import com.yareg.upkite.viewmodel.TaskViewModel;

public class TasksFragment extends ListFragment {

    public static final FragmentItem ITEM = new FragmentItem(
            TasksFragment.class.hashCode(),
            0,
            R.string.tasks,
            0,
            0,
            true,
            0,
            null,
            new Creator(),
            false
    );

    public static class Creator extends BaseFragment.Creator {
        public Fragment createInstance() { return new TasksFragment(); }
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    protected void onBindView(
            @NonNull final BaseActivity activity,
            @NonNull final FragmentListBinding binding) {
        super.onBindView(activity, binding);

        final TaskViewModel viewModel = activity
                .getViewModelProvider()
                .get(TaskViewModel.class);

        binding.setItemCount(viewModel.getItemCount());
        binding.setLifecycleOwner(getViewLifecycleOwner());

        final ItemListAdapter<Task> adapter = new ItemListAdapter<>(
                R.layout.list_item_task,
                BR.item,
                viewModel.getRepository().getValue()
        );

        list.setLayoutManager(layoutManager);
        list.setAdapter(adapter);

        adapter.setOnItemClickListener((position)->
                TaskDialog
                        .createInstance(adapter.getItemId(position))
                        .show(activity.getSupportFragmentManager())
        );

        viewModel.getRepository().observe(getViewLifecycleOwner(), adapter::updateList);
    }
}
