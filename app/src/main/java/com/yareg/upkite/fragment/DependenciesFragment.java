package com.yareg.upkite.fragment;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.yareg.base.activity.BaseActivity;
import com.yareg.base.databinding.FragmentListBinding;
import com.yareg.base.fragment.BaseFragment;
import com.yareg.base.fragment.FragmentItem;
import com.yareg.base.fragment.ListFragment;
import com.yareg.upkite.R;
import com.yareg.upkite.adapter.DependencyListAdapter;
import com.yareg.upkite.model.Dependency;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class DependenciesFragment extends ListFragment {

    public static final FragmentItem ITEM = new FragmentItem(
            DependenciesFragment.class.hashCode(),
            0,
            R.string.open_source_libraries,
            0,
            0,
            false,
            0,
            null,
            new Creator(),
            true
    );

    public static class Creator extends BaseFragment.Creator {
        public Fragment createInstance() { return new DependenciesFragment(); }
    }

    private static class Dependencies {
        @SerializedName("dependencies")
        ArrayList<Dependency> list;
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    protected void onBindView(
            @NonNull final BaseActivity activity,
            @NonNull final FragmentListBinding binding) {
        super.onBindView(activity, binding);

        final InputStream inputStream = getResources().openRawResource(R.raw.libraries);
        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        final StringBuilder builder = new StringBuilder();

        String line;

        try {
            while((line = bufferedReader.readLine()) != null) { builder.append(line); }

            final Dependencies dependencies = new Gson().fromJson(
                    builder.toString(),
                    Dependencies.class
            );

            final MutableLiveData<Integer> itemCount = new MutableLiveData<>();
            itemCount.setValue(dependencies.list.size());

            binding.setItemCount(itemCount);
            binding.setLifecycleOwner(getViewLifecycleOwner());

            list.setLayoutManager(layoutManager);
            list.setAdapter(new DependencyListAdapter(dependencies.list));

        } catch (IOException e) {
            //e.printStackTrace();
        }
    }
}
