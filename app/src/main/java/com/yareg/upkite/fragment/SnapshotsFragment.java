package com.yareg.upkite.fragment;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.yareg.base.activity.BaseActivity;
import com.yareg.base.list.ItemListAdapter;
import com.yareg.base.databinding.FragmentListBinding;
import com.yareg.base.fragment.BaseFragment;
import com.yareg.base.fragment.FragmentItem;
import com.yareg.base.fragment.ListFragment;
import com.yareg.upkite.BR;
import com.yareg.upkite.R;
import com.yareg.upkite.dialog.SnapshotDialog;
import com.yareg.upkite.model.Snapshot;
import com.yareg.upkite.viewmodel.SnapshotViewModel;

public class SnapshotsFragment extends ListFragment {

    public static final FragmentItem ITEM = new FragmentItem(
            SnapshotsFragment.class.hashCode(),
            0,
            R.string.snapshots,
            0,
            0,
            true,
            R.id.menu_navigation_snapshots,
            null, //new FabData(R.drawable.icon_add, R.string.add_snapshot),
            new Creator(),
            false
    );

    public static class Creator extends BaseFragment.Creator {
        public Fragment createInstance() { return new SnapshotsFragment(); }
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    protected void onBindView(
            @NonNull final BaseActivity activity,
            @NonNull final FragmentListBinding binding) {
        super.onBindView(activity, binding);

        final SnapshotViewModel viewModel = activity
                .getViewModelProvider()
                .get(SnapshotViewModel.class);

        binding.setItemCount(viewModel.getItemCount());
        binding.setLifecycleOwner(getViewLifecycleOwner());

        //activity.getUi().addFabAction(ITEM.id, viewModel::createSnapshot);

        final ItemListAdapter<Snapshot> adapter = new ItemListAdapter<>(
                R.layout.list_item_snapshot,
                BR.item,
                viewModel.getRepository().getValue()
        );

        list.setLayoutManager(layoutManager);
        list.setAdapter(adapter);

        adapter.setOnItemClickListener((position)->
                SnapshotDialog
                        .createInstance(adapter.getItemId(position))
                        .show(activity.getSupportFragmentManager())

        );

        viewModel.getRepository().observe(getViewLifecycleOwner(), adapter::updateList);

        //IntegerRepository.getInstance(activity).getObservableItem(Key.Integer.LAST_SNAPSHOT).observe();
    }
}
