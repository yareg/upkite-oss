package com.yareg.upkite.sync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;

import com.yareg.upkite.constant.Key;
import com.yareg.upkite.work.AppWorkManager;
import com.yareg.upkite.work.BillingSync;
import com.yareg.upkite.work.FirewallSync;
import com.yareg.upkite.work.ProfileSync;
import com.yareg.upkite.work.ServersSync;
import com.yareg.upkite.work.SnapshotsSync;

class SyncAdapter extends AbstractThreadedSyncAdapter {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    SyncAdapter(@NonNull final Context context) {
        super(context, false, false);
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public void onPerformSync(
            @NonNull final Account account,
            @Nullable final Bundle extras,
            @NonNull final String authority,
            @NonNull final ContentProviderClient provider,
            @NonNull final SyncResult syncResult) {

        final Constraints constraints = new Constraints
                .Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        final OneTimeWorkRequest profileSync = new OneTimeWorkRequest
                .Builder(ProfileSync.class)
                .addTag("profile_sync")
                .setConstraints(constraints)
                .build();

        final OneTimeWorkRequest serverSync = new OneTimeWorkRequest
                .Builder(ServersSync.class)
                .addTag("server_sync")
                .setConstraints(constraints)
                //.setInitialDelay(500, TimeUnit.MILLISECONDS)
                .build();

        final OneTimeWorkRequest billingSync = new OneTimeWorkRequest
                .Builder(BillingSync.class)
                .addTag("billing_sync")
                .setConstraints(constraints)
                //.setInitialDelay(500, TimeUnit.MILLISECONDS)
                .build();

        final OneTimeWorkRequest snapshotsSync = new OneTimeWorkRequest
                .Builder(SnapshotsSync.class)
                .addTag("sync_snapshots")
                .setConstraints(constraints)
                //.setInitialDelay(500, TimeUnit.MILLISECONDS)
                .build();

        final OneTimeWorkRequest firewallSync = new OneTimeWorkRequest
                .Builder(FirewallSync.class)
                .addTag("firewall_sync")
                .setConstraints(constraints)
                //.setInitialDelay(500, TimeUnit.MILLISECONDS)
                .build();

        final AppWorkManager workManager = AppWorkManager.getInstance(getContext());

        workManager
                .getManager()
                .beginUniqueWork(Key.Work.SYNC, workManager.getExistingWorkPolicy(), profileSync)
                .then(serverSync)
                .then(billingSync)
                .then(snapshotsSync)
                .then(firewallSync)
                .enqueue();
    }
}
