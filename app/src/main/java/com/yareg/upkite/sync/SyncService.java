package com.yareg.upkite.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import androidx.annotation.Nullable;

public class SyncService extends Service {

    private static final Object lock = new Object();
    private static SyncAdapter syncAdapter;

    @Override
    public void onCreate() {
        synchronized (lock) {
            syncAdapter = new SyncAdapter(getApplicationContext());
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return syncAdapter.getSyncAdapterBinder();
    }
}
