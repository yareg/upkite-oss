package com.yareg.upkite.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.google.android.material.navigation.NavigationView;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.yareg.base.activity.FragmentActivity;
import com.yareg.base.fragment.FragmentItem;
import com.yareg.base.model.FabData;
import com.yareg.base.ui.BaseDrawerLayout;
import com.yareg.base.ui.Ui;
import com.yareg.upkite.R;
import com.yareg.upkite.databinding.AppBaseBinding;
import com.yareg.upkite.databinding.AppFabBinding;

public abstract class NavigationDrawerActivity extends FragmentActivity {

    //----------------------------------------------------------------------------------------------
    //
    // DATA BINDING
    //
    //----------------------------------------------------------------------------------------------

    private AppBaseBinding binding;

    private BaseDrawerLayout      drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private NavigationView        navigationView;
    private Toolbar               toolbar;

    //----------------------------------------------------------------------------------------------
    //
    // SUPER OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.app_base);

        baseLayout   = binding.appBase;
        contentFrame = binding.appBaseDrawer.appFrame;
        drawerLayout = binding.appBaseDrawer.appDrawer;
        toolbar      = binding.appBaseToolbar.appToolbar;

        createToolbar();
        createFab();
        createNavigationView();
    }

    @Override
    protected void onPostCreate(@Nullable final Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
        setFragmentContainerViewId(R.id.fl_fragment);
    }

    @Override
    public boolean onCreateOptionsMenu(@NonNull final Menu menu) {
        if (ui.optionsMenu != 0) {
            getMenuInflater().inflate(ui.optionsMenu, menu);
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull final MenuItem item) {
        return drawerToggle.onOptionsItemSelected(item) ||
        super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(@NonNull final Configuration configuration) {
        super.onConfigurationChanged(configuration);
        drawerToggle.onConfigurationChanged(configuration);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.closeDrawer()) { return; }
        super.onBackPressed();
    }

    @Override
    protected void onFragmentTransaction(@NonNull final FragmentItem item) {
        super.onFragmentTransaction(item);

        setActiveNavigationItemId(item.drawerItemId);
        setToolbarTitle((item.titleStringId > 0) ? getString(item.titleStringId) : item.tag);
        setDrawerToggle(item.drawerMenu, ui.getFragmentStack().isBackEnabled());
        setOptionsMenu(item.optionsMenu);

        final boolean drawerLock = (!item.drawerMenu || ui.getFragmentStack().isBackEnabled());

        if (ui.forceCloseDrawer) {
            ui.forceCloseDrawer = false;
            drawerLayout.lockDrawer(true);
            drawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                    drawerLayout.lockDrawer(drawerLock);
                    drawerLayout.removeDrawerListener(this);
                }
            });
            drawerLayout.getHandler().postDelayed(
                    ()-> drawerLayout.closeDrawer(),
                    Ui.FRAME_MILLIS
            );
        } else if (drawerLock != drawerLayout.isDrawerLocked()) {
            drawerLayout.lockDrawer(drawerLock);
        }

        //drawerLayout.discardTouches();
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- LOADING ---------------------------------------------------------------------------------

    public void setIsLoading(final boolean isLoading) {
        binding.appBaseDrawer.setIsLoading(isLoading);
    }

    // --- SYNCING ---------------------------------------------------------------------------------

    public void setIsSyncing(final boolean isSyncing) {
        binding.appBaseDrawer.setIsSyncing(isSyncing);
    }

    //----------------------------------------------------------------------------------------------
    //
    // PRIVATE METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- CREATE TOOLBAR --------------------------------------------------------------------------

    private void createToolbar() {

        setSupportActionBar(toolbar);

        drawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.open_drawer, R.string.close_drawer) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                ui.isDrawerOpen = true;
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                ui.isDrawerOpen = false;
            }

            @Override
            public void syncState() {
                super.syncState();
                ui.isDrawerOpen = drawerLayout.isDrawerOpen();
            }
        };

        drawerLayout.addDrawerListener(drawerToggle);
    }

    // --- SET TOOLBAR TITLE -----------------------------------------------------------------------

    private void setToolbarTitle(@NonNull final String title) { toolbar.setTitle(title); }

    // --- SET DRAWER TOGGLE -----------------------------------------------------------------------

    private void setDrawerToggle(final boolean isEnabled, final boolean isBackAction) {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar == null) { return; }
        if (isBackAction) {
            drawerToggle.setDrawerIndicatorEnabled(false);
            drawerToggle.setToolbarNavigationClickListener((view)-> onBackPressed());
            actionBar.setDisplayHomeAsUpEnabled(true);
        } else {
            actionBar.setDisplayHomeAsUpEnabled(false);
            drawerToggle.setToolbarNavigationClickListener(null);
            drawerToggle.setDrawerIndicatorEnabled(isEnabled);
        }
    }

    // --- SET OPTIONS MENU ------------------------------------------------------------------------

    private void setOptionsMenu(final int menu) {
        if (menu != ui.optionsMenu) {
            ui.optionsMenu = menu;
            supportInvalidateOptionsMenu();
        }
    }

    // --- CREATE NAVIGATION VIEW ------------------------------------------------------------------

    private void createNavigationView() {
        onCreateNavigationView(binding.appBaseDrawer.navView);
    }

    protected void onCreateNavigationView(@NonNull final NavigationView navigationView) {
        this.navigationView = navigationView;
        this.navigationView.setNavigationItemSelectedListener(this::onNavigationItemSelected);
    }

    // --- ON NAVIGATION ITEM SELECTED -------------------------------------------------------------

    protected boolean onNavigationItemSelected(@NonNull final MenuItem item) {
        return !item.isChecked();
    }

    // --- SET ACTIVE DRAWER ITEM ID ---------------------------------------------------------------

    private void setActiveNavigationItemId(final int itemId) {
        if (itemId > 0) { navigationView.getMenu().findItem(itemId).setChecked(true); }
    }

    // --- CREATE FAB ------------------------------------------------------------------------------

    private void createFab() {

        final AppFabBinding fabBinding = DataBindingUtil.bind(
                binding.appBaseDrawer.appDrawerFab.appFab
        );

        if (fabBinding == null) { return; }

        //fabBinding.setLifecycleOwner(this);
        //fabBinding.setViewModel(ViewModelProviders.of(this).get(Ui.class));

        ui.getFabData().observe(this, (@Nullable final FabData fabData)-> {
            if (fabData == null) {
                fabBinding.appFab.hide();
                return;
            }
            if (fabData.textResId == 0) {
                fabBinding.appFab.shrink();
            } else {
                fabBinding.setFabText(getString(fabData.textResId));
                fabBinding.appFab.extend();
            }
            fabBinding.setFabIconResId(fabData.iconResId);
            fabBinding.appFab.show();
        });

        ui.getIsFabEnabled().observe(this, fabBinding::setEnabled);

        fabBinding.setOnClick((view)-> ui.runFabAction());
    }
}
