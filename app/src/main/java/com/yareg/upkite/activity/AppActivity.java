package com.yareg.upkite.activity;

import android.view.MenuItem;

import androidx.annotation.NonNull;

import com.yareg.base.manager.BaseUrlManager;
import com.yareg.upkite.constant.Value;
import com.yareg.upkite.dialog.DebugDialog;
import com.yareg.upkite.fragment.AboutFragment;
import com.yareg.upkite.fragment.FirewallFragment;
import com.yareg.upkite.fragment.MainFragment;
import com.yareg.upkite.fragment.ServersFragment;
import com.yareg.upkite.fragment.SettingsFragment;
import com.yareg.upkite.fragment.SnapshotsFragment;
import com.yareg.upkite.R;
import com.yareg.upkite.fragment.TasksFragment;
import com.yareg.upkite.repository.BillingRepository;

public class AppActivity extends ProfileActivity {

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    /*
    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
        if(getIntent().getAction().equals(Intent.ACTION_MAIN)) {

        }


        if(AppFeatures.ACCOUNT) {
            ui.setStartFragment(App.PROFILE_ID == 0 ? LoginFragment.ITEM : MainFragment.ITEM);
        } else {
            ui.setStartFragment(MainFragment.ITEM);
        }
    }
    */

    @Override
    public boolean onOptionsItemSelected(@NonNull final MenuItem item) {

        switch(item.getItemId()) {

            case Value.Menu.Options.TASKS:
                ui.pushFragment(TasksFragment.ITEM, true);
                return true;

            case Value.Menu.Options.REFRESH:
                getProfileViewModel().syncAccount(true);
                return true;

            case Value.Menu.Options.HELP:
                BaseUrlManager.openInBrowser(this, getString(R.string.url_help));
                return true;

            case Value.Menu.Options.DEBUG:
                DebugDialog.createInstance().show(this);
                return true;

            default: //Log.e(TAG, "unexpected item selected from options menu");
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected boolean onNavigationItemSelected(@NonNull final MenuItem item) {
        if(super.onNavigationItemSelected(item)) {
            switch (item.getItemId()) {

                case Value.Menu.Navigation.MAIN:
                    ui.pushFragment(MainFragment.ITEM);
                    return true;

                case Value.Menu.Navigation.SERVERS:
                    ui.pushFragment(ServersFragment.ITEM);
                    return true;

                case Value.Menu.Navigation.SNAPSHOTS:
                    ui.pushFragment(SnapshotsFragment.ITEM);
                    return true;

                case Value.Menu.Navigation.FIREWALL:
                    ui.pushFragment(FirewallFragment.ITEM);
                    return true;

                case Value.Menu.Navigation.SETTINGS:
                    ui.pushFragment(SettingsFragment.ITEM, true);
                    return true;

                case Value.Menu.Navigation.HELP:
                    BaseUrlManager.openInBrowser(this, getString(R.string.url_help));
                    return true;

                case Value.Menu.Navigation.ABOUT:
                    ui.pushFragment(AboutFragment.ITEM, true);
                    return true;

                case Value.Menu.Navigation.DONATE:
                    BaseUrlManager.openInBrowser(this, getString(R.string.url_donate));
                    return true;

                default:
                    //Log.e(TAG, "unexpected item selected from navigation menu");
                    return false;
            }
        }
        return false;
    }

    @Override
    protected void onProfileIdChanged(final long profileId) {
        super.onProfileIdChanged(profileId);
        BillingRepository.setEntityId(profileId);
    }
}