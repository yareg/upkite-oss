package com.yareg.upkite.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.google.android.material.navigation.NavigationView;
import com.yareg.account.model.SyncState;
import com.yareg.upkite.databinding.NavHeaderBinding;
import com.yareg.upkite.repository.ProfileRepository;
import com.yareg.upkite.viewmodel.ProfileViewModel;

public abstract class ProfileActivity extends NavigationDrawerActivity {

    //----------------------------------------------------------------------------------------------
    //
    // SUPER OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ProfileViewModel viewModel = getProfileViewModel();

        viewModel.getProfileId().observe(
                this,
                (profileId)-> { if(profileId != null) { onProfileIdChanged(profileId.value); } }
        );

        viewModel.getSyncState().observe(
                this,
                (status)-> setIsSyncing(status.primary == SyncState.ACTIVE)
        );
    }

    @Override
    protected void onCreateNavigationView(@NonNull final NavigationView navigationView) {
        super.onCreateNavigationView(navigationView);

        final NavHeaderBinding navHeaderBinding = DataBindingUtil.bind(
                navigationView.getHeaderView(0)
        );

        if(navHeaderBinding == null) { return; }

        navHeaderBinding.setViewModel(getProfileViewModel());
        navHeaderBinding.setLifecycleOwner(this);
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    public ProfileViewModel getProfileViewModel() {
        return getViewModelProvider().get(ProfileViewModel.class);
    }

    //----------------------------------------------------------------------------------------------
    //
    // PROTECTED METHODS
    //
    //----------------------------------------------------------------------------------------------

    protected void onProfileIdChanged(final long profileId) {
        ProfileRepository.setEntityId(profileId);
    }
}