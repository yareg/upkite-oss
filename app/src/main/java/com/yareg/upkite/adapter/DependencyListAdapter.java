package com.yareg.upkite.adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.yareg.base.list.ImmutableListAdapter;
import com.yareg.upkite.R;
import com.yareg.upkite.databinding.ListItemTriplexBinding;
import com.yareg.upkite.model.Dependency;

import java.util.ArrayList;

public class DependencyListAdapter
        extends ImmutableListAdapter<Dependency, DependencyListAdapter.ViewHolder> {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public DependencyListAdapter(final ArrayList<Dependency> items) {
        super(R.layout.list_item_triplex, items);
    }

    //----------------------------------------------------------------------------------------------
    //
    // VIEW HOLDER
    //
    //----------------------------------------------------------------------------------------------

    class ViewHolder extends ImmutableListAdapter.ViewHolder {

        private ViewHolder(@NonNull View view) { super(view); }

        @Override
        protected void bind(final int position) {
            final ListItemTriplexBinding binding = DataBindingUtil.bind(itemView);
            if (binding != null) {
                final Dependency dependency = getItem(position);
                binding.setPrimary(dependency.name);
                binding.setSecondary(dependency.version);
                binding.setTertiary(dependency.license);
                binding.executePendingBindings();
            }
        }
    }

    //----------------------------------------------------------------------------------------------
    //
    // IMPLEMENTED METHODS
    //
    //----------------------------------------------------------------------------------------------

    protected ViewHolder createViewHolder(@NonNull View view) {
        return new ViewHolder(view);
    }
}
