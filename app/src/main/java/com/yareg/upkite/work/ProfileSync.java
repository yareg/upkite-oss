package com.yareg.upkite.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.WorkerParameters;

import com.yareg.base.content.BaseContentProviderClient;
import com.yareg.base.model.PersistentInteger;
import com.yareg.base.model.Time;
import com.yareg.upkite.app.AppFeatures;
import com.yareg.upkite.constant.Key;
import com.yareg.upkite.constant.Value;
import com.yareg.upkite.model.Profile;
import com.yareg.upkite.model.Task;
import com.yareg.upkite.notification.AppNotificationManager;
import com.yareg.upkite.vultr.VultrClient;
import com.yareg.upkite.vultr.model.VultrAuth;

public class ProfileSync extends ContentSync {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public ProfileSync(
            @NonNull final Context context,
            @NonNull final WorkerParameters workerParams) {
        super(context, workerParams);
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @NonNull
    @Override
    public Result doWork() {

        final BaseContentProviderClient resolver = getContentResolver();

        if (resolver == null) {
            return Result.failure();
        }

        if (AppFeatures.NOTIFICATIONS) {
            final PersistentInteger ncSet = resolver.getByPrototype(
                    new PersistentInteger(Key.Integer.NOTIFICATION_CHANNELS_SET)
            );

            if (ncSet == null || ncSet.asInt() != Value.Notification.VERSION) {
                AppNotificationManager.createNotificationChannels(getApplicationContext());
                resolver.addInsertToBatch(
                        new PersistentInteger(
                                Key.Integer.NOTIFICATION_CHANNELS_SET,
                                Value.Notification.VERSION
                        )
                );
            }
        }

        final String token = getInputData().getString(Key.Database.TOKEN);

        final PersistentInteger profileId = resolver.getByPrototype(
                new PersistentInteger(Key.Integer.PROFILE_ID)
        );

        final Profile profile = (profileId == null || profileId.value < 1) ?
                new Profile() :
                resolver.completePrototype(new Profile(profileId.value));

        if (token != null) { profile.token = token; }

        final VultrAuth data = VultrClient.getInstance().getAuth(profile.token);

        if (data == null) {
            return Result.failure();
        }

        if (!data.name.isEmpty() && data.name.contains(Profile.NAME_SPLITTER)) {
            final String[] name = data.name.split(Profile.NAME_SPLITTER);
            profile.firstName = name[0];
            profile.lastName  = name[1];
        } else {
            profile.firstName = data.name;
        }

        profile.email       = data.email;
        profile.permissions = data.permissions;
        profile.lastSync    = Time.getUnixTime();

        if (profileId == null || profileId.value != profile.id) {
            resolver.addInsertToBatch(new PersistentInteger(Key.Integer.PROFILE_ID, profile.id));
        }

        resolver.addInsertToBatch(profile);
        resolver.purgeTrash(Task.contract.uri);
        resolver.applyBatch();
        resolver.close();

        return Result.success();
    }
}
