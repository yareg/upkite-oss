package com.yareg.upkite.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.WorkerParameters;

import com.yareg.base.content.BaseContentProviderClient;
import com.yareg.upkite.constant.Key;
import com.yareg.upkite.model.Profile;
import com.yareg.upkite.model.Server;
import com.yareg.upkite.model.Task;
import com.yareg.upkite.vultr.VultrClient;
import com.yareg.upkite.vultr.model.VultrSnapshot;

public class SnapshotCreate extends ContentSync {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public SnapshotCreate(
            @NonNull final Context context,
            @NonNull final WorkerParameters workerParams) {
        super(context, workerParams);
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @NonNull
    @Override
    public Result doWork() {

        final long taskId   = getInputData().getLong(Key.Work.TASK, 0);
        final long serverId = getInputData().getLong(Key.Work.ID, 0);

        if (taskId == 0 || serverId == 0) {
            return Result.failure();
        }

        final BaseContentProviderClient resolver = getContentResolver();

        if (resolver == null) {
            return Result.failure();
        }

        final Task task = resolver.getByPrototype(new Task(taskId));

        if (task == null || task.status == Key.Status.CANCELED) {
            return Result.success();
        }

        final Profile profile = getProfile();

        if (profile == null) {
            return Result.failure();
        }

        final Server server = resolver.getByPrototype(new Server(serverId));

        if (server == null) {
            return Result.failure();
        }

        final VultrClient client = VultrClient.getInstance();

        final String snapshotId = client.createSnapshot(
                profile.token, Long.toString(serverId), server.label
        );

        if (snapshotId == null) {
            return Result.failure();
        }

        final VultrSnapshot vultrSnapshot = client
                .getSnapshot(profile.token, snapshotId);

        if (vultrSnapshot != null) {
            resolver.syncData(VultrSnapshot.createSnapshot(vultrSnapshot));
        }

        task.status = Key.Status.COMPLETED;
        resolver.addInsertToBatch(task);

        resolver.applyBatch();
        resolver.close();

        return Result.success();
    }
}
