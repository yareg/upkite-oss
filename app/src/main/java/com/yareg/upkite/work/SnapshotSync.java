package com.yareg.upkite.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.WorkerParameters;

import com.yareg.base.content.BaseContentProviderClient;
import com.yareg.upkite.constant.Key;
import com.yareg.upkite.model.Profile;
import com.yareg.upkite.vultr.VultrClient;
import com.yareg.upkite.vultr.model.VultrSnapshot;

public class SnapshotSync extends ContentSync {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public SnapshotSync(
            @NonNull final Context context,
            @NonNull final WorkerParameters workerParams) {
        super(context, workerParams);
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @NonNull
    @Override
    public Result doWork() {

        final String snapshotId = getInputData().getString(Key.Work.ID);

        if (snapshotId == null) {
            return Result.failure();
        }

        final BaseContentProviderClient resolver = getContentResolver();

        if (resolver == null) {
            return Result.failure();
        }

        final Profile profile = getProfile();

        if (profile == null) {
            return Result.failure();
        }

        final VultrSnapshot vultrSnapshot = VultrClient
                .getInstance()
                .getSnapshot(profile.token, snapshotId);

        if (vultrSnapshot == null) {
            return Result.failure();
        }

        resolver.syncData(VultrSnapshot.createSnapshot(vultrSnapshot));
        resolver.applyBatch();
        resolver.close();

        return Result.success();
    }
}
