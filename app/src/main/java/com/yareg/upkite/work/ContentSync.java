package com.yareg.upkite.work;

import android.content.ContentProviderClient;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.yareg.base.content.BaseContentProviderClient;
import com.yareg.base.manager.BaseNotificationManager;
import com.yareg.base.model.BaseNotification;
import com.yareg.base.model.PersistentInteger;
import com.yareg.upkite.constant.Key;
import com.yareg.upkite.constant.Value;
import com.yareg.upkite.model.Profile;

public abstract class ContentSync extends Worker {

    private final BaseContentProviderClient contentResolver;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public ContentSync(
            @NonNull final Context context,
            @NonNull final WorkerParameters workerParams) {
        super(context, workerParams);

        final ContentProviderClient client = context
                .getContentResolver()
                .acquireContentProviderClient(Value.ContentProvider.AUTHORITY);

        contentResolver = (client == null) ? null : BaseContentProviderClient.getInstance(client);
    }

    //----------------------------------------------------------------------------------------------
    //
    // PROTECTED METHODS
    //
    //----------------------------------------------------------------------------------------------

    @Nullable
    protected BaseContentProviderClient getContentResolver() {
        return contentResolver;
    }

    // --- GET PROFILE -----------------------------------------------------------------------------

    @Nullable
    protected Profile getProfile() {

        if (contentResolver == null) {
            return null;
        }

        final PersistentInteger profileId = contentResolver.getByPrototype(
                new PersistentInteger(Key.Integer.PROFILE_ID)
        );

        if (profileId == null) {
            return null;
        }

        return contentResolver.getByPrototype(new Profile(profileId.value));
    }

    // --- SHOW NOTIFICATION -----------------------------------------------------------------------

    protected void showNotification(@NonNull final BaseNotification notification) {
        BaseNotificationManager.showNotification(getApplicationContext(), notification);
    }
}
