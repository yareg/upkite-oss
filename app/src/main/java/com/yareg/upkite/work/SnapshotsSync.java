package com.yareg.upkite.work;

import android.content.Context;
import android.util.LongSparseArray;

import androidx.annotation.NonNull;
import androidx.work.WorkerParameters;

import com.yareg.base.content.BaseContentProviderClient;
import com.yareg.base.model.BaseModel;
import com.yareg.upkite.model.Profile;
import com.yareg.upkite.model.Snapshot;
import com.yareg.upkite.notification.SnapshotCompleteNotification;
import com.yareg.upkite.vultr.VultrClient;
import com.yareg.upkite.vultr.model.VultrSnapshot;

import java.util.ArrayList;

public class SnapshotsSync extends ContentSync {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public SnapshotsSync(
            @NonNull final Context context,
            @NonNull final WorkerParameters workerParams) {
        super(context, workerParams);
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @NonNull
    @Override
    public Result doWork() {

        final BaseContentProviderClient resolver = getContentResolver();

        if (resolver == null) {
            return Result.failure();
        }

        final Profile profile = getProfile();

        if (profile == null) {
            return Result.failure();
        }

        final ArrayList<VultrSnapshot> data = VultrClient
                .getInstance()
                .getSnapshotList(profile.token);

        if (data == null) {
            return Result.failure();
        }

        final LongSparseArray<Snapshot> snapshots = resolver.list(
                new Snapshot(),
                null,
                null
        );

        final ArrayList<Long> ids = new ArrayList<>(data.size());

        for (final VultrSnapshot vultrSnapshot : data) {
            final Snapshot newSnapshot = VultrSnapshot.createSnapshot(vultrSnapshot);
            ids.add(newSnapshot.id);
            final Snapshot oldSnapshot = snapshots.get(newSnapshot.id);

            if (oldSnapshot != null) {
                if (!BaseModel.isChanged(oldSnapshot, newSnapshot)) {
                    continue;
                }
                if (!oldSnapshot.isActive && newSnapshot.isActive) {
                    showNotification(new SnapshotCompleteNotification(
                            getApplicationContext().getResources(),
                            newSnapshot.label
                    ));
                }
            }

            resolver.addInsertToBatch(newSnapshot);
        }

        resolver.removeObsoleteData(Snapshot.contract.uri, ids);
        resolver.applyBatch();
        resolver.close();

        return Result.success();
    }
}
