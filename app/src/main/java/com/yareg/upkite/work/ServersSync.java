package com.yareg.upkite.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.WorkerParameters;

import com.yareg.base.content.BaseContentProviderClient;
import com.yareg.base.model.PersistentInteger;
import com.yareg.base.model.PersistentPrecise;
import com.yareg.upkite.constant.Key;
import com.yareg.upkite.model.Profile;
import com.yareg.upkite.model.Server;
import com.yareg.upkite.vultr.VultrClient;
import com.yareg.upkite.vultr.model.VultrServer;

import java.util.ArrayList;

public class ServersSync extends ContentSync {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public ServersSync(
            @NonNull final Context context,
            @NonNull final WorkerParameters workerParams) {
        super(context, workerParams);
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @NonNull
    @Override
    public Result doWork() {

        final BaseContentProviderClient resolver = getContentResolver();

        if (resolver == null) {
            return Result.failure();
        }

        final Profile profile = getProfile();

        if (profile == null) {
            return Result.failure();
        }

        final ArrayList<VultrServer> data = VultrClient.getInstance().getServerList(profile.token);

        if (data == null) {
            return Result.failure();
        }

        int    activeServers        = 0;
        int    runningServers       = 0;
        int    serverIssues         = 0;
        long   worstBandwidthServer = 0;
        double worstBandwidthGb     = 0;
        float  totalMonthlyCost     = 0;

        final ArrayList<Long>   newIds    = new ArrayList<>(data.size());
        final ArrayList<String> locations = new ArrayList<>(data.size());

        for (final VultrServer vultrServer : data) {

            final Server server = new Server(Long.parseLong(vultrServer.id));

            newIds.add(server.id);

            server.label    = vultrServer.label;
            server.status   = vultrServer.getStatus();
            server.state    = vultrServer.getState();
            server.isActive = vultrServer.status.equals(VultrServer.ACTIVE);
            if (server.isActive) { activeServers++; } else { serverIssues++; }

            server.isRunning = vultrServer.powerStatus.equals(VultrServer.RUNNING);
            if (server.isRunning) { runningServers++; } else { serverIssues++; }

            server.isOk = vultrServer.state.equals(VultrServer.OK);
            if (!server.isOk) { serverIssues++; }

            server.osId           = Integer.parseInt(vultrServer.osId);
            server.os             = vultrServer.os;
            server.pendingCharges = Float.parseFloat(vultrServer.pendingCharges);

            server.monthlyCost = Float.parseFloat(vultrServer.costPerMonth);
            totalMonthlyCost  += server.monthlyCost;

            server.currentBandwidth = vultrServer.currentBandwidth;
            server.allowedBandwidth = Double.parseDouble(vultrServer.allowedBandwidth);
            server.bandwidthPercent = (float) (server.currentBandwidth / server.allowedBandwidth * 100);

            if (worstBandwidthGb < server.currentBandwidth) {
                worstBandwidthServer = server.id;
                worstBandwidthGb = server.currentBandwidth;
            }

            server.location = vultrServer.location;
            if (!locations.contains(server.location)) { locations.add(server.location); }

            server.mainIp     = vultrServer.mainIp;
            server.internalIp = vultrServer.internalIp;
            server.tag        = vultrServer.tag;
            server.firewall   = vultrServer.firewall;
            server.plan       = vultrServer.plan;

            resolver.syncData(server);
        }

        resolver.syncData(new PersistentInteger(Key.Integer.SERVERS_TOTAL, data.size()));
        resolver.syncData(new PersistentInteger(Key.Integer.ACTIVE_SERVERS, activeServers));
        resolver.syncData(new PersistentInteger(Key.Integer.RUNNING_SERVERS, runningServers));
        resolver.syncData(new PersistentInteger(Key.Integer.SERVER_ISSUES, serverIssues));
        resolver.syncData(new PersistentInteger(Key.Integer.WORST_BANDWIDTH_SERVER_ID, worstBandwidthServer));
        resolver.syncData(new PersistentInteger(Key.Integer.LOCATIONS, locations.size()));

        final float totalDailyCost = totalMonthlyCost / 30.00f;

        resolver.syncData(new PersistentPrecise(Key.Precise.TOTAL_MONTHLY_COST, totalMonthlyCost));
        resolver.syncData(new PersistentPrecise(Key.Precise.TOTAL_DAILY_COST, totalDailyCost));
        resolver.syncData(new PersistentPrecise(Key.Precise.TOTAL_HOURLY_COST, totalDailyCost / 24.00f));

        resolver.removeObsoleteData(Server.contract.uri, newIds);

        resolver.applyBatch();
        resolver.close();

        return Result.success();
    }
}
