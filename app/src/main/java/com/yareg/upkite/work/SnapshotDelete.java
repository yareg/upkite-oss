package com.yareg.upkite.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.WorkerParameters;

import com.yareg.base.content.BaseContentProviderClient;
import com.yareg.upkite.constant.Key;
import com.yareg.upkite.model.Profile;
import com.yareg.upkite.model.Snapshot;
import com.yareg.upkite.model.Task;
import com.yareg.upkite.vultr.VultrClient;

public class SnapshotDelete extends ContentSync {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public SnapshotDelete(
            @NonNull final Context context,
            @NonNull final WorkerParameters workerParams) {
        super(context, workerParams);
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @NonNull
    @Override
    public Result doWork() {

        final long taskId     = getInputData().getLong(Key.Work.TASK, 0);
        final long snapshotId = getInputData().getLong(Key.Work.ID, 0);

        if (taskId == 0 || snapshotId >= 0) { // marked as trash only
            return Result.failure();
        }

        final BaseContentProviderClient resolver = getContentResolver();

        if (resolver == null) {
            return Result.failure();
        }

        final Task task = resolver.getByPrototype(new Task(taskId));

        if (task == null || task.status == Key.Status.CANCELED) {
            return Result.success();
        }

        final Profile profile = getProfile();

        if (profile == null) {
            return Result.failure();
        }

        final Snapshot snapshot = resolver.getByPrototype(new Snapshot(snapshotId));

        if (snapshot == null) {
            return Result.failure();
        }

        final boolean isDeleted = VultrClient
                .getInstance()
                .destroySnapshot(profile.token, snapshot.label);

        if (!isDeleted) {
            return Result.failure();
        }

        task.status = Key.Status.COMPLETED;
        resolver.addInsertToBatch(task);

        resolver.addDeleteToBatch(Snapshot.contract.uri, snapshotId);
        resolver.applyBatch();
        resolver.close();

        return Result.success();
    }
}
