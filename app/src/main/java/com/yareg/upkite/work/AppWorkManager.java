package com.yareg.upkite.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.yareg.upkite.constant.Key;

public class AppWorkManager {

    private final WorkManager manager;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    private AppWorkManager(@NonNull final Context context) {
        manager = WorkManager.getInstance(context);
    }

    //----------------------------------------------------------------------------------------------
    //
    // INSTANCE
    //
    //----------------------------------------------------------------------------------------------

    public static AppWorkManager getInstance(@NonNull final Context context) {
        return new AppWorkManager(context);
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- GET MANAGER -----------------------------------------------------------------------------

    public WorkManager getManager() {
        return manager;
    }

    // --- IS SYNC DONE ----------------------------------------------------------------------------

    /*public boolean isSyncDone() {
        return manager.getWorkInfosForUniqueWork(Key.Work.SYNC).isDone();
    }*/

    // --- GET EXISTING WORK POLICY ----------------------------------------------------------------

    public ExistingWorkPolicy getExistingWorkPolicy() {
        //return isSyncDone() ? ExistingWorkPolicy.REPLACE : ExistingWorkPolicy.APPEND;
        return ExistingWorkPolicy.APPEND_OR_REPLACE;
    }

    // --- CREATE SNAPSHOT -------------------------------------------------------------------------

    public void createSnapshot(final long taskId, final long serverId) {

        final Data data = new Data
                .Builder()
                .putLong(Key.Work.ID, serverId)
                .putLong(Key.Work.TASK, taskId)
                .build();

        final Constraints constraints = new Constraints
                .Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        final OneTimeWorkRequest snapshotCreate = new OneTimeWorkRequest
                .Builder(SnapshotCreate.class)
                .addTag("create_snapshot_" + taskId)
                .setConstraints(constraints)
                .setInputData(data)
                .build();

        manager
                .beginUniqueWork(Key.Work.SYNC, getExistingWorkPolicy(), snapshotCreate)
                .enqueue();
    }

    // --- DESTROY SNAPSHOT ------------------------------------------------------------------------

    public void destroySnapshot(final long taskId, final long snapshotId) {

        final Data data = new Data
                .Builder()
                .putLong(Key.Work.ID, snapshotId)
                .putLong(Key.Work.TASK, taskId)
                .build();

        final Constraints constraints = new Constraints
                .Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        final OneTimeWorkRequest snapshotDelete = new OneTimeWorkRequest
                .Builder(SnapshotDelete.class)
                .addTag("delete_snapshot_" + taskId)
                .setConstraints(constraints)
                .setInputData(data)
                .build();

        manager
                .beginUniqueWork(Key.Work.SYNC, getExistingWorkPolicy(), snapshotDelete)
                .enqueue();
    }

}
