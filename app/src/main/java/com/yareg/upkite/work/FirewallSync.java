package com.yareg.upkite.work;

import android.content.Context;
import android.util.LongSparseArray;

import androidx.annotation.NonNull;
import androidx.work.WorkerParameters;

import com.yareg.base.content.BaseContentProviderClient;
import com.yareg.base.model.Time;
import com.yareg.upkite.model.FirewallGroup;
import com.yareg.upkite.model.Profile;
import com.yareg.upkite.vultr.VultrClient;
import com.yareg.upkite.vultr.model.VultrFirewallGroup;

import java.util.ArrayList;
import java.util.Arrays;

public class FirewallSync extends ContentSync {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public FirewallSync(
            @NonNull final Context context,
            @NonNull final WorkerParameters workerParams) {
        super(context, workerParams);
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @NonNull
    @Override
    public Result doWork() {

        final BaseContentProviderClient resolver = getContentResolver();

        if (resolver == null) {
            return Result.failure();
        }

        final Profile profile = getProfile();

        if (profile == null) {
            return Result.failure();
        }

        if (!Arrays.asList(profile.getPermissions()).contains("firewall")) {
            return Result.failure();
        }

        final ArrayList<VultrFirewallGroup> newData = VultrClient
                .getInstance()
                .getFirewallGroupList(profile.token);

        if (newData == null) {
            return Result.failure();
        }

        final LongSparseArray<FirewallGroup> oldData = resolver.list(
                new FirewallGroup(), null, null
        );

        final ArrayList<Long> newIds = new ArrayList<>(newData.size());

        for (final VultrFirewallGroup vultrFirewallGroup : newData) {

            FirewallGroup firewallGroup = null;

            if (oldData.size() > 0) {
                for (int i = 0, size = oldData.size(); i < size; i++) {
                    if (oldData.valueAt(i).label.equals(vultrFirewallGroup.label)) {
                        firewallGroup = oldData.valueAt(i);
                        break;
                    }
                }
            }

            if (firewallGroup == null) {
                firewallGroup = new FirewallGroup();
            }

            newIds.add(firewallGroup.id);

            final long dateUpdated = Time.getUnixTime(
                    vultrFirewallGroup.dateUpdated, Time.getUtc()
            );

            if (dateUpdated != firewallGroup.dateUpdated) {
                firewallGroup.label         = vultrFirewallGroup.label;
                firewallGroup.description   = vultrFirewallGroup.description;
                firewallGroup.dateUpdated   = dateUpdated;
                firewallGroup.instanceCount = vultrFirewallGroup.instanceCount;
                firewallGroup.ruleCount     = vultrFirewallGroup.ruleCount;
                firewallGroup.maxRuleCount  = vultrFirewallGroup.maxRuleCount;
                firewallGroup.dateCreated   = Time.getUnixTime(
                        vultrFirewallGroup.dateCreated, Time.getUtc()
                );
                resolver.addInsertToBatch(firewallGroup);
            }
        }

        resolver.removeObsoleteData(FirewallGroup.contract.uri, newIds);
        resolver.applyBatch();
        resolver.close();

        return Result.success();
    }
}
