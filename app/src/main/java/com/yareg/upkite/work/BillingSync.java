package com.yareg.upkite.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.WorkerParameters;

import com.yareg.base.content.BaseContentProviderClient;
import com.yareg.base.model.PersistentInteger;
import com.yareg.base.model.PersistentPrecise;
import com.yareg.base.model.Time;
import com.yareg.upkite.constant.Key;
import com.yareg.upkite.constant.Value;
import com.yareg.upkite.model.Billing;
import com.yareg.upkite.model.Payment;
import com.yareg.upkite.model.Profile;
import com.yareg.upkite.notification.LowBalanceNotification;
import com.yareg.upkite.notification.TopUpNotification;
import com.yareg.upkite.vultr.VultrClient;
import com.yareg.upkite.vultr.model.VultrAccount;

import java.util.TimeZone;

public class BillingSync extends ContentSync {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public BillingSync(
            @NonNull final Context context,
            @NonNull final WorkerParameters workerParams) {
        super(context, workerParams);
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @NonNull
    @Override
    public Result doWork() {

        final BaseContentProviderClient resolver = getContentResolver();

        if (resolver == null) {
            return Result.failure();
        }

        final Profile profile = getProfile();

        if (profile == null) {
            return Result.failure();
        }

        final VultrAccount data = VultrClient.getInstance().getAccount(profile.token);

        if (data == null) {
            return Result.failure();
        }

        final Billing billing = new Billing(profile.id);
        final Billing parent  = resolver.getByPrototype(billing);

        billing.balance           = Math.abs(Float.parseFloat(data.balance));
        billing.pendingCharges    = Math.abs(Float.parseFloat(data.pendingCharges));
        billing.remainingCredit   = billing.balance - billing.pendingCharges;
        billing.lastPaymentAmount = Math.abs(Float.parseFloat(data.lastPaymentAmount));
        billing.totalMonthlyCost  = resolver.completePrototype(
                new PersistentPrecise(Key.Precise.TOTAL_MONTHLY_COST)
        ).asFloat();
        billing.lastPaymentDate   = Time.getUnixTime(
                data.lastPaymentDate, TimeZone.getTimeZone(Time.ZONE_GMT)
        );

        resolver.syncData(billing);

        float totalHourlyCost = resolver.completePrototype(
                new PersistentPrecise(Key.Precise.TOTAL_HOURLY_COST)
        ).asFloat();

        final PersistentPrecise vatMultiplier = resolver.getByPrototype(
                new PersistentPrecise(Key.Precise.VAT_MULTIPLIER)
        );

        if (vatMultiplier != null && vatMultiplier.asFloat() > 1.0f) {
            totalHourlyCost = totalHourlyCost * vatMultiplier.asFloat();
        }

        resolver.syncData(new PersistentInteger(
                Key.Integer.MILLIS_PAID,
                Math.round(billing.remainingCredit / totalHourlyCost) * Time.HOUR
        ));

        if (parent == null || billing.lastPaymentDate > parent.lastPaymentDate) {
            final Payment payment = new Payment(billing.lastPaymentDate);
            payment.amount  = billing.lastPaymentAmount;
            resolver.addInsertToBatch(payment);

            if (parent != null && resolver.completePrototype(
                    new PersistentInteger(
                            Key.Integer.NOTIFY_ON_TOP_UP, Value.Notification.ON_TOP_UP
                    )).asBoolean()) {
                showNotification(new TopUpNotification(
                        getApplicationContext().getResources(), billing.lastPaymentAmount
                ));
            }
        }

        if (billing.totalMonthlyCost > billing.remainingCredit && resolver.completePrototype(
                new PersistentInteger(
                        Key.Integer.NOTIFY_ON_LOW_BALANCE, Value.Notification.ON_LOW_BALANCE
                )).asBoolean()) {
            showNotification(new LowBalanceNotification(
                    getApplicationContext().getResources(), billing.remainingCredit
            ));
        }

        resolver.purgeTrash(Payment.contract.uri);
        resolver.applyBatch();
        resolver.close();

        return Result.success();
    }
}
