package com.yareg.upkite.constant;

import android.os.Build;

import androidx.appcompat.app.AppCompatDelegate;

public final class Key {

    //----------------------------------------------------------------------------------------------
    //
    // LONG
    //
    //----------------------------------------------------------------------------------------------

    public static final class Integer {
        public static final int THEME_ID                  = 1;
        public static final int PROFILE_ID                = 2;
        public static final int SERVERS_TOTAL             = 3;
        public static final int ACTIVE_SERVERS            = 4;
        public static final int RUNNING_SERVERS           = 5;
        public static final int SERVER_ISSUES             = 6;
        //public static final int LAST_SNAPSHOT           = 7;
        public static final int WORST_BANDWIDTH_SERVER_ID = 8;
        public static final int LOCATIONS                 = 9;
        //public static final int SNAPSHOTS               = 10;
        public static final int NOTIFY_ON_LOW_BALANCE     = 11;
        public static final int NOTIFY_ON_TOP_UP          = 12;
        public static final int MILLIS_PAID               = 13;
        public static final int NOTIFICATION_CHANNELS_SET = 14;
    }

    //----------------------------------------------------------------------------------------------
    //
    // DOUBLE
    //
    //----------------------------------------------------------------------------------------------

    public static final class Precise {
        public static final int TOTAL_MONTHLY_COST = 1;
        public static final int TOTAL_DAILY_COST   = 2;
        public static final int TOTAL_HOURLY_COST  = 3;
        public static final int WORST_BANDWIDTH_GB = 4;
        public static final int VAT_MULTIPLIER     = 5;
    }

    //----------------------------------------------------------------------------------------------
    //
    // THEME
    //
    //----------------------------------------------------------------------------------------------

    public static final class Theme {
        public static final int Auto  =
                (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) ?
                AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM :
                AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY;

        public static final int Light = AppCompatDelegate.MODE_NIGHT_NO;
        public static final int Dark  = AppCompatDelegate.MODE_NIGHT_YES;
    }

    //----------------------------------------------------------------------------------------------
    //
    // PREFERENCE
    //
    //----------------------------------------------------------------------------------------------

    public static final class Preference {

        public static final class Appearance {
            public static final String ThemeId = "appearance_theme_id";
        }

        public static final class Sync {
            public static final String Enabled   = "sync_enabled";
            public static final String Frequency = "sync_frequency";
        }

        public static final class Notification {
            public static final String TopUp      = "notification_top_up";
            public static final String LowBalance = "notification_low_balance";
        }

        public static final class Billing {
            public static final String AdditionalVatEnabled = "additional_vat_enabled";
            public static final String AdditionalVatPercent = "additional_vat_percent";
        }
    }

    //----------------------------------------------------------------------------------------------
    //
    // DATABASE
    //
    //----------------------------------------------------------------------------------------------

    public static final class Database {
        //public static final String ENTITY_ID         = "entity_id";
        public static final String TOKEN               = "token";
        public static final String LAST_SYNC           = "last_sync";
        public static final String FIRST_NAME          = "first_name";
        public static final String LAST_NAME           = "last_name";
        public static final String EMAIL               = "email";
        public static final String PERMISSIONS         = "permissions";
        public static final String BALANCE             = "balance";
        public static final String PENDING_CHARGES     = "pending_charges";
        public static final String REMAINING_CREDIT    = "remaining_credit";
        public static final String LAST_PAYMENT_AMOUNT = "last_payment_amount";
        public static final String LAST_PAYMENT_DATE   = "last_payment_date";
        public static final String AMOUNT              = "amount";
        public static final String LABEL               = "label";
        public static final String DESCRIPTION         = "description";
        public static final String SIZE                = "size";
        public static final String IS_ACTIVE           = "is_active";
        public static final String IS_RUNNING          = "is_running";
        public static final String IS_OK               = "is_ok";
        public static final String OS_ID               = "os_id";
        public static final String APP_ID              = "app_id";
        public static final String OS                  = "os";
        public static final String MONTHLY_COST        = "monthly_cost";
        public static final String CURRENT_BANDWIDTH   = "current_bandwidth";
        public static final String ALLOWED_BANDWIDTH   = "allowed_bandwidth";
        public static final String BANDWIDTH_PERCENT   = "bandwidth_percent";
        public static final String LOCATION            = "location";
        public static final String MAIN_IP             = "main_ip";
        public static final String INTERNAL_IP         = "internal_ip";
        public static final String TYPE_ID             = "type_id";
        public static final String OBJECT_ID           = "object_id";
        public static final String STATUS              = "status";
        public static final String STATE               = "state";
        public static final String FIREWALL            = "firewall";
        public static final String TAG                 = "tag";
        public static final String PLAN                = "plan";
        public static final String CPU                 = "cpu";
        public static final String RAM                 = "ram";
        public static final String DISK                = "disk";
        public static final String NUMBER              = "number";
        public static final String ACTION              = "action";
        public static final String PROTOCOL            = "protocol";
        public static final String PORT                = "port";
        public static final String SUBNET              = "subnet";
        public static final String SUBNET_SIZE         = "subnet_size";
        public static final String SOURCE              = "source";
        public static final String CREATED             = "created";
        public static final String UPDATED             = "updated";
        public static final String INSTANCE_COUNT      = "instance_count";
        public static final String RULE_COUNT          = "rule_count";
        public static final String MAX_RULE_COUNT      = "max_rule_count";
    }

    //----------------------------------------------------------------------------------------------
    //
    // WORK
    //
    //----------------------------------------------------------------------------------------------

    public static final class Work {
        public static final String SYNC = "sync";
        public static final String ID   = "id";
        public static final String TASK = "task";
    }

    //----------------------------------------------------------------------------------------------
    //
    // SERVER
    //
    //----------------------------------------------------------------------------------------------

    public static final class Server {
        public static final class Status {
            public static final int UNKNOWN   = 0;
            public static final int ACTIVE    = 1;
            public static final int PENDING   = 2;
            public static final int SUSPENDED = 3;
            public static final int CLOSED    = 4;
        }
        public static final class State {
            public static final int UNKNOWN      = 0;
            public static final int OK           = 1;
            public static final int LOCKED       = 2;
            public static final int BOOTING      = 3;
            public static final int ISO_MOUNTING = 4;
        }
    }

    //----------------------------------------------------------------------------------------------
    //
    // TASK
    //
    //----------------------------------------------------------------------------------------------

    public static final class Task {
        public static final short CREATE_SNAPSHOT  = 1;
        public static final short DESTROY_SNAPSHOT = 2;
    }

    //----------------------------------------------------------------------------------------------
    //
    // STATUS
    //
    //----------------------------------------------------------------------------------------------

    public static final class Status {
        public static final short UNKNOWN   = 0;
        public static final short PENDING   = 1;
        public static final short COMPLETED = 2;
        public static final short SUSPENDED = 3;
        public static final short FAILED    = 4;
        public static final short CANCELED  = 5;
    }
}
