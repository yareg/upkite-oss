package com.yareg.upkite.constant;

import android.content.ContentResolver;

import com.yareg.base.model.DatabaseTable;
import com.yareg.base.model.PersistentInteger;
import com.yareg.base.model.PersistentPrecise;
import com.yareg.base.model.Time;
import com.yareg.upkite.BuildConfig;
import com.yareg.upkite.R;
import com.yareg.upkite.model.Billing;
import com.yareg.upkite.model.FirewallGroup;
import com.yareg.upkite.model.FirewallRule;
import com.yareg.upkite.model.Payment;
import com.yareg.upkite.model.Profile;
import com.yareg.upkite.model.Server;
import com.yareg.upkite.model.Snapshot;
import com.yareg.upkite.model.Task;

public final class Value {

    //----------------------------------------------------------------------------------------------
    //
    // MENU
    //
    //----------------------------------------------------------------------------------------------

    public static final class Menu {
        public static final class Options {
            public static final int TASKS   = R.id.menu_options_tasks;
            public static final int REFRESH = R.id.menu_options_refresh;
            public static final int HELP    = R.id.menu_options_help;
            public static final int DEBUG   = R.id.menu_options_debug;
        }
        public static final class Navigation {
            public static final int MAIN      = R.id.menu_navigation_main;
            public static final int SERVERS   = R.id.menu_navigation_servers;
            public static final int SNAPSHOTS = R.id.menu_navigation_snapshots;
            public static final int FIREWALL  = R.id.menu_navigation_firewall;
            public static final int SETTINGS  = R.id.menu_navigation_settings;
            public static final int HELP      = R.id.menu_navigation_help;
            public static final int ABOUT     = R.id.menu_navigation_about;
            public static final int DONATE    = R.id.menu_navigation_donate;
        }
    }

    //----------------------------------------------------------------------------------------------
    //
    // ACCOUNT
    //
    //----------------------------------------------------------------------------------------------

    public static final class Account {
        public static final String TYPE            = BuildConfig.APPLICATION_ID + ".account";
        public static final long   SYNC_FREQUENCY  = Time.HOUR;
        public static final long   SYNC_RESOLUTION = Time.MINUTE;
    }

    //----------------------------------------------------------------------------------------------
    //
    // CONTENT PROVIDER
    //
    //----------------------------------------------------------------------------------------------

    public static final class ContentProvider {
        public static final String AUTHORITY = BuildConfig.APPLICATION_ID + ".provider";
        public static final String DIRECTORY = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd." + AUTHORITY + ".";
        public static final String ITEM      = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd." + AUTHORITY + ".";
    }

    //----------------------------------------------------------------------------------------------
    //
    // DATABASE
    //
    //----------------------------------------------------------------------------------------------

    public static final class Database {
        public static final String          NAME    = "content.db";
        public static final int             VERSION = 8;
        public static final DatabaseTable[] TABLES  = {

                // Key-value storage
                PersistentInteger.contract.table,
                PersistentPrecise.contract.table,

                // Model storage
                Profile.contract.table,
                Billing.contract.table,
                Payment.contract.table,
                Server.contract.table,
                Snapshot.contract.table,
                Task.contract.table,
                FirewallGroup.contract.table,
                FirewallRule.contract.table
        };
    }

    //----------------------------------------------------------------------------------------------
    //
    // NOTIFICATION
    //
    //----------------------------------------------------------------------------------------------

    public static final class Notification {
        public static final int VERSION            = 1;
        public static final boolean ON_LOW_BALANCE = true;
        public static final boolean ON_TOP_UP      = true;
    }
}
