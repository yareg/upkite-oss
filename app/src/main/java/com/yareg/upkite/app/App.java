package com.yareg.upkite.app;

import android.app.Application;

import androidx.appcompat.app.AppCompatDelegate;

import com.yareg.account.manager.BaseAccountManager;
import com.yareg.base.content.BaseContentProvider;
import com.yareg.base.content.BaseContentResolver;
import com.yareg.base.manager.BaseDatabaseManager;
import com.yareg.base.repository.IntegerRepository;

import com.yareg.base.ui.Ui;
import com.yareg.upkite.constant.Key;
import com.yareg.upkite.constant.Value;
import com.yareg.upkite.fragment.LoginFragment;
import com.yareg.upkite.fragment.MainFragment;

public class App extends Application {

    public static int THEME_ID = 0;

    //----------------------------------------------------------------------------------------------
    //
    // STATIC CONFIGURATION
    //
    //----------------------------------------------------------------------------------------------

    static {
        BaseContentProvider.AUTHORITY = Value.ContentProvider.AUTHORITY;
        BaseContentProvider.TYPE_DIR  = Value.ContentProvider.DIRECTORY;
        BaseContentProvider.TYPE_ITEM = Value.ContentProvider.ITEM;

        BaseDatabaseManager.DATABASE_NAME    = Value.Database.NAME;
        BaseDatabaseManager.DATABASE_VERSION = Value.Database.VERSION;
        BaseDatabaseManager.DATABASE_TABLES  = Value.Database.TABLES;

        BaseAccountManager.AUTHORITY    = Value.ContentProvider.AUTHORITY;
        BaseAccountManager.ACCOUNT_TYPE = Value.Account.TYPE;
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public void onCreate() {
        super.onCreate();

        BaseContentResolver.setContentResolver(getContentResolver());

        if (AppFeatures.ACCOUNT) {
            final long profileId = IntegerRepository
                    .getInstance()
                    .getItem(Key.Integer.PROFILE_ID)
                    .value;

            Ui.START_FRAGMENT = (profileId == 0) ? LoginFragment.ITEM : MainFragment.ITEM;
        } else {
            Ui.START_FRAGMENT = MainFragment.ITEM;
        }

        if (AppFeatures.THEME) {
            THEME_ID = IntegerRepository
                    .getInstance()
                    .getItem(Key.Integer.THEME_ID)
                    .asInt();
            if (AppCompatDelegate.getDefaultNightMode() != THEME_ID) {
                AppCompatDelegate.setDefaultNightMode(THEME_ID);
            }
        }
    }
}
