package com.yareg.upkite.app;

public final class AppFeatures {
    static final boolean THEME = true;
    public static final boolean ACCOUNT = true;
    public static final boolean NOTIFICATIONS = true;
}
