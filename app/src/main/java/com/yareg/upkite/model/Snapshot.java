package com.yareg.upkite.model;

import android.content.ContentValues;
import android.database.Cursor;

import androidx.annotation.NonNull;

import com.yareg.base.content.BaseContentProvider;
import com.yareg.base.model.BaseModel;
import com.yareg.base.model.DatabaseTable;
import com.yareg.upkite.constant.Key;

public class Snapshot extends BaseModel<Snapshot> {

    //----------------------------------------------------------------------------------------------
    //
    // DATABASE CONTRACT
    //
    //----------------------------------------------------------------------------------------------

    public static class Contract extends BaseContentProvider.BaseContract {
        Contract() { super("snapshot"); }

        @Override
        protected DatabaseTable getTable(@NonNull final String name) {
            final DatabaseTable table = super.getTable(name);
            table.put(Key.Database.LABEL,       TYPE_TEXT);
            table.put(Key.Database.DESCRIPTION, TYPE_TEXT);
            table.put(Key.Database.SIZE,        TYPE_REAL);
            table.put(Key.Database.IS_ACTIVE,   TYPE_INTEGER);
            table.put(Key.Database.OS_ID,       TYPE_INTEGER);
            table.put(Key.Database.APP_ID,      TYPE_INTEGER);
            return table;
        }
    }

    public static final Contract contract = new Contract();

    //----------------------------------------------------------------------------------------------
    //
    // PROPERTIES
    //
    //----------------------------------------------------------------------------------------------

    public String  label;
    public String  description;
    public float   size;
    public boolean isActive;
    public int     osId;
    public int     appId;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTORS
    //
    //----------------------------------------------------------------------------------------------

    public Snapshot() { super(contract.uri); }

    public Snapshot(final long id) { super(contract.uri, id); }

    private Snapshot(@NonNull final Cursor cursor) {
        super(contract.uri, cursor);
        label = cursor.getString(cursor.getColumnIndex(Key.Database.LABEL));
        description = cursor.getString(cursor.getColumnIndex(Key.Database.DESCRIPTION));
        size = cursor.getFloat(cursor.getColumnIndex(Key.Database.SIZE));
        isActive = (cursor.getInt(cursor.getColumnIndex(Key.Database.IS_ACTIVE)) == 1);
        osId = cursor.getInt(cursor.getColumnIndex(Key.Database.OS_ID));
        appId = cursor.getInt(cursor.getColumnIndex(Key.Database.APP_ID));
    }

    //----------------------------------------------------------------------------------------------
    //
    // FACTORY IMPLEMENTATION
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public Snapshot create(final long id) { return new Snapshot(id); }

    @Override
    public Snapshot create(@NonNull final Cursor cursor) { return new Snapshot(cursor); }

    //----------------------------------------------------------------------------------------------
    //
    // CONTENT VALUES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public ContentValues getContentValues() {
        final ContentValues values = super.getContentValues();
        values.put(Key.Database.LABEL, label);
        values.put(Key.Database.DESCRIPTION, description);
        values.put(Key.Database.SIZE, size);
        values.put(Key.Database.IS_ACTIVE, (isActive ? 1 : 0));
        values.put(Key.Database.OS_ID, osId);
        values.put(Key.Database.APP_ID, appId);
        return values;
    }
}
