package com.yareg.upkite.model;

import android.content.ContentValues;
import android.database.Cursor;

import androidx.annotation.NonNull;

import com.yareg.base.content.BaseContentProvider;
import com.yareg.base.model.BaseModel;
import com.yareg.base.model.DatabaseTable;
import com.yareg.upkite.constant.Key;

public class FirewallGroup extends BaseModel<FirewallGroup> {

    //----------------------------------------------------------------------------------------------
    //
    // DATABASE CONTRACT
    //
    //----------------------------------------------------------------------------------------------

    public static class Contract extends BaseContentProvider.BaseContract {
        Contract() { super("firewall_group"); }

        @Override
        protected DatabaseTable getTable(@NonNull final String name) {
            final DatabaseTable table = super.getTable(name);
            table.put(Key.Database.LABEL,          TYPE_TEXT);
            table.put(Key.Database.DESCRIPTION,    TYPE_TEXT);
            table.put(Key.Database.CREATED,        TYPE_INTEGER);
            table.put(Key.Database.UPDATED,        TYPE_INTEGER);
            table.put(Key.Database.INSTANCE_COUNT, TYPE_INTEGER);
            table.put(Key.Database.RULE_COUNT,     TYPE_INTEGER);
            table.put(Key.Database.MAX_RULE_COUNT, TYPE_INTEGER);
            return table;
        }
    }

    public static final Contract contract = new Contract();

    //----------------------------------------------------------------------------------------------
    //
    // PROPERTIES
    //
    //----------------------------------------------------------------------------------------------

    public String label;
    public String description;
    public long   dateCreated;
    public long   dateUpdated;
    public int    instanceCount;
    public int    ruleCount;
    public int    maxRuleCount;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTORS
    //
    //----------------------------------------------------------------------------------------------

    public FirewallGroup() {
        super(contract.uri);
    }

    public FirewallGroup(final long id) {
        super(contract.uri, id);
    }

    private FirewallGroup(@NonNull final Cursor cursor) {
        super(contract.uri, cursor);
        label         = cursor.getString(cursor.getColumnIndex(Key.Database.LABEL));
        description   = cursor.getString(cursor.getColumnIndex(Key.Database.DESCRIPTION));
        dateCreated   = cursor.getLong(cursor.getColumnIndex(Key.Database.CREATED));
        dateUpdated   = cursor.getLong(cursor.getColumnIndex(Key.Database.UPDATED));
        instanceCount = cursor.getInt(cursor.getColumnIndex(Key.Database.INSTANCE_COUNT));
        ruleCount     = cursor.getInt(cursor.getColumnIndex(Key.Database.RULE_COUNT));
        maxRuleCount  = cursor.getInt(cursor.getColumnIndex(Key.Database.MAX_RULE_COUNT));
    }

    //----------------------------------------------------------------------------------------------
    //
    // FACTORY IMPLEMENTATION
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public FirewallGroup create(final long id) { return new FirewallGroup(id); }

    @Override
    public FirewallGroup create(@NonNull final Cursor cursor) { return new FirewallGroup(cursor); }

    //----------------------------------------------------------------------------------------------
    //
    // CONTENT VALUES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public ContentValues getContentValues() {
        final ContentValues values = super.getContentValues();
        values.put(Key.Database.LABEL, label);
        values.put(Key.Database.DESCRIPTION, description);
        values.put(Key.Database.CREATED, dateCreated);
        values.put(Key.Database.UPDATED, dateUpdated);
        values.put(Key.Database.INSTANCE_COUNT, instanceCount);
        values.put(Key.Database.RULE_COUNT, ruleCount);
        values.put(Key.Database.MAX_RULE_COUNT, maxRuleCount);
        return values;
    }
}
