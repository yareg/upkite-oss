package com.yareg.upkite.model;

import android.content.ContentValues;
import android.database.Cursor;

import androidx.annotation.NonNull;

import com.yareg.base.content.BaseContentProvider;
import com.yareg.base.model.BaseModel;
import com.yareg.base.model.DatabaseTable;
import com.yareg.upkite.constant.Key;

public class FirewallRule extends BaseModel<FirewallRule> {

    //----------------------------------------------------------------------------------------------
    //
    // DATABASE CONTRACT
    //
    //----------------------------------------------------------------------------------------------

    public static class Contract extends BaseContentProvider.BaseContract {
        Contract() { super("firewall_rule"); }

        @Override
        protected DatabaseTable getTable(@NonNull final String name) {
            final DatabaseTable table = super.getTable(name);
            table.put(Key.Database.NUMBER,      TYPE_INTEGER);
            table.put(Key.Database.DESCRIPTION, TYPE_TEXT);
            table.put(Key.Database.ACTION,      TYPE_TEXT);
            table.put(Key.Database.PROTOCOL,    TYPE_TEXT);
            table.put(Key.Database.PORT,        TYPE_TEXT);
            table.put(Key.Database.SUBNET,      TYPE_TEXT);
            table.put(Key.Database.SUBNET_SIZE, TYPE_INTEGER);
            table.put(Key.Database.SOURCE,      TYPE_INTEGER);
            return table;
        }
    }

    public static final Contract contract = new Contract();

    //----------------------------------------------------------------------------------------------
    //
    // PROPERTIES
    //
    //----------------------------------------------------------------------------------------------

    public int    number;
    public String description;
    public String action;
    public String protocol;
    public String port;
    public String subnet;
    public int    subnetSize;
    public String source;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTORS
    //
    //----------------------------------------------------------------------------------------------

    public FirewallRule() {
        super(contract.uri);
    }

    public FirewallRule(final long id) {
        super(contract.uri, id);
    }

    private FirewallRule(@NonNull final Cursor cursor) {
        super(contract.uri, cursor);
        number      = cursor.getInt(cursor.getColumnIndex(Key.Database.NUMBER));
        description = cursor.getString(cursor.getColumnIndex(Key.Database.DESCRIPTION));
        action      = cursor.getString(cursor.getColumnIndex(Key.Database.ACTION));
        protocol    = cursor.getString(cursor.getColumnIndex(Key.Database.PROTOCOL));
        port        = cursor.getString(cursor.getColumnIndex(Key.Database.PORT));
        subnet      = cursor.getString(cursor.getColumnIndex(Key.Database.SUBNET));
        subnetSize  = cursor.getInt(cursor.getColumnIndex(Key.Database.SUBNET_SIZE));
        source      = cursor.getString(cursor.getColumnIndex(Key.Database.SOURCE));
    }

    //----------------------------------------------------------------------------------------------
    //
    // FACTORY IMPLEMENTATION
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public FirewallRule create(final long id) { return new FirewallRule(id); }

    @Override
    public FirewallRule create(@NonNull final Cursor cursor) { return new FirewallRule(cursor); }

    //----------------------------------------------------------------------------------------------
    //
    // CONTENT VALUES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public ContentValues getContentValues() {
        final ContentValues values = super.getContentValues();
        values.put(Key.Database.NUMBER, number);
        values.put(Key.Database.DESCRIPTION, description);
        values.put(Key.Database.ACTION, action);
        values.put(Key.Database.PROTOCOL, protocol);
        values.put(Key.Database.PORT, port);
        values.put(Key.Database.SUBNET, subnet);
        values.put(Key.Database.SUBNET_SIZE, subnetSize);
        values.put(Key.Database.SOURCE, source);
        return values;
    }
}
