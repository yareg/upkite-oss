package com.yareg.upkite.model;

import android.content.ContentValues;
import android.database.Cursor;

import androidx.annotation.NonNull;

import com.yareg.base.content.BaseContentProvider;
import com.yareg.base.model.BaseModel;
import com.yareg.base.model.DatabaseTable;
import com.yareg.upkite.constant.Key;

public class Billing extends BaseModel<Billing> {

    //----------------------------------------------------------------------------------------------
    //
    // DATABASE CONTRACT
    //
    //----------------------------------------------------------------------------------------------

    public static class Contract extends BaseContentProvider.BaseContract {
        Contract() { super("billing"); }

        @Override
        protected DatabaseTable getTable(@NonNull final String name) {
            final DatabaseTable table = super.getTable(name);
            table.put(Key.Database.BALANCE,             TYPE_REAL);
            table.put(Key.Database.PENDING_CHARGES,     TYPE_REAL);
            table.put(Key.Database.REMAINING_CREDIT,    TYPE_REAL);
            table.put(Key.Database.MONTHLY_COST,        TYPE_REAL);
            table.put(Key.Database.LAST_PAYMENT_AMOUNT, TYPE_REAL);
            table.put(Key.Database.LAST_PAYMENT_DATE,   TYPE_INTEGER);
            return table;
        }
    }

    public static final Contract contract = new Contract();

    //----------------------------------------------------------------------------------------------
    //
    // PROPERTIES
    //
    //----------------------------------------------------------------------------------------------

    public float balance;
    public float pendingCharges;
    public float remainingCredit;
    public float totalMonthlyCost;
    public float lastPaymentAmount;
    public long  lastPaymentDate;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTORS
    //
    //----------------------------------------------------------------------------------------------

    public Billing(final long id) {
        super(contract.uri, id);
    }

    private Billing(@NonNull final Cursor cursor) {
        super(contract.uri, cursor);
        balance = cursor.getFloat(cursor.getColumnIndex(Key.Database.BALANCE));
        pendingCharges = cursor.getFloat(cursor.getColumnIndex(Key.Database.PENDING_CHARGES));
        remainingCredit = cursor.getFloat(cursor.getColumnIndex(Key.Database.REMAINING_CREDIT));
        totalMonthlyCost = cursor.getFloat(cursor.getColumnIndex(Key.Database.MONTHLY_COST));
        lastPaymentAmount = cursor.getFloat(cursor.getColumnIndex(Key.Database.LAST_PAYMENT_AMOUNT));
        lastPaymentDate = cursor.getLong(cursor.getColumnIndex(Key.Database.LAST_PAYMENT_DATE));
    }

    //----------------------------------------------------------------------------------------------
    //
    // FACTORY IMPLEMENTATION
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public Billing create(final long id) { return new Billing(id); }

    @Override
    public Billing create(@NonNull final Cursor cursor) {
        return new Billing(cursor);
    }

    //----------------------------------------------------------------------------------------------
    //
    // CONTENT VALUES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public ContentValues getContentValues() {
        final ContentValues values = super.getContentValues();
        values.put(Key.Database.BALANCE, balance);
        values.put(Key.Database.PENDING_CHARGES, pendingCharges);
        values.put(Key.Database.REMAINING_CREDIT, remainingCredit);
        values.put(Key.Database.MONTHLY_COST, totalMonthlyCost);
        values.put(Key.Database.LAST_PAYMENT_AMOUNT, lastPaymentAmount);
        values.put(Key.Database.LAST_PAYMENT_DATE, lastPaymentDate);
        return values;
    }
}
