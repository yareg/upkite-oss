package com.yareg.upkite.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.yareg.base.content.BaseContentProvider;
import com.yareg.base.model.DatabaseTable;
import com.yareg.upkite.constant.Key;
import com.yareg.base.model.BaseModel;

public class Profile extends BaseModel<Profile> {

    public static final String NAME_SPLITTER = " ";
    public static final String PERMISSIONS_DELIMITER = ",";

    //----------------------------------------------------------------------------------------------
    //
    // DATABASE CONTRACT
    //
    //----------------------------------------------------------------------------------------------

    public static class Contract extends BaseContentProvider.BaseContract {
        Contract() { super("profile"); }

        @Override
        protected DatabaseTable getTable(@NonNull final String name) {
            final DatabaseTable table = super.getTable(name);
            table.put(Key.Database.TOKEN,       TYPE_TEXT);
            table.put(Key.Database.FIRST_NAME,  TYPE_TEXT);
            table.put(Key.Database.LAST_NAME,   TYPE_TEXT);
            table.put(Key.Database.EMAIL,       TYPE_TEXT);
            table.put(Key.Database.PERMISSIONS, TYPE_TEXT);
            table.put(Key.Database.LAST_SYNC,   TYPE_INTEGER);
            return table;
        }
    }

    public static final Contract contract = new Contract();

    //----------------------------------------------------------------------------------------------
    //
    // PROPERTIES
    //
    //----------------------------------------------------------------------------------------------

    public String   token;
    public String   firstName;
    public String   lastName;
    public String   email;
    public String[] permissions;
    public long     lastSync;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTORS
    //
    //----------------------------------------------------------------------------------------------

    public Profile() {
        super(contract.uri);
    }

    public Profile(final long id) {
        super(contract.uri, id);
    }

    private Profile(@NonNull final Cursor cursor) {
        super(contract.uri, cursor);
        token = cursor.getString(cursor.getColumnIndex(Key.Database.TOKEN));
        firstName = cursor.getString(cursor.getColumnIndex(Key.Database.FIRST_NAME));
        lastName = cursor.getString(cursor.getColumnIndex(Key.Database.LAST_NAME));
        email = cursor.getString(cursor.getColumnIndex(Key.Database.EMAIL));
        lastSync = cursor.getLong(cursor.getColumnIndex(Key.Database.LAST_SYNC));

        final String s = cursor.getString(cursor.getColumnIndex(Key.Database.PERMISSIONS));
        permissions = s == null ? new String[]{} : s.split(PERMISSIONS_DELIMITER);
    }

    //----------------------------------------------------------------------------------------------
    //
    // FACTORY IMPLEMENTATION
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public Profile create(final long id) { return new Profile(id); }

    @Override
    public Profile create(@NonNull final Cursor cursor) { return new Profile(cursor); }

    //----------------------------------------------------------------------------------------------
    //
    // CONTENT VALUES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public ContentValues getContentValues() {
        final ContentValues values = super.getContentValues();
        values.put(Key.Database.TOKEN, token);
        values.put(Key.Database.FIRST_NAME, firstName);
        values.put(Key.Database.LAST_NAME, lastName);
        values.put(Key.Database.EMAIL, email);
        values.put(Key.Database.PERMISSIONS, TextUtils.join(PERMISSIONS_DELIMITER, permissions));
        values.put(Key.Database.LAST_SYNC, lastSync);
        return values;
    }

    //----------------------------------------------------------------------------------------------
    //
    // VALIDITY
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public boolean isValid() {
        return (super.isValid() && !email.isEmpty());
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- GET TITLE -------------------------------------------------------------------------------

    // return null to revert to value from strings
    //public String getTitle() { return firstName + NAME_SPLITTER + lastName; }

    // --- GET DESCRIPTION -------------------------------------------------------------------------

    // return null to revert to value from strings
    //public String getDescription() { return email; }

    // --- GET ACCOUNT NAME ------------------------------------------------------------------------

    public String getAccountName() { return email; }

    // --- GET PERMISSIONS -------------------------------------------------------------------------

    public String[] getPermissions() { return permissions; }
}
