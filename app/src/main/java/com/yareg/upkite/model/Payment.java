package com.yareg.upkite.model;

import android.content.ContentValues;
import android.database.Cursor;

import androidx.annotation.NonNull;

import com.yareg.base.content.BaseContentProvider;
import com.yareg.base.model.BaseModel;
import com.yareg.base.model.DatabaseTable;
import com.yareg.upkite.constant.Key;

public class Payment extends BaseModel<Payment> {

    //----------------------------------------------------------------------------------------------
    //
    // DATABASE CONTRACT
    //
    //----------------------------------------------------------------------------------------------

    public static class Contract extends BaseContentProvider.BaseContract {
        Contract() { super("payment"); }

        @Override
        protected DatabaseTable getTable(@NonNull final String name) {
            final DatabaseTable table = super.getTable(name);
            table.put(Key.Database.AMOUNT, TYPE_REAL);
            return table;
        }
    }

    public static final Contract contract = new Contract();

    //----------------------------------------------------------------------------------------------
    //
    // PROPERTIES
    //
    //----------------------------------------------------------------------------------------------

    public float amount;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTORS
    //
    //----------------------------------------------------------------------------------------------

    public Payment() {
        super(contract.uri);
    }

    public Payment(final long id) {
        super(contract.uri, id);
    }

    private Payment(@NonNull final Cursor cursor) {
        super(contract.uri, cursor);
        amount = cursor.getFloat(cursor.getColumnIndex(Key.Database.AMOUNT));
    }

    //----------------------------------------------------------------------------------------------
    //
    // FACTORY IMPLEMENTATION
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public Payment create(final long id) { return new Payment(id); }

    @Override
    public Payment create(@NonNull final Cursor cursor) { return new Payment(cursor); }

    //----------------------------------------------------------------------------------------------
    //
    // CONTENT VALUES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public ContentValues getContentValues() {
        final ContentValues values = super.getContentValues();
        values.put(Key.Database.AMOUNT, amount);
        return values;
    }
}
