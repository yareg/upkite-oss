package com.yareg.upkite.model;

import android.content.ContentValues;
import android.database.Cursor;

import androidx.annotation.NonNull;

import com.yareg.base.content.BaseContentProvider;
import com.yareg.base.model.BaseModel;
import com.yareg.base.model.DatabaseTable;
import com.yareg.upkite.constant.Key;

public class Server extends BaseModel<Server> {

    //----------------------------------------------------------------------------------------------
    //
    // DATABASE CONTRACT
    //
    //----------------------------------------------------------------------------------------------

    public static class Contract extends BaseContentProvider.BaseContract {
        Contract() { super("server"); }

        @Override
        protected DatabaseTable getTable(@NonNull final String name) {
            final DatabaseTable table = super.getTable(name);
            table.put(Key.Database.LABEL,             TYPE_TEXT);
            table.put(Key.Database.STATUS,            TYPE_INTEGER);
            table.put(Key.Database.STATE,             TYPE_INTEGER);
            table.put(Key.Database.IS_ACTIVE,         TYPE_INTEGER);
            table.put(Key.Database.IS_RUNNING,        TYPE_TEXT); //TODO: should be integer
            table.put(Key.Database.IS_OK,             TYPE_TEXT); //TODO: should be integer
            table.put(Key.Database.OS_ID,             TYPE_INTEGER);
            table.put(Key.Database.OS,                TYPE_TEXT);
            table.put(Key.Database.PENDING_CHARGES,   TYPE_REAL);
            table.put(Key.Database.MONTHLY_COST,      TYPE_REAL);
            table.put(Key.Database.CURRENT_BANDWIDTH, TYPE_REAL);
            table.put(Key.Database.ALLOWED_BANDWIDTH, TYPE_REAL);
            table.put(Key.Database.BANDWIDTH_PERCENT, TYPE_REAL);
            table.put(Key.Database.LOCATION,          TYPE_TEXT);
            table.put(Key.Database.MAIN_IP,           TYPE_TEXT);
            table.put(Key.Database.INTERNAL_IP,       TYPE_TEXT);
            table.put(Key.Database.FIREWALL,          TYPE_TEXT);
            table.put(Key.Database.TAG,               TYPE_TEXT);
            table.put(Key.Database.PLAN,              TYPE_TEXT);
            table.put(Key.Database.CPU,               TYPE_INTEGER);
            table.put(Key.Database.RAM,               TYPE_INTEGER);
            table.put(Key.Database.DISK,              TYPE_INTEGER);
            return table;
        }
    }

    public static final Contract contract = new Contract();

    //----------------------------------------------------------------------------------------------
    //
    // PROPERTIES
    //
    //----------------------------------------------------------------------------------------------

    public String  label;
    public int     status;
    public int     state;
    public boolean isActive;
    public boolean isRunning;
    public boolean isOk;
    public int     osId;
    public String  os;
    public float   pendingCharges;
    public float   monthlyCost;
    public double  currentBandwidth;
    public double  allowedBandwidth;
    public float   bandwidthPercent;
    public String  location;
    public String  mainIp;
    public String  internalIp;
    public String  firewall;
    public String  tag;
    public String  plan;
    public int     cpu;
    public int     ram;
    public int     disk;


    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTORS
    //
    //----------------------------------------------------------------------------------------------

    public Server() {
        super(contract.uri);
    }

    public Server(final long id) {
        super(contract.uri, id);
    }

    private Server(@NonNull final Cursor cursor) {
        super(contract.uri, cursor);
        label = cursor.getString(cursor.getColumnIndex(Key.Database.LABEL));
        status = cursor.getInt(cursor.getColumnIndex(Key.Database.STATUS));
        state = cursor.getInt(cursor.getColumnIndex(Key.Database.STATE));
        isActive = (cursor.getInt(cursor.getColumnIndex(Key.Database.IS_ACTIVE)) == 1);
        isRunning = (cursor.getInt(cursor.getColumnIndex(Key.Database.IS_RUNNING)) == 1);
        isOk = (cursor.getInt(cursor.getColumnIndex(Key.Database.IS_OK)) == 1);
        osId = cursor.getInt(cursor.getColumnIndex(Key.Database.OS_ID));
        os = cursor.getString(cursor.getColumnIndex(Key.Database.OS));
        pendingCharges = cursor.getFloat(cursor.getColumnIndex(Key.Database.PENDING_CHARGES));
        monthlyCost = cursor.getFloat(cursor.getColumnIndex(Key.Database.MONTHLY_COST));
        currentBandwidth = cursor.getDouble(cursor.getColumnIndex(Key.Database.CURRENT_BANDWIDTH));
        allowedBandwidth = cursor.getDouble(cursor.getColumnIndex(Key.Database.ALLOWED_BANDWIDTH));
        bandwidthPercent = cursor.getFloat(cursor.getColumnIndex(Key.Database.BANDWIDTH_PERCENT));
        location = cursor.getString(cursor.getColumnIndex(Key.Database.LOCATION));
        mainIp = cursor.getString(cursor.getColumnIndex(Key.Database.MAIN_IP));
        internalIp = cursor.getString(cursor.getColumnIndex(Key.Database.INTERNAL_IP));
        firewall = cursor.getString(cursor.getColumnIndex(Key.Database.FIREWALL));
        tag = cursor.getString(cursor.getColumnIndex(Key.Database.TAG));
        plan = cursor.getString(cursor.getColumnIndex(Key.Database.PLAN));
        cpu = cursor.getInt(cursor.getColumnIndex(Key.Database.CPU));
        ram = cursor.getInt(cursor.getColumnIndex(Key.Database.RAM));
        disk = cursor.getInt(cursor.getColumnIndex(Key.Database.DISK));
    }

    //----------------------------------------------------------------------------------------------
    //
    // FACTORY IMPLEMENTATION
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public Server create(final long id) { return new Server(id); }

    @Override
    public Server create(@NonNull final Cursor cursor) { return new Server(cursor); }

    //----------------------------------------------------------------------------------------------
    //
    // CONTENT VALUES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public ContentValues getContentValues() {
        final ContentValues values = super.getContentValues();
        values.put(Key.Database.LABEL, label);
        values.put(Key.Database.STATUS, status);
        values.put(Key.Database.STATE, state);
        values.put(Key.Database.IS_ACTIVE, (isActive ? 1 : 0));
        values.put(Key.Database.IS_RUNNING, (isRunning ? 1 : 0));
        values.put(Key.Database.IS_OK, (isOk ? 1 : 0));
        values.put(Key.Database.OS_ID, osId);
        values.put(Key.Database.OS, os);
        values.put(Key.Database.PENDING_CHARGES, pendingCharges);
        values.put(Key.Database.MONTHLY_COST, monthlyCost);
        values.put(Key.Database.CURRENT_BANDWIDTH, currentBandwidth);
        values.put(Key.Database.ALLOWED_BANDWIDTH, allowedBandwidth);
        values.put(Key.Database.BANDWIDTH_PERCENT, bandwidthPercent);
        values.put(Key.Database.LOCATION, location);
        values.put(Key.Database.MAIN_IP, mainIp);
        values.put(Key.Database.INTERNAL_IP, internalIp);
        values.put(Key.Database.FIREWALL, firewall);
        values.put(Key.Database.TAG, tag);
        values.put(Key.Database.PLAN, plan);
        values.put(Key.Database.CPU, cpu);
        values.put(Key.Database.RAM, ram);
        values.put(Key.Database.DISK, disk);
        return values;
    }
}
