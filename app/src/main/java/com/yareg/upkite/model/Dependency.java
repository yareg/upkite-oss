package com.yareg.upkite.model;

import com.google.gson.annotations.SerializedName;

public class Dependency {

    /*
    {
        "moduleName": "androidx.activity:activity",
        "moduleUrl": "http://developer.android.com/tools/extras/support-library.html",
        "moduleVersion": "1.0.0-alpha08",
        "moduleLicense": "The Apache Software License, Version 2.0",
        "moduleLicenseUrl": "http://www.apache.org/licenses/LICENSE-2.0.txt"
    }
    */

    @SerializedName("moduleName")
    public String name;

    @SerializedName("moduleUrl")
    public String url;

    @SerializedName("moduleVersion")
    public String version;

    @SerializedName("moduleLicense")
    public String license;

    @SerializedName("moduleLicenseUrl")
    public String licenseUrl;
}