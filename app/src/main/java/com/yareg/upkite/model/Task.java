package com.yareg.upkite.model;

import android.content.ContentValues;
import android.database.Cursor;

import androidx.annotation.NonNull;

import com.yareg.base.content.BaseContentProvider;
import com.yareg.base.model.BaseModel;
import com.yareg.base.model.DatabaseTable;
import com.yareg.upkite.constant.Key;

public class Task extends BaseModel<Task> {

    //----------------------------------------------------------------------------------------------
    //
    // DATABASE CONTRACT
    //
    //----------------------------------------------------------------------------------------------

    public static class Contract extends BaseContentProvider.BaseContract {
        Contract() { super("task"); }

        @Override
        protected DatabaseTable getTable(@NonNull final String name) {
            final DatabaseTable table = super.getTable(name);
            table.put(Key.Database.TYPE_ID,   TYPE_INTEGER);
            table.put(Key.Database.OBJECT_ID, TYPE_TEXT);
            table.put(Key.Database.STATUS,    TYPE_INTEGER);
            table.put(Key.Database.UPDATED,   TYPE_INTEGER);
            return table;
        }
    }

    public static final Contract contract = new Contract();

    //----------------------------------------------------------------------------------------------
    //
    // PROPERTIES
    //
    //----------------------------------------------------------------------------------------------

    public short  typeId;
    public String objectId;
    public short  status;
    public long   updated;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTORS
    //
    //----------------------------------------------------------------------------------------------

    public Task() {
        super(contract.uri);
    }

    public Task(final long id) {
        super(contract.uri, id);
    }

    private Task(@NonNull final Cursor cursor) {
        super(contract.uri, cursor);
        typeId = cursor.getShort(cursor.getColumnIndex(Key.Database.TYPE_ID));
        objectId = cursor.getString(cursor.getColumnIndex(Key.Database.OBJECT_ID));
        status = cursor.getShort(cursor.getColumnIndex(Key.Database.STATUS));
        updated = cursor.getShort(cursor.getColumnIndex(Key.Database.UPDATED));
    }

    //----------------------------------------------------------------------------------------------
    //
    // FACTORY IMPLEMENTATION
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public Task create(final long id) { return new Task(id); }

    @Override
    public Task create(@NonNull final Cursor cursor) { return new Task(cursor); }

    //----------------------------------------------------------------------------------------------
    //
    // CONTENT VALUES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    public ContentValues getContentValues() {
        final ContentValues values = super.getContentValues();
        values.put(Key.Database.TYPE_ID, typeId);
        values.put(Key.Database.OBJECT_ID, objectId);
        values.put(Key.Database.STATUS, status);
        values.put(Key.Database.UPDATED, updated);
        return values;
    }
}
