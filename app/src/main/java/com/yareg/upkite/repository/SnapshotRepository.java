package com.yareg.upkite.repository;

import androidx.annotation.NonNull;

import com.yareg.base.livedata.ListLiveData;
import com.yareg.upkite.model.Snapshot;

public class SnapshotRepository extends ListLiveData<Snapshot> {

    //----------------------------------------------------------------------------------------------
    //
    // INSTANCE
    // https://en.wikipedia.org/wiki/Initialization_on_demand_holder_idiom
    //
    //----------------------------------------------------------------------------------------------

    private static class Singleton {
        static final SnapshotRepository INSTANCE = new SnapshotRepository();
    }

    @NonNull
    public static SnapshotRepository getInstance() { return SnapshotRepository.Singleton.INSTANCE; }

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    private SnapshotRepository() {
        super(new Snapshot(), true);
    }
}
