package com.yareg.upkite.repository;

import androidx.annotation.NonNull;

import com.yareg.base.livedata.SingleLiveData;
import com.yareg.upkite.model.Profile;

public class ProfileRepository extends SingleLiveData<Profile> {

    private static long entityId = 0;

    //----------------------------------------------------------------------------------------------
    //
    // INSTANCE
    // https://en.wikipedia.org/wiki/Initialization_on_demand_holder_idiom
    //
    //----------------------------------------------------------------------------------------------

    private static class Singleton {
        static final ProfileRepository INSTANCE = new ProfileRepository();
    }

    @NonNull
    public static ProfileRepository getInstance() { return ProfileRepository.Singleton.INSTANCE; }

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    private ProfileRepository() {
        super(new Profile(entityId));
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    public static void setEntityId(final long id) {
        entityId = id;
        getInstance().setId(id);
    }
}
