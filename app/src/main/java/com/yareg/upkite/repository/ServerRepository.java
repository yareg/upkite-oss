package com.yareg.upkite.repository;

import androidx.annotation.NonNull;

import com.yareg.upkite.model.Server;
import com.yareg.base.livedata.ListLiveData;

public class ServerRepository extends ListLiveData<Server> {

    //----------------------------------------------------------------------------------------------
    //
    // INSTANCE
    // https://en.wikipedia.org/wiki/Initialization_on_demand_holder_idiom
    //
    //----------------------------------------------------------------------------------------------

    private static class Singleton {
        static final ServerRepository INSTANCE = new ServerRepository();
    }

    @NonNull
    public static ServerRepository getInstance() { return ServerRepository.Singleton.INSTANCE; }

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    private ServerRepository() {
        super(new Server(), true);
    }
}
