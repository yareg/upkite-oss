package com.yareg.upkite.repository;

import androidx.annotation.NonNull;

import com.yareg.base.livedata.SingleLiveData;
import com.yareg.upkite.model.Billing;

public class BillingRepository extends SingleLiveData<Billing> {

    private static long entityId = 0;

    //----------------------------------------------------------------------------------------------
    //
    // INSTANCE
    // https://en.wikipedia.org/wiki/Initialization_on_demand_holder_idiom
    //
    //----------------------------------------------------------------------------------------------

    private static class Singleton {
        static final BillingRepository INSTANCE = new BillingRepository();
    }

    @NonNull
    public static BillingRepository getInstance() { return BillingRepository.Singleton.INSTANCE; }

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    private BillingRepository() {
        super(new Billing(entityId));
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    public static void setEntityId(final long id) {
        entityId = id;
        getInstance().setId(id);
    }
}
