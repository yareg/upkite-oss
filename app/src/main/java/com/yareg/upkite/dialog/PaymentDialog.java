package com.yareg.upkite.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.yareg.upkite.R;
import com.yareg.upkite.activity.AppActivity;
import com.yareg.upkite.databinding.DialogPaymentBinding;
import com.yareg.upkite.databinding.LinearActionBinding;
import com.yareg.upkite.viewmodel.PaymentViewModel;

public class PaymentDialog extends BaseItemDialog {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public PaymentDialog() {
        super(PaymentDialog.class.hashCode());
    }

    public static PaymentDialog createInstance(final long paymentId) {
        final PaymentDialog instance = new PaymentDialog();
        instance.setItemId(paymentId);
        return instance;
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    @Nullable
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {

        final AppActivity activity = (AppActivity) getActivity();
        if (activity == null) { return null; }

        final PaymentViewModel viewModel = activity
                .getViewModelProvider()
                .get(PaymentViewModel.class);

        final long itemId = getItemId();

        final DialogPaymentBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.dialog_payment, container,false
        );

        binding.setLifecycleOwner(getViewLifecycleOwner());
        binding.setItem(viewModel.getItem(itemId));

        final LinearActionBinding actionDelete = DataBindingUtil.inflate(
                inflater,
                R.layout.linear_action,
                binding.dialogPaymentActions,
                true
        );

        actionDelete.setIcon(R.drawable.icon_delete);
        actionDelete.setText(R.string.action_delete);
        actionDelete.setOnClick((view) -> {
            view.setEnabled(false);
            viewModel.trashItem(itemId, null);
            dismiss();
        });

        return binding.getRoot();
    }
}
