package com.yareg.upkite.dialog;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public abstract class BaseItemDialog extends BottomSheetDialogFragment {

    private static final String KEY_ID = "id";
    private final String tag;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public BaseItemDialog(final int hashCode) {
        tag = "f" + hashCode;
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    public long getItemId() {
        return getArguments() == null ? 0 : getArguments().getLong(KEY_ID);
    }

    // --- SET ID ----------------------------------------------------------------------------------
    public void setItemId(final long id) {
        final Bundle args = new Bundle();
        args.putLong(KEY_ID, id);
        setArguments(args);
    }

    // --- SHOW ------------------------------------------------------------------------------------
    public void show(@NonNull FragmentManager fragmentManager) {
        if (getArguments() != null) {
            super.show(fragmentManager, tag + "-" + getArguments().getLong(KEY_ID));
        }
    }
}
