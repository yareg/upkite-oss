package com.yareg.upkite.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.yareg.upkite.R;
import com.yareg.upkite.activity.AppActivity;
import com.yareg.upkite.databinding.DialogFirewallGroupBinding;
import com.yareg.upkite.databinding.LinearActionBinding;
import com.yareg.upkite.viewmodel.FirewallViewModel;

public class FirewallGroupDialog extends BaseItemDialog {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public FirewallGroupDialog() {
        super(FirewallGroupDialog.class.hashCode());
    }

    @NonNull
    public static FirewallGroupDialog createInstance(final long firewallGroupId) {
        final FirewallGroupDialog instance = new FirewallGroupDialog();
        instance.setItemId(firewallGroupId);
        return instance;
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    @Nullable
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {

        final AppActivity activity = (AppActivity) getActivity();
        if (activity == null) { return null; }

        final FirewallViewModel viewModel = activity
                .getViewModelProvider()
                .get(FirewallViewModel.class);

        final long itemId = getItemId();

        final DialogFirewallGroupBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.dialog_firewall_group, container,false
        );

        binding.setLifecycleOwner(getViewLifecycleOwner());
        binding.setItem(viewModel.getItem(itemId));

        final LinearActionBinding actionRules = DataBindingUtil.inflate(
                inflater,
                R.layout.linear_action,
                binding.dialogFirewallGroupActions,
                true
        );

        actionRules.setIcon(R.drawable.icon_rules);
        actionRules.setText(R.string.action_manage_rules);
        actionRules.setOnClick((view) -> {
            view.setEnabled(false);
            dismiss();
        });

        return binding.getRoot();
    }
}
