package com.yareg.upkite.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.yareg.upkite.R;
import com.yareg.upkite.activity.AppActivity;
import com.yareg.upkite.databinding.DialogSnapshotBinding;
import com.yareg.upkite.databinding.LinearActionBinding;
import com.yareg.upkite.viewmodel.SnapshotViewModel;

public class SnapshotDialog extends BaseItemDialog {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public SnapshotDialog() {
        super(SnapshotDialog.class.hashCode());
    }

    public static SnapshotDialog createInstance(final long snapshotId) {
        final SnapshotDialog instance = new SnapshotDialog();
        instance.setItemId(snapshotId);
        return instance;
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    @Nullable
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {

        final AppActivity activity = (AppActivity) getActivity();
        if (activity == null) { return null; }

        final SnapshotViewModel viewModel = activity
                .getViewModelProvider()
                .get(SnapshotViewModel.class);

        final long itemId = getItemId();

        final DialogSnapshotBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.dialog_snapshot, container,false
        );

        binding.setLifecycleOwner(getViewLifecycleOwner());
        binding.setItem(viewModel.getItem(itemId));

        final LinearActionBinding actionDelete = DataBindingUtil.inflate(
                inflater,
                R.layout.linear_action,
                binding.dialogSnapshotActions,
                true
        );

        actionDelete.setIcon(R.drawable.icon_delete);
        actionDelete.setText(R.string.action_delete);
        actionDelete.setOnClick((view) -> {
            view.setEnabled(false);
            viewModel.trashItem(itemId, viewModel::deleteSnapshot);
            dismiss();
        });

        return binding.getRoot();
    }
}
