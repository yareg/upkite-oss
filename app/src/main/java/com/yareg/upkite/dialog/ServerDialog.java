package com.yareg.upkite.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.yareg.upkite.R;
import com.yareg.upkite.activity.AppActivity;
import com.yareg.upkite.databinding.DialogServerBinding;
import com.yareg.upkite.databinding.LinearActionBinding;
import com.yareg.upkite.viewmodel.ServerViewModel;

public class ServerDialog extends BaseItemDialog {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public ServerDialog() {
        super(ServerDialog.class.hashCode());
    }

    @NonNull
    public static ServerDialog createInstance(final long serverId) {
        final ServerDialog instance = new ServerDialog();
        instance.setItemId(serverId);
        return instance;
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    @Nullable
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {

        final AppActivity activity = (AppActivity) getActivity();
        if (activity == null) { return null; }

        final ServerViewModel viewModel = activity
                .getViewModelProvider()
                .get(ServerViewModel.class);

        final long itemId = getItemId();

        final DialogServerBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.dialog_server, container,false
        );

        binding.setLifecycleOwner(getViewLifecycleOwner());
        binding.setItem(viewModel.getItem(itemId));

        final LinearActionBinding actionSnapshot = DataBindingUtil.inflate(
                inflater,
                R.layout.linear_action,
                binding.dialogServerActions,
                true
        );

        actionSnapshot.setIcon(R.drawable.icon_cloud);
        actionSnapshot.setText(R.string.action_snapshot);
        actionSnapshot.setOnClick((view) -> {
            view.setEnabled(false);
            viewModel.createSnapshot(itemId);
            dismiss();
        });

        return binding.getRoot();
    }
}
