package com.yareg.upkite.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.yareg.upkite.R;
import com.yareg.upkite.activity.AppActivity;
import com.yareg.upkite.databinding.DialogTaskBinding;
import com.yareg.upkite.databinding.LinearActionBinding;
import com.yareg.upkite.viewmodel.TaskViewModel;

public class TaskDialog extends BaseItemDialog {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public TaskDialog() {
        super(TaskDialog.class.hashCode());
    }

    public static TaskDialog createInstance(final long paymentId) {
        final TaskDialog instance = new TaskDialog();
        instance.setItemId(paymentId);
        return instance;
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    @Nullable
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {

        final AppActivity activity = (AppActivity) getActivity();
        if (activity == null) { return null; }

        final TaskViewModel viewModel = activity
                .getViewModelProvider()
                .get(TaskViewModel.class);

        final long itemId = getItemId();

        final DialogTaskBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.dialog_task, container,false
        );

        binding.setLifecycleOwner(getViewLifecycleOwner());
        binding.setItem(viewModel.getItem(itemId));

        final LinearActionBinding actionDelete = DataBindingUtil.inflate(
                inflater,
                R.layout.linear_action,
                binding.dialogTaskActions,
                true
        );

        actionDelete.setIcon(R.drawable.icon_delete);
        actionDelete.setText(R.string.action_delete);
        actionDelete.setOnClick((view) -> {
            view.setEnabled(false);
            viewModel.trashItem(itemId, null);
            dismiss();
        });

        return binding.getRoot();
    }
}
