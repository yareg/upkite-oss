package com.yareg.upkite.dialog;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.yareg.base.manager.BaseNotificationManager;
import com.yareg.upkite.R;
import com.yareg.upkite.activity.AppActivity;
import com.yareg.upkite.databinding.DialogDebugBinding;
import com.yareg.upkite.databinding.LinearActionBinding;
import com.yareg.upkite.notification.SnapshotCompleteNotification;

public class DebugDialog extends BottomSheetDialogFragment {

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    public static DebugDialog createInstance() {
        return new DebugDialog();
    }

    //----------------------------------------------------------------------------------------------
    //
    // OVERRIDES
    //
    //----------------------------------------------------------------------------------------------

    @Override
    @Nullable
    public View onCreateView(
            @NonNull LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {

        final Activity activity = getActivity();
        if (activity == null) { return null; }

        final DialogDebugBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.dialog_debug, container,false
        );

        final LinearActionBinding actionNotification = DataBindingUtil.inflate(
                inflater,
                R.layout.linear_action,
                binding.dialogDebugActions,
                true
        );

        actionNotification.setIcon(R.drawable.icon_notification);
        actionNotification.setText(R.string.action_notification);
        actionNotification.setOnClick((view) ->
                BaseNotificationManager.showNotification(
                        activity,
                        new SnapshotCompleteNotification(
                                activity.getResources(), "8b60032e05e0a"
                        )
                )
        );

        return binding.getRoot();
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- SHOW ------------------------------------------------------------------------------------
    public void show(@NonNull AppActivity activity) {
        super.show(activity.getSupportFragmentManager(), "f" + DebugDialog.class.hashCode());
    }
}
