package com.yareg.upkite.vultr.model;

import com.google.gson.annotations.SerializedName;

public class VultrUser {

    /*
    {
        "USERID": "564a1a7794d83",
        "NAME": "example user 1",
        "email": "example@vultr.com",
        "api_enabled": "yes",
        "acls": [
            "root",
            "manage_users",
            "subscriptions",
            "billing",
            "support",
            "provisioning",
            "dns",
            "abuse",
            "upgrade",
            "firewall"
        ]
    }
    */

    public static final String YES = "yes";

    @SerializedName("USERID")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("email")
    public String email;

    @SerializedName("api_enabled")
    public String apiEnabled;

    @SerializedName("acls")
    public String[] permissions;
}
