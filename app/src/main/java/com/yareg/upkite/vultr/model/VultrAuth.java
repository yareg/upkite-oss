package com.yareg.upkite.vultr.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class VultrAuth {

    /*
    {
        "acls": [
            "root",
            "manage_users",
            "subscriptions",
            "billing",
            "support",
            "provisioning",
            "dns",
            "abuse",
            "upgrade",
            "firewall"
        ],
        "email": "example@vultr.com",
        "name": "Example API"
    }
    */

    @SerializedName("acls")
    public String[] permissions;

    @SerializedName("email")
    public String email;

    @SerializedName("name")
    public String name;

    // --- LACKS PERMISSIONS -----------------------------------------------------------------------

    public boolean lacksPermissions(@NonNull final String[] requiredPermissions) {
        if (permissions == null || permissions.length < requiredPermissions.length) {
            return true;
        }
        int match = 0;
        for (final String permission : permissions) {
            for(final String requiredPermission : requiredPermissions) {
                if(requiredPermission.equalsIgnoreCase(permission)) { match++; }
            }
        }
        return (match < requiredPermissions.length);
    }

    // --- IS VALID --------------------------------------------------------------------------------

    public boolean isValid() {
        if (permissions == null || email == null || name == null) { return false; }
        return !(email.isEmpty() || name.isEmpty());
    }
}
