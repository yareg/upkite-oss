package com.yareg.upkite.vultr.api;

import com.yareg.upkite.vultr.model.VultrUser;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface VultrUserApi {
    @GET("user/list")
    Call<Map<String, VultrUser>> list(@Header("API-Key") String token);
}
