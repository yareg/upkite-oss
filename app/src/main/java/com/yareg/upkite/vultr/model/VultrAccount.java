package com.yareg.upkite.vultr.model;

import com.google.gson.annotations.SerializedName;

public class VultrAccount {

    /*
    {
        "balance": "-5519.11",
        "pending_charges": "57.03",
        "last_payment_date": "2014-07-18 15:31:01",
        "last_payment_amount": "-1.00"
    }
    */

    @SerializedName("balance")
    public String balance;

    @SerializedName("pending_charges")
    public String pendingCharges;

    @SerializedName("last_payment_date")
    public String lastPaymentDate;

    @SerializedName("last_payment_amount")
    public String lastPaymentAmount;
}
