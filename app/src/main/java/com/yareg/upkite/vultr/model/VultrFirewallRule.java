package com.yareg.upkite.vultr.model;

import com.google.gson.annotations.SerializedName;

public class VultrFirewallRule {

    /*
    "1": {
        "rulenumber": 1,
        "action": "accept",
        "protocol": "icmp",
        "port": "",
        "subnet": "",
        "subnet_size": 0,
        "source": "",
        "notes": ""
    }
    */

    @SerializedName("rulenumber")
    public int number;

    @SerializedName("action")
    public String action;

    @SerializedName("protocol")
    public String protocol;

    @SerializedName("port")
    public String port;

    @SerializedName("subnet")
    public String subnet;

    @SerializedName("subnet_size")
    public int subnetSize;

    @SerializedName("source")
    public String source;

    @SerializedName("notes")
    public String notes;
}
