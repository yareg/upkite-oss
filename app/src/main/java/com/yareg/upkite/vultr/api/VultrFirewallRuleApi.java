package com.yareg.upkite.vultr.api;

import com.yareg.upkite.vultr.model.VultrFirewallRule;

import java.util.AbstractMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface VultrFirewallRuleApi {

    @GET("firewall/rule_list")
    Call<Map<String, VultrFirewallRule>> list(
            @Header("API-Key") String token,
            @Field("FIREWALLGROUPID") String groupId,
            @Field("direction") String direction,
            @Field("ip_type") String ipVersion
    );

    @FormUrlEncoded
    @POST("firewall/rule_create")
    Call<AbstractMap.SimpleImmutableEntry<String, Integer>> create(
            @Header("API-Key") String token,
            @Field("FIREWALLGROUPID") String groupId,
            @Field("direction") String direction, // Direction of rule. Possible values: "in".
            @Field("ip_type") String ipVersion, // IP address type. Possible values: "v4", "v6".
            @Field("protocol") String protocol, // Protocol type. Possible values: "icmp", "tcp", "udp", "gre".
            @Field("subnet") String subnet, // IP address representing a subnet. The IP address format must match with the "ip_type" parameter value.
            @Field("subnet_size") int subnetSize, // IP prefix size in bits.
            @Field("port") String port, // [optional] TCP/UDP only. This field can be an integer value specifying a port or a colon separated port range.
            @Field("notes") String description, // [optional] This field supports notes up to 255 characters.
            @Field("source") String source // [optional] If the source string is given a value of "cloudflare" subnet and subnet_size will both be ignored, this will allow all of Cloudflare's IP space through the firewall. Possible values: "", "cloudflare".
    );

    @FormUrlEncoded
    @POST("firewall/rule_delete")
    Call<Boolean> delete(
            @Header("API-Key") String token,
            @Field("FIREWALLGROUPID") String groupId,
            @Field("rulenumber") int ruleNumber
    );
}
