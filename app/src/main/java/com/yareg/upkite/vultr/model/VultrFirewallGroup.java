package com.yareg.upkite.vultr.model;

import com.google.gson.annotations.SerializedName;

public class VultrFirewallGroup {

    /*
    "1234abcd": {
        "FIREWALLGROUPID": "1234abcd",
        "description": "my http firewall",
        "date_created": "2017-02-14 17:48:40",
        "date_modified": "2017-02-14 17:48:40",
        "instance_count": 2,
        "rule_count": 2,
        "max_rule_count": 50
    }
    */

    @SerializedName("FIREWALLGROUPID")
    public String label;

    @SerializedName("description")
    public String description;

    @SerializedName("date_created")
    public String dateCreated;

    @SerializedName("date_modified")
    public String dateUpdated;

    @SerializedName("instance_count")
    public int instanceCount;

    @SerializedName("rule_count")
    public int ruleCount;

    @SerializedName("max_rule_count")
    public int maxRuleCount;
}
