package com.yareg.upkite.vultr;

import androidx.annotation.NonNull;

import com.yareg.network.callback.ApiCallback;
import com.yareg.upkite.vultr.VultrClient.ResponseListener;

public class VultrCallback<T> extends ApiCallback<T> {

    private final ResponseListener<T> listener;

    //----------------------------------------------------------------------------------------------
    //
    // CONSTRUCTOR
    //
    //----------------------------------------------------------------------------------------------

    VultrCallback(@NonNull final ResponseListener<T> listener) {
        this.listener = listener;
    }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    public void onApiResponse(final T data) { listener.onResponse(data); }
    public void onApiError(final String message) { listener.onError(message); }
}
