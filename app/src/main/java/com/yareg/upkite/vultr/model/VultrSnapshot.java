package com.yareg.upkite.vultr.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.yareg.base.model.Time;
import com.yareg.upkite.model.Snapshot;

public class VultrSnapshot {

    public static final String COMPLETE = "complete";

    /*
    "5359435d28b9a": {
        "SNAPSHOTID": "5359435d28b9a",
        "date_created": "2014-04-18 12:40:40",
        "description": "Test snapshot",
        "size": "42949672960",
        "status": "complete",
        "OSID": "127",
        "APPID": "0"
    }
    */

    @SerializedName("SNAPSHOTID")
    public String label;

    @SerializedName("date_created")
    public String date;

    @SerializedName("description")
    public String description;

    @SerializedName("size")
    private String size;

    @SerializedName("status")
    public String status;

    @SerializedName("OSID")
    public String osId;

    @SerializedName("APPID")
    public String appId;

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- GET SIZE IN GB --------------------------------------------------------------------------

    public float getSizeInGB() {
        return (Long.parseLong(size) / 1073741824f);
    }

    // --- CREATE SNAPSHOT -------------------------------------------------------------------------

    public static Snapshot createSnapshot(@NonNull final VultrSnapshot vultrSnapshot) {

        final Snapshot snapshot = new Snapshot(Time.getUnixTime(
                vultrSnapshot.date, Time.getUtc()
        ));

        snapshot.label       = vultrSnapshot.label;
        snapshot.description = vultrSnapshot.description;
        snapshot.size        = vultrSnapshot.getSizeInGB();
        snapshot.isActive    = vultrSnapshot.status.equals(VultrSnapshot.COMPLETE);
        snapshot.osId        = Integer.parseInt(vultrSnapshot.osId);
        snapshot.appId       = Integer.parseInt(vultrSnapshot.appId);

        return snapshot;
    }
}
