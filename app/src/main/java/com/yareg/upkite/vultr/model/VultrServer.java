package com.yareg.upkite.vultr.model;

import com.google.gson.annotations.SerializedName;
import com.yareg.upkite.constant.Key;

public class VultrServer {

    /*
    "576965": {
        "SUBID": "576965",
        "os": "CentOS 6 x64",
        "ram": "4096 MB",
        "disk": "Virtual 60 GB",
        "main_ip": "123.123.123.123",
        "vcpu_count": "2",
        "location": "New Jersey",
        "DCID": "1",
        "default_password": "nreqnusibni",
        "date_created": "2013-12-19 14:45:41",
        "pending_charges": "46.67",
        "status": "active", ( pending | active | suspended | closed )
        "cost_per_month": "10.05",
        "current_bandwidth_gb": 131.512,
        "allowed_bandwidth_gb": "1000",
        "netmask_v4": "255.255.255.248",
        "gateway_v4": "123.123.123.1",
        "power_status": "running",
        "server_state": "ok", ( none | locked | installingbooting | isomounting | ok )
        "VPSPLANID": "28",
        "v6_main_ip": "2001:DB8:1000::100",
        "v6_network_size": "64",
        "v6_network": "2001:DB8:1000::",
        "v6_networks": [
            {
                "v6_network": "2001:DB8:1000::",
                "v6_main_ip": "2001:DB8:1000::100",
                "v6_network_size": "64"
            }
        ],
        "label": "my new server",
        "internal_ip": "10.99.0.10",
        "kvm_url": "https://my.vultr.com/subs/novnc/api.php?data=eawxFVZw2mXnhGUV",
        "auto_backups": "yes",
        "tag": "mytag",
        "OSID": "127",
        "APPID": "0",
        "FIREWALLGROUPID": "0"
    }
    */

    public static final String PENDING   = "pending";
    public static final String ACTIVE    = "active";
    public static final String SUSPENDED = "suspended";
    public static final String CLOSED    = "closed";

    public static final String OK           = "ok";
    public static final String LOCKED       = "locked";
    public static final String BOOTING      = "installingbooting";
    public static final String ISO_MOUNTING = "isomounting";

    public static final String RUNNING = "running";

    @SerializedName("SUBID")
    public String id;

    @SerializedName("os")
    public String os;

    @SerializedName("ram")
    public String ram;

    @SerializedName("disk")
    public String disk;

    @SerializedName("main_ip")
    public String mainIp;

    @SerializedName("vcpu_count")
    public String cpu;

    @SerializedName("location")
    public String location;

    @SerializedName("pending_charges")
    public String pendingCharges;

    @SerializedName("status")
    public String status;

    @SerializedName("cost_per_month")
    public String costPerMonth;

    @SerializedName("current_bandwidth_gb")
    public double currentBandwidth;

    @SerializedName("allowed_bandwidth_gb")
    public String allowedBandwidth;

    @SerializedName("power_status")
    public String powerStatus;

    @SerializedName("server_state")
    public String state;

    @SerializedName("VPSPLANID")
    public String plan;

    @SerializedName("label")
    public String label;

    @SerializedName("internal_ip")
    public String internalIp;

    @SerializedName("kvm_url")
    public String kvmUrl;

    @SerializedName("auto_backups")
    public String autoBackups;

    @SerializedName("tag")
    public String tag;

    @SerializedName("OSID")
    public String osId;

    @SerializedName("FIREWALLGROUPID")
    public String firewall;

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- GET STATUS ------------------------------------------------------------------------------

    public int getStatus() {
        if (status == null) { return Key.Server.Status.UNKNOWN; }
        switch (status) {
            case ACTIVE:    return Key.Server.Status.ACTIVE;
            case PENDING:   return Key.Server.Status.PENDING;
            case SUSPENDED: return Key.Server.Status.SUSPENDED;
            case CLOSED:    return Key.Server.Status.CLOSED;
            default:        return Key.Server.Status.UNKNOWN;
        }
    }

    // --- GET STATE -------------------------------------------------------------------------------

    public int getState() {
        if (state == null) { return Key.Server.State.UNKNOWN; }
        switch (state) {
            case OK:           return Key.Server.State.OK;
            case LOCKED:       return Key.Server.State.LOCKED;
            case BOOTING:      return Key.Server.State.BOOTING;
            case ISO_MOUNTING: return Key.Server.State.ISO_MOUNTING;
            default:           return Key.Server.State.UNKNOWN;
        }
    }
}
