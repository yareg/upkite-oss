package com.yareg.upkite.vultr.api;

import com.yareg.upkite.vultr.model.VultrFirewallGroup;

import java.util.AbstractMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface VultrFirewallGroupApi {

    @GET("firewall/group_list")
    Call<Map<String, VultrFirewallGroup>> list(@Header("API-Key") String token);

    @FormUrlEncoded
    @POST("firewall/group_create")
    Call<AbstractMap.SimpleImmutableEntry<String, String>> create(
            @Header("API-Key") String token,
            @Field("description") String description
    );

    @FormUrlEncoded
    @POST("firewall/group_delete")
    Call<Boolean> delete(
            @Header("API-Key") String token,
            @Field("FIREWALLGROUPID") String firewallGroupId
    );

    @FormUrlEncoded
    @POST("firewall/group_set_description")
    Call<Boolean> setDescription(
            @Header("API-Key") String token,
            @Field("FIREWALLGROUPID") String firewallGroupId,
            @Field("description") String description
    );
}
