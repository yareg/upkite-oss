package com.yareg.upkite.vultr;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.yareg.network.manager.ApiManager;
import com.yareg.upkite.vultr.api.VultrAccountApi;
import com.yareg.upkite.vultr.api.VultrAuthApi;
import com.yareg.upkite.vultr.api.VultrFirewallGroupApi;
import com.yareg.upkite.vultr.api.VultrServerApi;
import com.yareg.upkite.vultr.api.VultrSnapshotApi;
import com.yareg.upkite.vultr.model.VultrAccount;
import com.yareg.upkite.vultr.model.VultrAuth;
import com.yareg.upkite.vultr.model.VultrFirewallGroup;
import com.yareg.upkite.vultr.model.VultrServer;
import com.yareg.upkite.vultr.model.VultrSnapshot;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class VultrClient {

    private static final String BASE_URL = "https://api.vultr.com/v1/";

    public static final int TOKEN_LENGTH = 36;

    public static final String[] REQUIRED_PERMISSIONS = {
            "billing",
            "subscriptions"
    };

    //----------------------------------------------------------------------------------------------
    //
    // INTERFACE
    //
    //----------------------------------------------------------------------------------------------

    public interface ResponseListener<T> {
        void onResponse(final T data);
        void onError(final String message);
    }

    //----------------------------------------------------------------------------------------------
    //
    // INSTANCE
    // https://en.wikipedia.org/wiki/Initialization_on_demand_holder_idiom
    //
    //----------------------------------------------------------------------------------------------

    private static class Singleton {
        static final VultrClient INSTANCE = new VultrClient();
    }

    public static VultrClient getInstance() { return Singleton.INSTANCE; }

    //----------------------------------------------------------------------------------------------
    //
    // PUBLIC METHODS
    //
    //----------------------------------------------------------------------------------------------

    // --- GET ACCOUNT -----------------------------------------------------------------------------

    @Nullable
    public VultrAccount getAccount(@NonNull final String token) {
        try {
            return ApiManager
                    .createInterface(VultrAccountApi.class, BASE_URL)
                    .get(token)
                    .execute()
                    .body();
        } catch(IOException e) {
            return null;
        }
    }

    // --- GET AUTH --------------------------------------------------------------------------------

    @Nullable
    public VultrAuth getAuth(@NonNull final String token) {
        try {
            return ApiManager
                    .createInterface(VultrAuthApi.class, BASE_URL)
                    .get(token)
                    .execute()
                    .body();
        } catch(IOException e) {
            return null;
        }
    }

    // --- GET AUTH ASYNC --------------------------------------------------------------------------

    public void getAuth(
            @NonNull final String token,
            @NonNull final ResponseListener<VultrAuth> listener) {

        ApiManager
                .createInterface(VultrAuthApi.class, BASE_URL)
                .get(token)
                .enqueue(new VultrCallback<>(listener));
    }

    // --- GET SERVER LIST -------------------------------------------------------------------------

    @Nullable
    public ArrayList<VultrServer> getServerList(@NonNull final String token) {
        try {
            final Map<String, VultrServer> data = ApiManager
                    .createInterface(VultrServerApi.class, BASE_URL)
                    .list(token)
                    .execute()
                    .body();
            return  data == null ? null : new ArrayList<>(data.values());
        } catch(final IOException e) {
            return null;
        }
    }

    // --- GET SNAPSHOT LIST -----------------------------------------------------------------------

    @Nullable
    public ArrayList<VultrSnapshot> getSnapshotList(@NonNull final String token) {
        try {
            final Map<String, VultrSnapshot> data = ApiManager
                    .createInterface(VultrSnapshotApi.class, BASE_URL)
                    .list(token)
                    .execute()
                    .body();
            return data == null ? null : new ArrayList<>(data.values());
        } catch(final IOException e) {
            return null;
        }
    }

    // --- GET SNAPSHOT ----------------------------------------------------------------------------

    @Nullable
    public VultrSnapshot getSnapshot(
            @NonNull final String token,
            @NonNull final String snapshotId) {
        try {
            final Map<String, VultrSnapshot> data = ApiManager
                    .createInterface(VultrSnapshotApi.class, BASE_URL)
                    .get(token, snapshotId)
                    .execute()
                    .body();
            return (data == null || data.isEmpty()) ? null : new ArrayList<>(data.values()).get(0);
        } catch(final IOException e) {
            return null;
        }
    }

    // --- GET FIREWALL GROUP LIST -----------------------------------------------------------------

    @Nullable
    public ArrayList<VultrFirewallGroup> getFirewallGroupList(@NonNull final String token) {
        try {
            final Map<String, VultrFirewallGroup> data = ApiManager
                    .createInterface(VultrFirewallGroupApi.class, BASE_URL)
                    .list(token)
                    .execute()
                    .body();
            return data == null ? null : new ArrayList<>(data.values());
        } catch(final IOException e) {
            return null;
        }
    }

    // --- CREATE SNAPSHOT -------------------------------------------------------------------------

    @Nullable
    public String createSnapshot(
            @NonNull final String token,
            @NonNull final String serverId,
            @NonNull final String description) {
        try {
            final VultrSnapshot data = ApiManager
                    .createInterface(VultrSnapshotApi.class, BASE_URL)
                    .create(token, serverId, description)
                    .execute()
                    .body();
            return data == null ? null : data.label;
        } catch(final IOException e) {
            return null;
        }
    }

    // --- DESTROY SNAPSHOT ------------------------------------------------------------------------

    public boolean destroySnapshot(@NonNull final String token, @NonNull final String snapshotId) {
        try {
            return ApiManager
                    .createInterface(VultrSnapshotApi.class, BASE_URL)
                    .destroy(token, snapshotId)
                    .execute()
                    .code() == 200;
        } catch(final IOException e) {
            return false;
        }
    }

    // --- GET USER LIST SYNC ----------------------------------------------------------------------

// --Commented out by Inspection START (5/1/20 4:47 PM):
//    @Nullable
//    public ArrayList<VultrUser> getUserList(@NonNull final String token) {
//        try {
//            final Map<String, VultrUser> data = ApiManager
//                    .createInterface(VultrUserApi.class, BASE_URL)
//                    .list(token)
//                    .execute()
//                    .body();
//            return data == null ? null : new ArrayList<>(data.values());
//        } catch(final IOException e) {
//            return null;
//        }
//    }
// --Commented out by Inspection STOP (5/1/20 4:47 PM)
}