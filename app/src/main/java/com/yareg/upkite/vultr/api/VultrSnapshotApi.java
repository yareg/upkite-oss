package com.yareg.upkite.vultr.api;

import com.yareg.upkite.vultr.model.VultrSnapshot;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface VultrSnapshotApi {

    @GET("snapshot/list")
    Call<Map<String, VultrSnapshot>> list(@Header("API-Key") String token);

    @GET("snapshot/list")
    Call<Map<String, VultrSnapshot>> get(
            @Header("API-Key") String token,
            @Query("SNAPSHOTID") String snapshotId
    );

    @FormUrlEncoded
    @POST("snapshot/create")
    Call<VultrSnapshot> create(
            @Header("API-Key") String token,
            @Field("SUBID") String serverId,
            @Field("description") String description
    );

    @FormUrlEncoded
    @POST("snapshot/destroy")
    Call<Void> destroy(
            @Header("API-Key") String token,
            @Field("SNAPSHOTID") String snapshotId
    );
}
