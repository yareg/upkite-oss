package com.yareg.upkite.vultr.api;

import com.yareg.upkite.vultr.model.VultrAccount;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface VultrAccountApi {
    @GET("account/info")
    Call<VultrAccount> get(@Header("API-Key") String token);
}
