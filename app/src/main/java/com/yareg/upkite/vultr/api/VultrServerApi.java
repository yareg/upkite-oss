package com.yareg.upkite.vultr.api;

import com.yareg.upkite.vultr.model.VultrServer;

import java.util.AbstractMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface VultrServerApi {

    @GET("server/list")
    Call<Map<String, VultrServer>> list(@Header("API-Key") String token);

    @GET("server/list")
    Call<AbstractMap.SimpleImmutableEntry<String, VultrServer>> get(
            @Header("API-Key") String token,
            @Query("SUBID") String serverId
    );
}
