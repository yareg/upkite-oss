package com.yareg.upkite.vultr.api;

import com.yareg.upkite.vultr.model.VultrAuth;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface VultrAuthApi {
    @GET("auth/info")
    Call<VultrAuth> get(@Header("API-Key") String token);
}
